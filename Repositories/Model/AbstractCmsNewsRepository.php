<?php
namespace SE\AdminBundle\Repositories\Model;

use Doctrine\ORM\EntityRepository;

/**
 * Gestion des news
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
abstract class AbstractCmsNewsRepository extends EntityRepository
{
    /**
     * Recherche toute les news pour le backoffice
     * @return array
     */
    public function findAllAdmin()
    {
        $qb = $this->createQueryBuilder('news');
        $qb->select('news', 'img', 'imgs', 'cat')
           ->leftJoin('news.image', 'img')
           ->leftJoin('news.images', 'imgs')
           ->leftJoin('news.categories', 'cat')
           ->orderBy('news.datepost', 'DESC');
        $query = $qb->getQuery();

        return $query->getResult();
    }

    /**
     * Affiche les news actives
     * @return array
     */
    public function findAll()
    {
        $qb = $this->createQueryBuilder('news');
        $qb->select('news', 'img', 'imgs', 'cat')
           ->leftJoin('news.image', 'img')
           ->leftJoin('news.images', 'imgs')
           ->leftJoin('news.categories', 'cat')
           ->where('news.datepost <= :now')
           ->setParameter('now', date('Y-m-d'))
           ->orderBy('news.datepost', 'DESC');
        $query = $qb->getQuery();

        return $query->getResult();
    }
}