<?php
namespace SE\AdminBundle\Repositories\Model;
use Doctrine\ORM\EntityRepository;

/**
 * CmsCommentRepository
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
abstract class AbstractCmsCommentRepository extends EntityRepository
{
    /**
     * Find by config key
     * @param string $configkey
     * @return array
     */
    public function findByConfigKey($configkey)
    {
        $qb = $this->createQueryBuilder('c');
        $qb->select('c', 'g')
           ->join('c.generic', 'g')
           ->where('g.configkey = :configkey')
           ->setParameter('configkey', $configkey)
           ->orderBy('c.datepost', 'DESC');
        $query = $qb->getQuery();

        return $query->getResult();
    }
}