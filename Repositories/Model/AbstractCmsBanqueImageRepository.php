<?php
namespace SE\AdminBundle\Repositories\Model;

use Doctrine\ORM\EntityRepository;

/**
 * Abstract banque image repository
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
abstract class AbstractCmsBanqueImageRepository extends EntityRepository
{
}