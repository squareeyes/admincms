<?php
namespace SE\AdminBundle\Repositories\Model;

use Doctrine\ORM\EntityRepository;

/**
 * Abstract prestation repository
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
abstract class AbstractCmsPrestationRepository extends EntityRepository
{
    /**
     * Override findAll
     * @return array
     */
    public function findAll()
    {
        $qb = $this->createQueryBuilder('presta');
        $qb->select('presta', 'img')
           ->join('presta.image', 'img')
           ->orderBy('presta.pos', 'ASC')
           ->addOrderBy('presta.id', 'DESC');
        $query = $qb->getQuery();

        return $query->getResult();
    }

    /**
     * Recherche presta active (all si slug = null)
     * @param string $slug
     * @return array
     */
    public function findAllForFront($slug = null)
    {
        $qb = $this->createQueryBuilder('presta');
        $qb->select('presta', 'img', 'cat', 'catimg')
           ->leftJoin('presta.image', 'img')
           ->leftJoin('presta.categories', 'cat')
           ->leftJoin('cat.image', 'catimg')
           ->where('presta.active = 1');
        if ($slug) {
            $qb->andWhere('cat.slug = :slug')
               ->setParameter('slug', $slug);
        }
        $qb->orderBy('presta.pos', 'ASC')
           ->addOrderBy('presta.id', 'DESC');
        $query = $qb->getQuery();

        return $query->getResult();
    }

    /**
     * Override findOneBySlug
     * @param string $params
     * @return array
     */
    public function findOneBySlug($params)
    {
        $slug = is_array($params) ? $params['slug'] : $params;
        $qb = $this->createQueryBuilder('presta');
        $qb->select('presta', 'imgfirst', 'slideshow', 'slideshowimg', 'img', 'video')
           ->leftJoin('presta.image', 'imgfirst')
           ->leftJoin('presta.slideshow', 'slideshow')
           ->leftJoin('slideshow.images', 'slideshowimg')
           ->leftJoin('slideshowimg.image', 'img')
           ->leftJoin('presta.video', 'video')
           ->where('presta.active = 1')
           ->andWhere('presta.slug = :slug')
           ->setParameter('slug', $slug);
        $query = $qb->getQuery();

        return $query->getOneOrNullResult();
    }
}