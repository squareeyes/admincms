<?php
namespace SE\AdminBundle\Repositories\Model;

use Doctrine\ORM\EntityRepository;

/**
 * Generic field model
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
abstract class AbstractCmsGenericFieldRepository extends EntityRepository
{
    /**
     * Recherche les génériques par fieldkey and generic id
     * @param string $fieldkey
     * @param int    $refgen
     * @return array
     */
    public function findOneByFieldKeyAndRefGen($fieldkey, $refgen)
    {
        $qb = $this->createQueryBuilder('f');
        $qb->select('f', 'g')
           ->join('f.generic', 'g')
           ->where('g.id = :refgen')
           ->andWhere('f.fieldkey = :fieldkey')
           ->setParameters(array(
              'fieldkey' => $fieldkey,
              'refgen'   => $refgen
           ));
        $query = $qb->getQuery();

        return $query->getOneOrNullResult();
    }
}