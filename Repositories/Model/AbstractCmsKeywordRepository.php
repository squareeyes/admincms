<?php
namespace SE\AdminBundle\Repositories\Model;

use Doctrine\ORM\EntityRepository;

/**
 * Repository pour les requêtes sur les keywords
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
abstract class AbstractCmsKeywordRepository extends EntityRepository
{
}