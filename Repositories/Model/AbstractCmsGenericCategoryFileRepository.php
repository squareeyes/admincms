<?php
namespace SE\AdminBundle\Repositories\Model;

use Doctrine\ORM\EntityRepository;

/**
 * Generic field model
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
abstract class AbstractCmsGenericCategoryFileRepository extends EntityRepository
{
    /**
     * Find categorie with file from generic
     * @param int $refgen
     * @return array
     */
    public function findWithFiles($refgen)
    {
        $qb = $this->createQueryBuilder('cat');
        $qb->select('cat', 'f', 'g')
           ->join('cat.files', 'f')
           ->join('f.generic', 'g')
           ->where('g.id = :refgen')
           ->setParameters(array(
              'refgen' => $refgen
           ))
           ->orderBy('cat.title', 'ASC')
           ->addOrderBy('f.position', 'ASC')
           ->addOrderBy('f.id', 'DESC');
        $query = $qb->getQuery();

        return $query->getResult();
    }
}