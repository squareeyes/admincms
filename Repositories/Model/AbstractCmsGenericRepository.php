<?php
namespace SE\AdminBundle\Repositories\Model;

use Doctrine\ORM\EntityRepository;

/**
 * Generic model
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
abstract class AbstractCmsGenericRepository extends EntityRepository
{
    /**
     * Recherche les génériques par config key
     * @param string $configkey
     * @param string $orderfield
     * @param string $orderway
     * @return array
     */
    public function findAllByConfigKey($configkey, $orderfield = 'title', $orderway = 'ASC')
    {
        $qb = $this->createQueryBuilder('g');
        $qb->select('g', 'img', 'ss', 'imgs', 'f', 'fimg', 'g2', 'g3')
           ->leftJoin('g.image', 'img')
           ->leftJoin('g.slideshow', 'ss')
           ->leftJoin('ss.images', 'imgs')
           ->leftJoin('g.fields', 'f')
           ->leftJoin('f.image', 'fimg')
           ->leftJoin('g.genericsparents', 'g2')
           ->leftJoin('g.genericschildrens', 'g3')
           ->where('g.configkey = :configkey')
           ->setParameter('configkey', $configkey)
           ->orderBy('g.'.$orderfield, $orderway);
        $query = $qb->getQuery();

        return $query->getResult();
    }

    /**
     * Recherche générique par ID et config key
     * @param string $configkey
     * @param int    $refgen
     * @return CmsGeneric
     */
    public function findOneByConfigKeyAndId($configkey, $refgen)
    {
        $qb = $this->createQueryBuilder('g');
        $qb->select('g', 'img', 'ss', 'imgs', 'f', 'fimg', 'g2', 'g3')
           ->leftJoin('g.image', 'img')
           ->leftJoin('g.slideshow', 'ss')
           ->leftJoin('ss.images', 'imgs')
           ->leftJoin('g.fields', 'f')
           ->leftJoin('f.image', 'fimg')
           ->leftJoin('g.genericsparents', 'g2')
           ->leftJoin('g.genericschildrens', 'g3')
           ->where('g.configkey = :configkey')
           ->andWhere('g.id = :refgen')
           ->setParameters(array(
                'configkey' => $configkey,
                'refgen'    => $refgen
           ));
        $query = $qb->getQuery();

        return $query->getOneOrNullResult();
    }
}