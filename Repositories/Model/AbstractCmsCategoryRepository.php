<?php
namespace SE\AdminBundle\Repositories\Model;

use Doctrine\ORM\EntityRepository;

/**
 * Repository pour les requêtes sur les catégories
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
abstract class AbstractCmsCategoryRepository extends EntityRepository
{
    /**
     * Recherche les categories avec des mots clés
     * @return array
     */
    public function findCategoryWithKeyword()
    {
        $qb = $this->createQueryBuilder('cat');
        $qb->select('cat', 'kw', 'catimage')
           ->leftJoin('cat.keywords', 'kw')
           ->leftJoin('cat.image', 'catimage')
           ->where('kw IS NOT NULL')
           ->orderBy('cat.libelle', 'ASC');
        $query = $qb->getQuery();

        return $query->getResult();
    }

    /**
     * Recherche les categories avec des prestations
     * @param string $slug
     * @return array
     */
    public function findCategoryWithPrestation($slug = null)
    {
        $qb = $this->createQueryBuilder('cat');
        $qb->select('cat', 'presta', 'catimage')
           ->leftJoin('cat.prestations', 'presta')
           ->leftJoin('cat.image', 'catimage')
           ->where('presta IS NOT NULL');

        if ($slug) {
            $qb->andWhere('cat.slug = :slug')
               ->setParameter('slug', $slug);
        }

        $qb->orderBy('cat.libelle', 'ASC');
        $query = $qb->getQuery();

        return $query->getResult();
    }
}