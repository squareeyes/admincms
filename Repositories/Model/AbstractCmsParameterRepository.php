<?php
namespace SE\AdminBundle\Repositories\Model;

use Doctrine\ORM\EntityRepository;

/**
 * Parameter repository
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
abstract class AbstractCmsParameterRepository extends EntityRepository
{
    /**
     * Override findAll
     * @return array
     */
    public function findAll()
    {
        $qb = $this->createQueryBuilder('p');
        $qb->select('p', 'img')
           ->leftJoin('p.image', 'img');    
        $query = $qb->getQuery();

        return $query->getResult();
    }   
}