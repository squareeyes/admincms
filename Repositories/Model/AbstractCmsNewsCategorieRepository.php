<?php
namespace SE\AdminBundle\Repositories\Model;

use Doctrine\ORM\EntityRepository;

/**
 * Abstract CmsNewsCategorieRepository
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
abstract class AbstractCmsNewsCategorieRepository extends EntityRepository
{
    /**
     * Override findAll
     * @return array
     */
    public function findAll()
    {
        $qb = $this->createQueryBuilder('cat');
        $qb->select('cat', 'news')
           ->leftJoin('cat.news', 'news')
           ->orderBy('cat.libelle', 'ASC')
           ->addOrderBy('news.datepost', 'DESC');
        $query = $qb->getQuery();

        return $query->getResult();
    }
}