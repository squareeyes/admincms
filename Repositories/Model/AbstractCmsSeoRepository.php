<?php
namespace SE\AdminBundle\Repositories\Model;

use Doctrine\ORM\EntityRepository;

/**
 * Abstract CmsSeo repository
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
abstract class AbstractCmsSeoRepository extends EntityRepository
{
    /**
     * Override findOneByPagekey
     * @param string $pagekey
     * @return CmsSeo
     */
    public function findOneByPagekey($pagekey)
    {
        $qb = $this->createQueryBuilder('seo');
        $qb->select('seo', 'image', 'slideshow', 'images', 'imageslideshow', 'video')
           ->leftJoin('seo.image', 'image')
           ->leftJoin('seo.slideshow', 'slideshow')
           ->leftJoin('slideshow.images', 'images')
           ->leftJoin('images.image', 'imageslideshow')
           ->leftJoin('seo.video', 'video')
           ->where('seo.pagekey = :pagekey')
           ->setParameter('pagekey', $pagekey);
        $query = $qb->getQuery();

        return $query->getOneOrNullResult();
    }
}