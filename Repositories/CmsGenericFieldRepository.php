<?php
namespace SE\AdminBundle\Repositories;

use SE\AdminBundle\Repositories\Model\AbstractCmsGenericFieldRepository;

/**
 * Generic field repository
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class CmsGenericFieldRepository extends AbstractCmsGenericFieldRepository
{
}