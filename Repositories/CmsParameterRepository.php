<?php
namespace SE\AdminBundle\Repositories;

use SE\AdminBundle\Repositories\Model\AbstractCmsParameterRepository;

/**
 * Parameter repository
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class CmsParameterRepository extends AbstractCmsParameterRepository
{
}