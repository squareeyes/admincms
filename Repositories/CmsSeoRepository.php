<?php
namespace SE\AdminBundle\Repositories;

use SE\AdminBundle\Repositories\Model\AbstractCmsSeoRepository;

/**
 * CmsSeoRepository
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class CmsSeoRepository extends AbstractCmsSeoRepository
{
}