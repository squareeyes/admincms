<?php
namespace SE\AdminBundle\Repositories;

use SE\AdminBundle\Repositories\Model\AbstractCmsGenericCategoryFileRepository;

/**
 * Generic repository
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class CmsGenericCategoryFileRepository extends AbstractCmsGenericCategoryFileRepository
{
}