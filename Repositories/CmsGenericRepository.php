<?php
namespace SE\AdminBundle\Repositories;

use SE\AdminBundle\Repositories\Model\AbstractCmsGenericRepository;

/**
 * Generic repository
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class CmsGenericRepository extends AbstractCmsGenericRepository
{
}