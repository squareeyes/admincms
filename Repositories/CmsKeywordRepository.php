<?php
namespace SE\AdminBundle\Repositories;

use SE\AdminBundle\Repositories\Model\AbstractCmsKeywordRepository;

/**
 * Repository pour les requêtes sur les keywords
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class CmsKeywordRepository extends AbstractCmsKeywordRepository
{
}