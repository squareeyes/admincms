<?php
namespace SE\AdminBundle\Repositories;

use SE\AdminBundle\Repositories\Model\AbstractCmsCategoryRepository;

/**
 * Repository pour les requêtes sur les catégories
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class CmsCategoryRepository extends AbstractCmsCategoryRepository
{
}