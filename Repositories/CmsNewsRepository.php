<?php
namespace SE\AdminBundle\Repositories;

use SE\AdminBundle\Repositories\Model\AbstractCmsNewsRepository;

/**
 * Gestion des news
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class CmsNewsRepository extends AbstractCmsNewsRepository
{
}