<?php
namespace SE\AdminBundle\Repositories;

use SE\AdminBundle\Repositories\Model\AbstractCmsNewsCategorieRepository;

/**
 * News category repository
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class CmsNewsCategorieRepository extends AbstractCmsNewsCategorieRepository
{
}