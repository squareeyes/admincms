<?php
namespace SE\AdminBundle\Repositories;
use SE\AdminBundle\Repositories\Model\AbstractCmsCommentRepository;

/**
 * CmsCommentRepository
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class CmsCommentRepository extends AbstractCmsCommentRepository
{
}