<?php
namespace SE\AdminBundle\Repositories;
use SE\AdminBundle\Repositories\Model\AbstractCmsBanqueImageRepository;

/**
 * Banque image repository
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class CmsBanqueImageRepository extends AbstractCmsBanqueImageRepository
{
}