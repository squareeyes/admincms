<?php
namespace SE\AdminBundle\Repositories;

use SE\AdminBundle\Repositories\Model\AbstractCmsPrestationRepository;

/**
 * CmsPrestationRepository
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class CmsPrestationRepository extends AbstractCmsPrestationRepository
{
}