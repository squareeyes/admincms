<?php
namespace SE\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\OptionsResolver\OptionsResolverInterface,
    Doctrine\ORM\EntityRepository;

class SlideshowType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $time = array('0.5' => '0.5 sec', 1 => '1 sec', 2 => '2 sec', 3 => '3 sec', 4 => '4 sec', 5 => '5 sec', 6 => '6 sec', 7 => '7 sec', 8 => '8 sec', 9 => '9 sec', 10 => '10 sec');
        $builder
            ->add('effect', 'choice', array(
                'choices' => array(
                    'fade'            => 'Fondu',
                    'kenburns'        => 'Ken burns',
                    'reversekenburns' => 'Ken burns inverse',
                    'panelx2'         => 'Double panneau',
                    'panelx3'         => 'Triple panneau',
                    'slideleft'       => 'Slide gauche',
                    'slideright'      => 'Slide right',
                    'slideparallax'   => 'Slide horizontal parallax',
                    'slideupparallax' => 'Slide vertical parallax',
                    'puffup'          => 'Puff up',
                    'cartoon'         => 'Grossisement'
                ),
                'multiple' => false,
                'attr' => array('class' => 'span12'),
                'expanded' => false
            ))
            ->add('delayanim', 'choice', array(
                'choices' => $time,
                'multiple' => false,
                'attr' => array('class' => 'span12'),
                'expanded' => false
            ))
            ->add('delayshow', 'choice', array(
                'choices' => $time,
                'multiple' => false,
                'attr' => array('class' => 'span12'),
                'expanded' => false
            ))
    	;
    }

    public function getName()
    {
        return 'se_adminbundle_slideshowtype';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
    	$resolver->setDefaults(array(
        	'data_class' => 'SE\AdminBundle\Entity\CmsSlideshow'
    	));
    }
}