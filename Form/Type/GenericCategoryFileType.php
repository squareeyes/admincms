<?php
namespace SE\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Form type
 */
class GenericCategoryFileType extends AbstractType
{
    /**
     * Build form
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array('required' => true, 'label' => 'Titre de la catégorie : ', 'attr' => array('placeholder' => 'Titre de la catégorie ...', 'class' => 'span12')))
            ->add('content', 'textarea', array('required' => false, 'label' => 'Description : ', 'attr' => array('placeholder' => 'Description ...', 'class' => 'span12', 'style' => 'height: 150px;')));
    }

    /**
     * Get name of fields
     * @return string
     */
    public function getName()
    {
        return 'se_adminbundle_genericcategoryfiletype';
    }
    
    /**
     * Mapping with entity
     * @param OptionResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SE\AdminBundle\Entity\CmsGenericCategoryFile'
        ));
    }
}