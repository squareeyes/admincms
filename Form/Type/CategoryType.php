<?php
namespace SE\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * La classe générique pour les catégories
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class CategoryType extends AbstractType
{
    /**
     * Construction du formulaire
     * @param FormBuilderInterface $builder
     * @param Array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('image', 'choose_banque')
            ->add('libelle', 'text', array(
                'required'   => true,
                'max_length' => 60,
                'attr'       => array('class' => 'span12', 'placeholder' => 'Libellé ...')
            ));
    }

    /**
     * Nom du formulaire
     * @return string
     */
    public function getName()
    {
        return 'se_adminbundle_categorytype';
    }
    
    /**
     * Liaison du formulaire avec l'entité
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SE\AdminBundle\Entity\CmsCategory'
        ));
    }
}