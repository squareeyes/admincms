<?php
namespace SE\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Formulaire pour les paramètres du site
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class ParametersType extends AbstractType
{
    private $parameters;

    /**
     * Injection paramètres
     * @param array $parameters
     */
    public function __construct($parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * Build form
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        foreach ($this->parameters as $key => $param) {
            // string
            if ($param['type'] == 'string') {
                $builder->add($key, 'text', array(
                    'label' => $param['label'], 
                    'required' => false,
                    'attr' => array('class' => "span8")
                ));

            // text
            } elseif ($param['type'] == 'text') {
                $builder->add($key, 'textarea', array(
                    'label' => $param['label'],
                    'required' => false,
                    'attr' => array('class' => "span8", 'style' => "height: 100px;")
                ));

            // wysiwyg
            } elseif ($param['type'] == 'wysiwyg') {
                $builder->add($key, 'redactor', array(
                    'label' => $param['label'],
                    'required' => false,
                    'attr' => array('class' => "span8")
                ));
            
            // choices
            } elseif ($param['type'] == 'choices') {
                $options = array();
                foreach ($param['options'] as $opt) {
                    $options[$opt] = $opt;
                }
                $builder->add($key, 'choice', array(
                    'choices' => $options,
                    'multiple' => false,
                    'expanded' => false,
                    'label' => $param['label'],
                    'required' => true
                ));
            
            // boolean
            } elseif ($param['type'] == 'boolean') {
                $builder->add($key, 'choice', array(
                    'choices' => array(1 => 'Oui', 0 => 'Non'),
                    'multiple' => false,
                    'expanded' => true,
                    'label' => $param['label'],
                    'required' => true
                ));

            // image
            } else if ($param['type'] == 'image') {
                $builder->add($key, 'choose_banque', array(
                    'label' => $param['label'],
                    'required' => false 
                ));
            }
        }
    }

    /**
     * Get name of fields
     * @return string
     */
    public function getName()
    {
        return 'se_adminbundle_parametertype';
    }
}