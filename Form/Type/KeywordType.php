<?php
namespace SE\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\OptionsResolver\OptionsResolverInterface,
    Doctrine\ORM\EntityRepository;

/**
 * Le formulaire pour les keywords
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class KeywordType extends AbstractType
{
    /**
     * Construction du formulaire
     * @param FormBuilderInterface $builder
     * @param Array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('submitedImg', 'file', array('required' => false))
            ->add('title', 'text', array(
                'required'   => true,
                'max_length' => 150,
                'attr'       => array('class' => 'span12', 'placeholder' => 'Libellé ...')
            ))
            ->add('url', 'text', array(
                'required' => false,
                'attr'     => array('class' => 'span12', 'placeholder' => 'Lien ...')
            ))
            ->add('categories', 'entity', array(
                'class' => 'SEAdminBundle:CmsCategory',
                'property' => 'libelle',
                'multiple' => true,
                'label'     => 'Catégories : ',
                'attr' => array('class' => 'chosen', 'data-placeholder' => 'Choisir une catégorie ...'),
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('cat')
                              ->orderBy('cat.libelle', 'ASC');
                }
            ));
    }

    /**
     * Nom du formulaire
     * @return string
     */
    public function getName()
    {
        return 'se_adminbundle_keywordtype';
    }
    
    /**
     * Liaison du formulaire avec l'entité
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SE\AdminBundle\Entity\CmsKeyword'
        ));
    }
}