<?php
namespace SE\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\OptionsResolver\OptionsResolverInterface,
    Doctrine\ORM\EntityRepository;

class NewsletterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array('required' => true))
            ->add('sendtoall')
            ->add('submittedImage', 'file', array('required' => false))
            ->add('mailinglist', 'entity', array(
                'class'    => 'SEAdminBundle:CmsNewsletterMail',
                'property' => 'email', 
                'multiple' => true,
                'required' => false,
                'attr'     => array(
                    'class' => 'chosen',
                    'data-placeholder' => 'Emails ...'
                ),
            ))
            ->add('template', 'entity', array(
                'class' => 'SEAdminBundle:CmsNewsletterTemplate',
                'property' => 'title',
                'multiple' => false
            ))
            ->add('content', 'redactor', array('required' => false))
        ;
    }

    public function getName()
    {
        return 'se_adminbundle_newstype';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SE\AdminBundle\Entity\CmsNewsletter'
        ));
    }
}