<?php
namespace SE\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\OptionsResolver\OptionsResolverInterface;

class VideoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
        	->add('submitedMp4', 'file', array('required' => true, 'attr' => array('data-class' => 'span12')))
            ->add('submitedOgg', 'file', array('required' => true, 'attr' => array('data-class' => 'span12')))
            ->add('submitedWebm', 'file', array('required' => true, 'attr' => array('data-class' => 'span12')))
    	;
    }

    public function getName() {
        return 'se_adminbundle_videotype';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
    	$resolver->setDefaults(array(
        	'data_class' => 'SE\AdminBundle\Entity\CmsVideo'
    	));
    }
}