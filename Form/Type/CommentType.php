<?php
namespace SE\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Form type
 */
class CommentType extends AbstractType
{
    /**
     * Build form
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', 'textarea', array('required' => true, 'label' => 'Commentaires : ', 'attr' => array('style' => 'width: 80%; height: 130px;')))
            ->add('username', 'text', array('required' => false, 'label' => 'Nom (facultatif) : ', 'attr' => array('style' => 'width: 80%;')))
            ->add('email', 'text', array('required' => false, 'label' => 'Email (facultatif) : ', 'attr' => array('style' => 'width: 80%;')));
    }

    /**
     * Get name of fields
     * @return string
     */
    public function getName()
    {
        return 'se_adminbundle_comenttype';
    }
    
    /**
     * Mapping with entity
     * @param OptionResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SE\AdminBundle\Entity\CmsComment'
        ));
    }
}