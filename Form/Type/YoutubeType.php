<?php
namespace SE\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Form type
 */
class YoutubeType extends AbstractType
{
    /**
     * Build form
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('image', 'choose_banque')
            ->add('title', 'text', array('required' => false, 'label' => 'Titre : ', 'attr' => array('placeholder' => 'Titre ...', 'class' => 'span12')))
            ->add('url', 'text', array('required' => true, 'label' => 'Youtube url : ', 'attr' => array('placeholder' => 'Lien Youtube ...', 'class' => 'span12')));
    }

    /**
     * Get name of fields
     * @return string
     */
    public function getName()
    {
        return 'se_adminbundle_youtubetype';
    }
    
    /**
     * Mapping with entity
     * @param OptionResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SE\AdminBundle\Entity\CmsYoutube'
        ));
    }
}