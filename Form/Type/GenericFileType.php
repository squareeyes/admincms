<?php
namespace SE\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

/**
 * Form type
 */
class GenericFileType extends AbstractType
{
    /**
     * Build form
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('submittedFile', 'file', array('required' => false))
            ->add('title', 'text', array('required' => true, 'label' => 'Titre : ', 'attr' => array('placeholder' => 'Titre ...', 'class' => 'span12')))
            ->add('urlweb', 'text', array('required' => false, 'label' => 'Lien WEB : ', 'attr' => array('placeholder' => 'Lien WEB ...', 'class' => 'span12')))
            ->add('category', 'entity', array(
                'class' => 'SEAdminBundle:CmsGenericCategoryFile',
                'property' => 'title',
                'required' => false,
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('obj')
                              ->orderBy('obj.title', 'ASC');
                }
            ));
    }

    /**
     * Get name of fields
     * @return string
     */
    public function getName()
    {
        return 'se_adminbundle_genericfiletype';
    }
    
    /**
     * Mapping with entity
     * @param OptionResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SE\AdminBundle\Entity\CmsGenericFile'
        ));
    }
}