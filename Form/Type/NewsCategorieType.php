<?php
namespace SE\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NewsCategorieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle', 'text', array(
            	'max_length' => 150,
            	'required' => true,
            	'attr' => array('class' => 'span12', 'placeholder' => 'Libellé de la catégorie ...')
            ))
    	;
    }

    public function getName()
    {
        return 'se_adminbundle_newscategorietype';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
    	$resolver->setDefaults(array(
        	'data_class' => 'SE\AdminBundle\Entity\CmsNewsCategorie'
    	));
    }
}