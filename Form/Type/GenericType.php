<?php
namespace SE\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

/**
 * Form type
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class GenericType extends AbstractType
{
    private $config;
    private $configkey;

    /**
     * Constructor
     * @param array  $config
     * @param string $configkey
     */
    public function __construct($config, $configkey)
    {
        $this->config    = $config;
        $this->configkey = $configkey;
    }

    /**
     * Build form
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', 'text', array(
            'max_length' => 150,
            'required'   => true,
            'attr'       => array('class' => 'span8'),
            'label'      => 'Titre : '
        ));

        $displayed = $this->config[$this->configkey]['displayed'];
        if (array_key_exists('content', $displayed) && $displayed['content']) {
            $builder->add('content', 'redactor', array(
                'label' => 'Description : '
            ));
        }
        if (array_key_exists('image', $displayed) && $displayed['image']) {
            $builder->add('image', 'choose_banque');
        }
        if (array_key_exists('date', $displayed) && $displayed['date']) {
            $builder->add('datepost', 'datepicker', array('attr' => array('placeholder' => 'Date ...', 'label' => 'Date : ')));
        }

        if (array_key_exists('mappedmany', $this->config[$this->configkey]) && $this->config[$this->configkey]['mappedmany'] != "") {
            $mappedmany = $this->config[$this->configkey]['mappedmany'];
            $builder->add('genericsparents', 'entity', array(
                'class'    => 'SEAdminBundle:CmsGeneric',
                'property' => 'title', 
                'multiple' => true,
                'required' => false,
                'label'    => $this->config[$this->config[$this->configkey]['mappedmany']]['title'].' : ',
                'attr'     => array(
                    'class' => 'chosen',
                    'data-placeholder' => $this->config[$this->config[$this->configkey]['mappedmany']]['title'].' ...'
                ),
                'query_builder' => function(EntityRepository $er) use ($mappedmany) {
                    return $er->createQueryBuilder('g')
                              ->where('g.configkey = :mappedmany')
                              ->setParameter('mappedmany', $mappedmany)
                              ->orderBy('g.title', 'ASC');
                }
            ));
        }

        $fields = $this->config[$this->configkey]['fields'];
        if ($fields && count($fields)) {
            $builder->add('submitedFields', new ParametersType($fields));
        }
    }

    /**
     * Get name of fields
     * @return string
     */
    public function getName()
    {
        return 'se_adminbundle_generictype';
    }
    
    /**
     * Mapping with entity
     * @param OptionResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SE\AdminBundle\Entity\CmsGeneric'
        ));
    }
}