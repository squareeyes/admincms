<?php
namespace SE\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BanqueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
        	->add('submitedImg', 'file', array('required' => false))
            ->add('titre', 'text', array(
            	'required' => false,
            	'attr' => array('class' => 'span8')
            ))
        	->add('keywords', 'text', array(
        		'required' => false,
        		'attr' => array('class' => 'span8')
        	))
    	;
    }

    public function getName() {
        return 'se_adminbundle_banquetype';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
    	$resolver->setDefaults(array(
        	'data_class' => 'SE\AdminBundle\Entity\CmsBanqueImage'
    	));
    }
}