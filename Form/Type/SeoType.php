<?php
namespace SE\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\OptionsResolver\OptionsResolverInterface,
    Doctrine\ORM\EntityRepository;

class SeoType extends AbstractType
{
    private $content;
    private $image;

    /**
     * Filtre pour savoir si on permet l'ajout d'image ou de contenu
     * @param boolean $image
     * @param boolean $content
     */
    public function __construct($image = false, $content = false)
    {
        $this->image = $image;
        $this->content = $content;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($this->image) {
            $builder->add('image', 'choose_banque');
        }
        if ($this->content) {
            $builder->add('content', 'redactor', array('required' => false));
        }
        $builder->add('title', 'text', array(
            'required' => true,
            'attr' => array('class' => 'span12', 'placeholder' => 'Titre ...', 'style' => 'margin: 0px;'),
            'max_length' => 150
        ));
        $builder->add('description', 'text', array(
            'required' => true,
            'attr' => array('class' => 'span12', 'placeholder' => 'Description ...', 'style' => 'margin: 0px;'),
            'max_length' => 250
        ));
    }

    public function getName()
    {
        return 'se_adminbundle_seotype';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SE\AdminBundle\Entity\CmsSeo'
        ));
    }
}