<?php
namespace SE\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NewsCategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle', 'text', array(
                'required'   => true,
                'max_length' => 60,
                'attr'       => array('class' => 'span12')
            ));
    }

    public function getName()
    {
        return 'se_adminbundle_newscategorytype';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SE\AdminBundle\Entity\CmsNewsCategorie'
        ));
    }
}