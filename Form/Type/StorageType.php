<?php
namespace SE\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\OptionsResolver\OptionsResolverInterface;

class StorageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
        	->add('submitedFile', 'file', array('required' => true, 'attr' => array('data-class' => 'span12')))
            ->add('title', 'text', array(
                'max_length' => 150,
                'required' => true,
                'attr' => array('class' => 'span12', 'placeholder' => 'Titre ...')
            ))
    	;
    }

    public function getName() {
        return 'se_adminbundle_storagetype';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
    	$resolver->setDefaults(array(
        	'data_class' => 'SE\AdminBundle\Entity\CmsStorage'
    	));
    }
}