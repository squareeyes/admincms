<?php
namespace SE\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\OptionsResolver\OptionsResolverInterface,
    Doctrine\ORM\EntityRepository;

class NewsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('image', 'choose_banque')
            ->add('datepost', 'datepicker', array('attr' => array('placeholder' => 'Date de publication ...')))
            ->add('title', 'text', array(
            	'max_length' => 150,
            	'required' => true,
            	'attr' => array('class' => 'span8')
            ))
            ->add('categories', 'entity', array(
                'class' => 'SEAdminBundle:CmsNewsCategorie',
                'property' => 'libelle',
                'multiple' => true,
                'attr' => array('class' => 'chosen', 'data-placeholder' => 'Choisir une catégorie ...'),
                'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('cat')
                              ->orderBy('cat.libelle', 'ASC');
                }
            ))
            ->add('content', 'redactor', array('required' => false))
    	;
    }

    public function getName()
    {
        return 'se_adminbundle_newstype';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
    	$resolver->setDefaults(array(
        	'data_class' => 'SE\AdminBundle\Entity\CmsNews'
    	));
    }
}