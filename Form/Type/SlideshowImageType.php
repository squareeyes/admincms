<?php
namespace SE\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\OptionsResolver\OptionsResolverInterface,
    Doctrine\ORM\EntityRepository;

class SlideshowImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('image', 'choose_banque')
            ->add('title', 'text', array(
                'max_length' => 150,
                'required' => false,
                'attr' => array('class' => 'span12', 'placeholder' => 'Titre ...')
            ))
            ->add('url', 'text', array(
                'required' => false,
                'attr' => array('class' => 'span12', 'placeholder' => 'Lien ...')
            ))
            ->add('content', 'redactor', array('required' => false))
    	;
    }

    public function getName()
    {
        return 'se_adminbundle_slideshowimagetype';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
    	$resolver->setDefaults(array(
        	'data_class' => 'SE\AdminBundle\Entity\CmsSlideshowImage'
    	));
    }
}