<?php
namespace SE\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\OptionsResolver\OptionsResolverInterface,
    Doctrine\ORM\EntityRepository;

/**
 * Ajout et modification des prestations
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class PrestationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('image', 'choose_banque')
            ->add('title', 'text', array(
                'max_length' => 150,
                'required'   => true,
                'attr'       => array('class' => 'span8')
            ))
            ->add('active', 'checkbox', array(
                'label'     => 'En ligne : ',
                'required'  => false
            ))
            ->add('categories', 'entity', array(
                'class' => 'SEAdminBundle:CmsCategory',
                'property' => 'libelle',
                'multiple' => true,
                'label'     => 'Catégories : ',
                'attr' => array('class' => 'chosen', 'data-placeholder' => 'Choisir une catégorie ...'),
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('cat')
                              ->orderBy('cat.libelle', 'ASC');
                }
            ))
            ->add('content', 'redactor');
    }

    /**
     * Attribut un nom au formulaire
     * @return string
     */
    public function getName()
    {
        return 'se_adminbundle_prestationtype';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SE\AdminBundle\Entity\CmsPrestation'
        ));
    }
}