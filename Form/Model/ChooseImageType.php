<?php
namespace SE\AdminBundle\Form\Model;
use Symfony\Component\Form\FormInterface,
	Symfony\Component\Form\FormView,
	Symfony\Component\Form\AbstractType,
	Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ChooseImageType extends AbstractType
{
	public function setDefaultOptions(OptionsResolverInterface $resolver){
        $resolver->setDefaults(array(
            'class' => 'SE\\AdminBundle\\Entity\\CmsBanqueImage',
            'label' => "Ouvrir la banque d'image"
        ));
    }

    public function buildView(FormView $view, FormInterface $form, array $options){
        return;
    }

    public function getName() {
        return 'choose_banque';
    }
    
    public function getParent() {
        return 'entity';
    }
}