<?php
namespace SE\AdminBundle\Form\Model;
use Symfony\Component\Form\FormInterface,
	Symfony\Component\Form\FormView,
	Symfony\Component\Form\AbstractType,
	Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RedactorType extends AbstractType
{
	public function setDefaultOptions(OptionsResolverInterface $resolver){
        $resolver->setDefaults(array(
            'label' => ""
        ));
    }

    public function buildView(FormView $view, FormInterface $form, array $options){
        return;
    }

    public function getName() {
        return 'redactor';
    }
    
    public function getParent() {
        return 'textarea';
    }
}