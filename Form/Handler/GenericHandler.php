<?php
namespace SE\AdminBundle\Form\Handler;

use SE\AdminBundle\Entity\CmsGenericField;
use SE\AdminBundle\Entity\CmsSlideshow;

/**
 * Form handler
 */
class GenericHandler
{
    private $req;
    private $entityManager;
    private $configkey;
    private $slugify;

    /**
     * Constructor
     * @param Request       $request
     * @param EntityManager $entityManager
     * @param Slugify       $slugify
     */
    public function __construct($request, $entityManager, $slugify)
    {
        $this->req = $request;
        $this->entityManager = $entityManager;
        $this->slugify = $slugify;
    }

    /**
     * Check form validity
     * @param Form   $form
     * @param string $configkey
     * @return Generic
     */
    public function process($form, $configkey)
    {
        $this->configkey = $configkey;
        if ($this->req->getMethod() == 'POST') {
            $form->bind($this->req);
            if ($form->isValid()) {
                return $this->onSuccess($form->getData()); 
            }
        }

        return false;
    }
    
    /**
     * Persist entity
     * @param Generic $data
     * @return Generic
     */
    protected function onSuccess($data)
    {
        if ($data->getSlideshow() == null) {
            $slideshow = new CmsSlideshow();
            $data->setSlideshow($slideshow);
        }

        if (is_array($data->getSubmitedFields())) {
            foreach ($data->getSubmitedFields() as $key => $field) {
                $objectField = $data->getField($key);
                if ($objectField === false) {
                    $objectField = new CmsGenericField();
                    $objectField->setFieldkey($key);
                    $objectField->setGeneric($data);
                } else {
                    $objectField = $this->entityManager->getRepository('SEAdminBundle:CmsGenericField')->findOneByFieldKeyAndRefGen($key, $data->getId());
                }
                
                if (is_object($field)) {
                    $objectField->setImage($field);
                    $objectField->setFieldvalue($field->getId());
                } else {
                    $objectField->setFieldvalue($field);
                }
                $this->entityManager->persist($objectField);
            }
        }

        $data->setConfigkey($this->configkey);
        $slug = $this->slugify->slug($data->getTitle());
        $data->setSlug($slug);
        $this->entityManager->persist($data);
        $this->entityManager->flush();

        return $data;
    }
}