<?php
namespace SE\AdminBundle\Form\Handler;

/**
 * Traitement du formulaire pour les diaporamas
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class SeoHandler
{
    private $req;
    private $entityManager;

    /**
     * Injection des services
     * @param Request       $request
     * @param EntityManager $entityManager
     */
    public function __construct($request, $entityManager)
    {
        $this->req           = $request;
        $this->entityManager = $entityManager;
    }

    /**
     * Test si le formulaire a été envoyé et si il est valide
     * @param FormBuilder $form
     * @return CmsSeo
     */
    public function process($form)
    {
        if ($this->req->getMethod() == 'POST') {
            $form->bind($this->req);
            if ($form->isValid()) {
                return $this->onSuccess($form->getData()); 
            }
        }

        return false;
    }
    
    /**
     * On persist les données dans la BDD
     * @param CmsSeo $data
     * @return CmsSeo
     */
    protected function onSuccess($data)
    {
        $this->entityManager->persist($data);
        $this->entityManager->flush();

        return $data;
    }
}