<?php
namespace SE\AdminBundle\Form\Handler;
use SE\AdminBundle\Entity\CmsSlideshow;
use SE\AdminBundle\Entity\CmsVideo;

class PrestationHandler
{
    private $req;
    private $em;
    private $slugify;

    public function __construct($request, $em, $slugify)
    {
        $this->req = $request;
        $this->em = $em;
        $this->slugify = $slugify;
    }

    public function process($form)
    {
        if ($this->req->getMethod() == 'POST') {
            $form->bind($this->req);
            if ($form->isValid()) {
                return $this->onSuccess($form->getData()); 
            }
        }

        return false;
    }
 
    protected function onSuccess($data)
    {
        if(\is_object($data->getImage())) {
            if ($data->getSlideshow() == null) {
                $slideshow = new CmsSlideshow();
                $data->setSlideshow($slideshow);
            }
            if ($data->getVideo() == null) {
                $video = new CmsVideo();
                $data->setVideo($video);
            }
	        $slug = $this->slugify->slug($data->getTitle());
	        $data->setSlug($slug);
	        $this->em->persist($data);
	        $this->em->flush();

	        return $data;
        }

        return false;
    }
}