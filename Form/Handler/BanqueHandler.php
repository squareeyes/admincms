<?php
namespace SE\AdminBundle\Form\Handler;

/**
 * Traitement du formulaire pour la banque d'image
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class BanqueHandler
{
    private $req;
    private $entityManager;
    private $sefile;
    private $liip;

    /**
     * Injection des services nécessaires
     * @param Request        $request
     * @param EntityManager  $entityManager
     * @param File           $file
     * @param LiipController $liip
     */
    public function __construct($request, $entityManager, $file, $liip)
    {
        $this->req = $request;
        $this->entityManager = $entityManager;
        $this->sefile = $file;
        $this->liip = $liip;
    }

    /**
     * Test si le formulaire a été envoyé et si il est valide
     * @param FormBuilder $form
     * @return CmsBanqueImage
     */
    public function process($form)
    {
        if ($this->req->getMethod() == 'POST') {
            $form->bind($this->req);
            if ($form->isValid()) {
                return $this->onSuccess($form->getData()); 
            }
        }

        return false;
    }
    
    /**
     * On persist les données dans la BDD
     * @param CmsBanqueImage $data
     * @return CmsBanqueImage
     */
    protected function onSuccess($data)
    {
        // Si une image est chargée, on l'upload et modifi le champ urlimg avec l'url
        if (is_object($data->getSubmitedImg())) {
            if ($data->getUrlimg() != null && file_exists($data->getUrlimg())) {
                unlink($data->getUrlimg());
                unlink('media/admin/seadmin_200x120/'.$data->getUrlimg());
            }
            $urlimg = $this->sefile->upload($data->getSubmitedImg(), 'upload/banque/');
            $data->setUrlimg($urlimg);
            $data->eraseSubmitedImg();
        }
        if ($data->getUrlimg() != "") {
            //Création de la mini
            $this->liip->filterAction($this->req, $data->getUrlimg(), 'seadmin_200x120');
            if (!is_dir(dirname('media/admin/seadmin_200x120/'.$urlimg))) {
                mkdir(dirname('media/admin/seadmin_200x120/'.$urlimg), 0777, true);
            }
            copy('media/cache/seadmin_200x120/'.$urlimg, 'media/admin/seadmin_200x120/'.$urlimg);
            $this->entityManager->persist($data);
            $this->entityManager->flush();

            return $data;
        }

        return false;
    }
}