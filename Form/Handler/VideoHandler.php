<?php
namespace SE\AdminBundle\Form\Handler;

/**
 * Traitement du formulaire pour les video
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class VideoHandler
{
    private $req;
    private $entityManager;
    private $sefile;

    /**
     * Injection des services nécessaires
     * @param Request       $request
     * @param EntityManager $entityManager
     * @param File          $file
     */
    public function __construct($request, $entityManager, $file)
    {
        $this->req = $request;
        $this->entityManager = $entityManager;
        $this->sefile = $file;
    }

    /**
     * Test si le formulaire a été envoyé et si il est valide
     * @param FormBuilder $form
     * @return CmsVideo
     */
    public function process($form)
    {
        if ($this->req->getMethod() == 'POST') {
            $form->bind($this->req);
            if ($form->isValid()) {
                return $this->onSuccess($form->getData()); 
            }
        }

        return false;
    }
    
    /**
     * On persist les données dans la BDD
     * @param CmsVideo $data
     * @return CmsVideo
     */
    protected function onSuccess($data)
    {
        // Si une video est chargée
        if (is_object($data->getSubmitedMp4()) && is_object($data->getSubmitedOgg()) && is_object($data->getSubmitedWebm())) {
            $urlmp4 = $this->sefile->upload($data->getSubmitedMp4(), 'upload/video/');
            $urlogg = $this->sefile->upload($data->getSubmitedOgg(), 'upload/video/');
            $urlwebm = $this->sefile->upload($data->getSubmitedWebm(), 'upload/video/');
            // Et que les fichiers sont uploads
            if ($urlmp4 && $urlogg && $urlwebm) {
                // On supprime les anciennes vidéo
                if ($data->getUrlmp4() != null && file_exists($data->getUrlmp4())) {
                    unlink($data->getUrlmp4());
                }
                if ($data->getUrlogg() != null && file_exists($data->getUrlogg())) {
                    unlink($data->getUrlogg());
                }
                if ($data->getUrlwebm() != null && file_exists($data->getUrlwebm())) {
                    unlink($data->getUrlwebm());
                }
                // On sauvegarde les liens vers les nouvelles vidéos
                $data->setUrlmp4($urlmp4);
                $data->setUrlogg($urlogg);
                $data->setUrlwebm($urlwebm);
                $this->entityManager->persist($data);
                $this->entityManager->flush();
                $data->eraseSubmitedVideos();

                return $data;
            }
        }

        return false;
    }
}