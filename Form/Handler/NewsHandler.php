<?php
namespace SE\AdminBundle\Form\Handler;

/**
 * Traitement du formulaire pour les actualités
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class NewsHandler
{
    private $req;
    private $entityManager;
    private $slugify;

    /**
     * Injection des services
     * @param Request       $request
     * @param EntityManager $entityManager
     * @param Slugify       $slugify
     */
    public function __construct($request, $entityManager, $slugify)
    {
        $this->req           = $request;
        $this->entityManager = $entityManager;
        $this->slugify       = $slugify;
    }

    /**
     * Test si le formulaire a été envoyé et si il est valide
     * @param FormBuilder $form
     * @return CmsNews
     */
    public function process($form)
    {
        if ($this->req->getMethod() == 'POST') {
            $form->bind($this->req);
            if ($form->isValid()) {
                return $this->onSuccess($form->getData()); 
            }
        }

        return false;
    }
    
    /**
     * On persist les données dans la BDD
     * @param CmsNews $data
     * @return CmsNews
     */
    protected function onSuccess($data) {
    	if (\is_object($data->getImage())) {
	    	$slug = $this->slugify->slug($data->getTitle());
	    	$data->setSlug($slug);
	        $this->entityManager->persist($data);
	        $this->entityManager->flush();
	        return $data;
    	}

    	return false;
    }
}