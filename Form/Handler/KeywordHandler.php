<?php
namespace SE\AdminBundle\Form\Handler;

/**
 * Traitement pour l'ajout et la modification de keywords
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class KeywordHandler
{
    private $request;
    private $entityManager;
    private $sefile;

    /**
     * Constructeur
     * @param Request       $request
     * @param EntityManager $entityManager
     * @param File          $file
     */
    public function __construct($request, $entityManager, $file)
    {
        $this->request       = $request;
        $this->entityManager = $entityManager;
        $this->sefile        = $file;
    }

    /**
     * Test si le formulaire est valide
     * @param FormBuilder $form
     * @return CmsCategory
     */
    public function process($form)
    {
        if ($this->request->getMethod() == 'POST') {
            $form->bind($this->request);
            if ($form->isValid()) {
                $data = $form->getData();

                return $this->onSuccess($data); 
            }
        }

        return false;
    }
 
    protected function onSuccess($data)
    {
        if (is_object($data->getSubmitedImg())) {
            if ($data->getUrlimg() != null && file_exists($data->getUrlimg())) {
                unlink($data->getUrlimg());
            }
            $urlimg = $this->sefile->upload($data->getSubmitedImg(), 'upload/keywords/');
            $data->setUrlimg($urlimg);
            $data->eraseSubmitedImg();
        }

        $this->entityManager->persist($data);
        $this->entityManager->flush();

        return $data;
    }
}