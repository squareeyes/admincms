<?php
namespace SE\AdminBundle\Form\Handler;

/**
 * Traitement du formulaire pour les newsletters
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class NewsletterImageHandler
{
    private $req;
    private $entityManager;

    /**
     * Injection des services
     * @param Request       $request
     * @param EntityManager $entityManager
     * @param File          $sefile
     */
    public function __construct($request, $entityManager, $sefile)
    {
        $this->req           = $request;
        $this->entityManager = $entityManager;
        $this->sefile        = $sefile;
    }

    /**
     * Test si le formulaire a été envoyé et si il est valide
     * @param FormBuilder $form
     * @return CmsNewsletter
     */
    public function process($form)
    {
        if ($this->req->getMethod() == 'POST' && $this->req->request->get('se_adminbundle_newsimagetype')) {
            $form->bind($this->req);
            if ($form->isValid()) {
                return $this->onSuccess($form->getData()); 
            }
        }

        return false;
    }
    
    /**
     * On persist les données dans la BDD
     * @param CmsNewsletter $data
     * @return CmsNewsletter
     */
    protected function onSuccess($data) {
        // Si une image est chargée, on l'upload et modifi le champ urlimg avec l'url
        if (is_object($data->getSubmittedImage())) {
            if ($data->getUrlimg() != null && file_exists($data->getUrlimg())) {
                unlink($data->getUrlimg());
            }
            $urlimg = $this->sefile->upload($data->getSubmittedImage(), 'upload/email/');
            $data->setUrlimg($urlimg);
        }

        $this->entityManager->persist($data);
        $this->entityManager->flush();
        
        return $data;
    }
}