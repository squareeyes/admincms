<?php
namespace SE\AdminBundle\Form\Handler;

/**
 * Traitement du formulaire pour les fichiers
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class StorageHandler
{
    private $req;
    private $entityManager;
    private $sefile;

    /**
     * Injection des services nécessaires
     * @param Request       $request
     * @param EntityManager $entityManager
     * @param File          $file
     */
    public function __construct($request, $entityManager, $file)
    {
        $this->req = $request;
        $this->entityManager = $entityManager;
        $this->sefile = $file;
    }

    /**
     * Test si le formulaire a été envoyé et si il est valide
     * @param FormBuilder $form
     * @return CmsStorage
     */
    public function process($form)
    {
        if ($this->req->getMethod() == 'POST') {
            $form->bind($this->req);
            if ($form->isValid()) {
                return $this->onSuccess($form->getData()); 
            }
        }

        return false;
    }
    
    /**
     * On persist les données dans la BDD
     * @param CmsStorage $data
     * @return CmsStorage
     */
    protected function onSuccess($data)
    {
        // Si un fichier est chargé
        if (is_object($data->getSubmitedFile())) {
            $urlfile = $this->sefile->upload($data->getSubmitedFile(), 'upload/storage/');
            // Et que le fichier est upload
            if ($urlfile) {
                // On sauvegarde le lien vers le fichier
                $data->setUrlfile($urlfile);
                $this->entityManager->persist($data);
                $this->entityManager->flush();
                $data->eraseSubmitedFile();

                return $data;
            }
        }

        return false;
    }
}