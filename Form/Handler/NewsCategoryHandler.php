<?php
namespace SE\AdminBundle\Form\Handler;

class NewsCategoryHandler
{
    private $request;
    private $entityManager;
    private $slugify;

    public function __construct($request, $entityManager, $slugify)
    {
		$this->request       = $request;
		$this->entityManager = $entityManager;
		$this->slugify       = $slugify;
    }

    public function process($form)
    {
        if ($this->request->getMethod() == 'POST') {
            $form->bind($this->request);
            if ($form->isValid()) {
                $data = $form->getData();

                return $this->onSuccess($data); 
            }
        }

        return false;
    }
 
    protected function onSuccess($data)
    {
    	$slug = $this->slugify->slug($data->getLibelle());
    	$data->setSlug($slug);
        $this->entityManager->persist($data);
        $this->entityManager->flush();

        return $data;
    }
}