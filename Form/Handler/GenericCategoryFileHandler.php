<?php
namespace SE\AdminBundle\Form\Handler;

use SE\AdminBundle\Entity\CmsGenericField;
use SE\AdminBundle\Entity\CmsSlideshow;

/**
 * Form handler
 */
class GenericCategoryFileHandler
{
    private $req;
    private $entityManager;

    /**
     * Constructor
     * @param Request       $request
     * @param EntityManager $entityManager
     */
    public function __construct($request, $entityManager)
    {
        $this->req = $request;
        $this->entityManager = $entityManager;
    }

    /**
     * Check form validity
     * @param Form   $form
     * @return GenericCategoryFile
     */
    public function process($form)
    {
        if ($this->req->getMethod() == 'POST') {
            $form->bind($this->req);
            if ($form->isValid()) {
                return $this->onSuccess($form->getData()); 
            }
        }

        return false;
    }
    
    /**
     * Persist entity
     * @param GenericCategoryFile $data
     * @return GenericCategoryFile
     */
    protected function onSuccess($data)
    {
        $this->entityManager->persist($data);
        $this->entityManager->flush();

        return $data;
    }
}