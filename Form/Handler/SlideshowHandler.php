<?php
namespace SE\AdminBundle\Form\Handler;

/**
 * Traitement du formulaire pour les diaporamas
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class SlideshowHandler
{
    private $req;
    private $entityManager;

    /**
     * Injection des services
     * @param Request       $request
     * @param EntityManager $entityManager
     */
    public function __construct($request, $entityManager)
    {
        $this->req           = $request;
        $this->entityManager = $entityManager;
    }

    /**
     * Test si le formulaire a été envoyé et si il est valide
     * @param FormBuilder $form
     * @return CmsSlideshow
     */
    public function process($form)
    {
        if ($this->req->getMethod() == 'POST') {
            $form->bind($this->req);
            if ($form->isValid()) {
                return $this->onSuccess($form->getData()); 
            }
        }

        return false;
    }
    
    /**
     * On persist les données dans la BDD
     * @param CmsSlideshow $data
     * @return CmsSlideshow
     */
    protected function onSuccess($data)
    {
        if (method_exists($data, 'getUrl') && $data->getUrl() != null) {
            $url = $data->getUrl();
            $url = preg_replace('(http://|https://)', '', $url);
            $data->setUrl('http://'.$url);
        }

        $this->entityManager->persist($data);
        $this->entityManager->flush();

        return $data;
    }
}