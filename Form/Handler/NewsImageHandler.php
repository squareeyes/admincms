<?php
namespace SE\AdminBundle\Form\Handler;

class NewsImageHandler
{
    private $request;
    private $entityManager;

    public function __construct($request, $entityManager)
    {
		$this->request       = $request;
		$this->entityManager = $entityManager;
    }

    public function process($form, $news)
    {
        if ($this->request->getMethod() == 'POST') {
            $form->bind($this->request);
            if ($form->isValid()) {
                $data = $form->getData();
                $data->setNews($news);

                return $this->onSuccess($data); 
            }
        }

        return false;
    }
 
    protected function onSuccess($data)
    {
        $position = count($data->getNews()->getImages()) + 1;
        $data->setPosition($position);
        $this->entityManager->persist($data);
        $this->entityManager->flush();

        return $data;
    }
}