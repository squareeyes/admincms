<?php
namespace SE\AdminBundle\Form\Handler;

use SE\AdminBundle\Entity\CmsParameter;

/**
 * Gestion de la configuration des constantes
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class ParameterHandler
{
    private $params;
    private $req;
    private $entityManager;

    /**
     * Injection des services nécessaires
     * @param Array          $seadmin
     * @param Request        $request
     * @param EntityManager  $entityManager
     */
    public function __construct($seadmin, $request, $entityManager)
    {
        $this->params = array_key_exists('parameters', $seadmin['modules']) ? $seadmin['modules']['parameters'] : false;
        $this->req = $request;
        $this->entityManager = $entityManager;
    }

    /**
     * Ajout des parameters
     * @param FormBuilder $form
     * @return boolean
     */
    public function process($form)
    {
        if ($this->req->getMethod() == 'POST') {
            $form->bind($this->req);
            if ($form->isValid()) {
                return $this->onSuccess($form->getData()); 
            }
        }

        return false;
    }

    protected function onSuccess($data)
    {
        foreach ($data as $key => $value) {
            $param = $this->entityManager->getRepository('SEAdminBundle:CmsParameter')->findOneBySekey($key);
            if (!$param) {
                $param = new CmsParameter();
                $param->setSekey($key);
            }
            if (is_object($value)) {
                $param->setSevalue($value->getId());
                $param->setImage($value);
            } else {
                $param->setSevalue($value);
            }
            $this->entityManager->persist($param);
            $this->entityManager->flush();
        }

        return true;
    }
}