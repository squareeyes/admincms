<?php
namespace SE\AdminBundle\Form\Handler;

/**
 * Traitement pour l'ajout et la modification de categories
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class CategoryHandler
{
    private $request;
    private $entityManager;
    private $slugify;

    /**
     * Constructeur
     * @param Request       $request
     * @param EntityManager $entityManager
     * @param Slugify       $slugify
     */
    public function __construct($request, $entityManager, $slugify)
    {
        $this->request       = $request;
        $this->entityManager = $entityManager;
        $this->slugify       = $slugify;
    }

    /**
     * Test si le formulaire est valide
     * @param FormBuilder $form
     * @return CmsCategory
     */
    public function process($form)
    {
        if ($this->request->getMethod() == 'POST') {
            $form->bind($this->request);
            if ($form->isValid()) {
                $data = $form->getData();

                return $this->onSuccess($data); 
            }
        }

        return false;
    }
 
    protected function onSuccess($data)
    {
        $slug = $this->slugify->slug($data->getLibelle());
        $data->setSlug($slug);
        $this->entityManager->persist($data);
        $this->entityManager->flush();

        return $data;
    }
}