<?php
namespace SE\AdminBundle\Form\Handler;

/**
 * Form handler
 */
class NewsletterMailHandler
{
    private $req;
    private $entityManager;

    /**
     * Constructor
     * @param Request       $request
     * @param EntityManager $entityManager
     */
    public function __construct($request, $entityManager)
    {
        $this->req = $request;
        $this->entityManager = $entityManager;
    }

    /**
     * Check form validity
     * @param Form    $form
     * @return Comment
     */
    public function process($form)
    {
        $request = $this->req;
        if ($request->getMethod() == 'POST' && $request->get('se_adminbundle_newslettermailtype')) {
            if ($request->get('company') || $request->get('firstname') || $request->get('lastname') || $request->get('address')) {
                return false;
            }

            $form->bind($request);
            if ($form->isValid()) {
                return $this->onSuccess($form->getData()); 
            }
        }

        return false;
    }
    
    /**
     * Persist entity
     * @param Comment $data
     * @return Comment
     */
    protected function onSuccess($data)
    {
        $this->entityManager->persist($data);
        $this->entityManager->flush();

        return $data;
    }
}