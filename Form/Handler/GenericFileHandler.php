<?php
namespace SE\AdminBundle\Form\Handler;

/**
 * Form handler
 */
class GenericFileHandler
{
    private $req;
    private $entityManager;
    private $sefile;

    /**
     * Constructor
     * @param Request       $request
     * @param EntityManager $entityManager
     * @param File          $sefile
     */
    public function __construct($request, $entityManager, $sefile)
    {
        $this->req = $request;
        $this->entityManager = $entityManager;
        $this->sefile = $sefile;
    }

    /**
     * Check form validity
     * @param Form $form
     * @return CmsGenericFile
     */
    public function process($form)
    {
        if ($this->req->getMethod() == 'POST') {
            $form->bind($this->req);
            if ($form->isValid()) {
                return $this->onSuccess($form->getData()); 
            }
        }

        return false;
    }
    
    /**
     * Persist entity
     * @param CmsGenericFile $data
     * @return CmsGenericFile
     */
    protected function onSuccess($data)
    {
        // Si un fichier est chargé
        if (is_object($data->getSubmittedFile())) {
            $urlfile = $this->sefile->upload($data->getSubmittedFile(), 'upload/storage/');
            // Et que le fichier est upload
            if ($urlfile) {
                // On sauvegarde le lien vers le fichier
                $data->setUrl($urlfile);
            }
        }
        
        $this->entityManager->persist($data);
        $this->entityManager->flush();

        return $data;
    }
}