<?php
namespace SE\AdminBundle\DependencyInjection;
use Symfony\Component\HttpKernel\DependencyInjection\Extension,
    Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface,
    Symfony\Component\DependencyInjection\ContainerBuilder,
    Symfony\Component\DependencyInjection\Loader,
    Symfony\Component\Config\FileLocator;

/**
 * Ajout des configurations du bundle
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class SEAdminExtension extends Extension implements PrependExtensionInterface
{
    /**
     * Lors du chargement du Bundle
     * @param array            $configs
     * @param ContainerBuilder $container
     * @return void
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        //On charge les services
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        //On ajoute la configuration présente dans config.yml aux parameters
        $seconf = $configs[0];
        $container->setParameter('se_admin', $seconf);

        //Configuration de fos_user
        if (array_key_exists('fos_user', $seconf)) {
            $fosconfig = $seconf['fos_user'];
            if (!array_key_exists('firewall_name', $fosconfig)) {
                $fosconfig['firewall_name'] = 'main';
            }
            if (!array_key_exists('db_driver', $fosconfig)) {
                $fosconfig['db_driver'] = 'orm';
            }
            if (!array_key_exists('user_class', $fosconfig)) {
                throw new \InvalidArgumentException('User class must be defined in config.yml "se_admin:fos_user:user_class"');
            }
            $container->prependExtensionConfig('fos_user', $fosconfig);
        } else {
            throw new \InvalidArgumentException('fos_user must be defined in config.yml "se_admin:fos_user"');
        }
    
        //On ajoute les patrons de formulaire custom
        $container->setParameter('twig.form.resources', \array_merge(
            $container->getParameter('twig.form.resources'),
            array('SEAdminBundle:form:form.html.twig')
        ));
    }

    /**
     * Avant d'executé la méthode load
     * @param ContainerBuilder $container
     * @return void
     */
    public function prepend(ContainerBuilder $container)
    {
        $bundles = $container->getParameter('kernel.bundles');
        // Configuration de LiipImagineBundle
        if (isset($bundles['LiipImagineBundle'])) {
            //On crée les filtres utilisé par SEAdminBundle
            $config = array(
                'filter_sets' => array(
                    'seadmin_50x50' => array(
                        'quality' => 75,
                        'filters' => array('upscale_crop' => array('size' => array(50, 50)))
                    ),
                    'seadmin_200x120' => array(
                        'quality' => 75,
                        'filters' => array('upscale_crop' => array('size' => array(200, 120)))
                    )
                )
            );
            // Et on ajoute notre configuration au fichier config.yml
            $container->prependExtensionConfig('liip_imagine', $config);
        } else {
            throw new \InvalidArgumentException('LiipImagineBundle must be loaded !');
        }
    }
}