<?php
namespace SE\AdminBundle\Listeners;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernel;

/**
 * Détecte et redirige les vieux naviguateurs
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class OldBrowserListener
{
    private $entityManager;
    private $twig;

    /**
     * Injection des services
     * @param EntityManager $entityManager
     * @param Twig          $twig
     */
    public function __construct($entityManager, $twig)
    {
        $this->entityManager = $entityManager;
        $this->twig = $twig;
    }

    /**
     * A chaque fois qu'une requête est émise
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        // Si c'est une requête principale
        if (HttpKernel::MASTER_REQUEST != $event->getRequestType()) {
            return;
        }
        $redirect = false;

        $internetExplorer = $this->entityManager->getRepository('SEAdminBundle:CmsParameter')->findOneBySekey('oldbrowser_ie');
        // Si on est sous ie et que la version est inférieur à la configuration
        $checkie = $this->checkIE();
        //var_dump($internetExplorer->getSevalue()); exit;
        if ($internetExplorer && $internetExplorer->getSevalue() && $checkie && $checkie <= (int) $internetExplorer->getSevalue()) {
            $redirect = true;
        }

        // Si l'on doit rediriger le visiteur
        if ($redirect) {
            $event = $this->displayUpdate($event);
        }
    }

    /**
     * Retourne le numéro de la version d'internet explorer
     * @return int
     */
    private function checkIE()
    {
        if (!preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $m) || preg_match('#Opera#', $_SERVER['HTTP_USER_AGENT'])) {
            return false;
        }

        return $m[1];
    }

    /**
     * Affichage la page de mise à jour
     * @param GetResponseEvent $event
     * @return GetResponseEvent
     */
    private function displayUpdate($event)
    {
        // On check si un template est passé dans le config.yml
        $modules = $this->twig->getGlobals()['seadmin']['modules'];
        if (array_key_exists('oldbrowser', $modules) && is_array($modules['oldbrowser']) && array_key_exists('view', $modules['oldbrowser'])) {
            $template = $this->twig->render($modules['oldbrowser']['view']);
        } else {
            $template = $this->twig->render('SEAdminBundle:oldbrowser:update.html.twig');
        }

        // On configure la réponse avec notre template
        $response = new Response($template);
        $event->setResponse($response);

        return $event;
    }
}