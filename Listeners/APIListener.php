<?php
namespace SE\AdminBundle\Listeners;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernel;

/**
 * Ecouteur d'évènement sur la requête
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class APIListener
{
    private $seapi;
    private $twig;
    private $router;
    private $request;

    /**
     * Injection du service seapi
     * @param Api     $seapi
     * @param Twig    $twig
     * @param Router  $router
     * @param Request $request
     */
    public function __construct($seapi, $twig, $router, $request)
    {
        $this->seapi = $seapi;
        $this->twig  = $twig;
        $this->router = $router;
        $this->request = $request;
    }

    /**
     * A chaque fois qu'une requête est émise
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        // Si il s'agit pas d'un requête principale
        if (HttpKernel::MASTER_REQUEST != $event->getRequestType()) {
            return;
        }

        // On récupère la route courante et c'est options ...
        $routeName = $this->request->get('_route');
        $routeOpt = $this->router->getRouteCollection()->get($routeName)->getOptions();
        
        // Si on est dans l'admin
        if (array_key_exists('admin', $routeOpt) && $routeOpt['admin']) {
            // On sauvegarde les informations de connexion via le service seapi
            $this->seapi->setLastConnect();
        }
    }
}