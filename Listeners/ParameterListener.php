<?php
namespace SE\AdminBundle\Listeners;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernel;

/**
 * Défini les variables globales pour twig
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class ParameterListener
{
    private $twig;
    private $request;
    private $entityManager;

    /**
     * Injection des services
     * @param Twig          $twig
     * @param Request       $request
     * @param EntityManager $entityManager
     * @param array         $seadmin
     */
    public function __construct($twig, $request, $entityManager, $seadmin)
    {
        $this->twig  = $twig;
        $this->request = $request;
        $this->entityManager = $entityManager;
        $this->seadmin = $seadmin;
    }

    /**
     * A chaque fois qu'une requête est émise
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        // Si c'est une requête principale
        if (HttpKernel::MASTER_REQUEST != $event->getRequestType()) {
            return;
        }

        // On récupère tous les objets CmsParameter et on les transmets à twig
        $parameters = $this->entityManager->getRepository('SEAdminBundle:CmsParameter')->findAll();
        $preparedParams = array();
        foreach ($parameters as $param) {
            if (is_object($param->getImage())) {
                $preparedParams[$param->getSekey()] = $param->getImage();
            } else {
                $preparedParams[$param->getSekey()] = $param->getSevalue();
            }
        }

        // Injecter des variables globals dans twig
        $this->twig->addGlobal('seadmin', $this->seadmin);

        // On inject les contantes de CmsParameters
        $this->twig->addGlobal('separameters', $preparedParams);
    }
}