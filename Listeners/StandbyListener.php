<?php
namespace SE\AdminBundle\Listeners;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernel;

/**
 * Affiche la page de maintenance si le site est en pause
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class StandbyListener
{
    private $twig;
    private $request;
    private $entityManager;
    private $router;

    /**
     * Injection des services
     * @param Twig          $twig
     * @param Request       $request
     * @param EntityManager $entityManager
     * @param Router        $router
     */
    public function __construct($twig, $request, $entityManager, $router)
    {
        $this->twig  = $twig;
        $this->request = $request;
        $this->entityManager = $entityManager;
        $this->router = $router;
    }

    /**
     * A chaque fois qu'une requête est émise
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        // Si c'est une requête principale
        if (HttpKernel::MASTER_REQUEST != $event->getRequestType()) {
            return;
        }

        // Récupère les options de la route en cour
        $routeName = $this->request->get('_route');
        $routeOpt = $this->router->getRouteCollection()->get($routeName)->getOptions();

        // Si on est pas dans le backoffice
        if (!array_key_exists('admin', $routeOpt) || !$routeOpt['admin']) {
            $standby = $this->entityManager->getRepository('SEAdminBundle:CmsParameter')->findOneBySekey('standby');
            // Et que le site est en standby
            if ($standby && $standby->getSevalue()) {
                // Le standby est activé on check que l'ip n'est pas sur list blanche
                $myip = $this->request->server->get('REMOTE_ADDR');
                $standbyAllowedip = $this->entityManager->getRepository('SEAdminBundle:CmsParameter')->findOneBySekey('standby_allowedip');
                $whiteList = $standbyAllowedip ? explode(',', $standbyAllowedip->getSevalue()) : array();
                
                // Si on est pas dans la liste blanche
                if (!in_array($myip, $whiteList)) {
                    // On check si un template est passé dans le config.yml
                    $modules = $this->twig->getGlobals()['seadmin']['modules'];
                    if (array_key_exists('standby', $modules) && is_array($modules['standby']) && array_key_exists('view', $modules['standby'])) {
                        $template = $this->twig->render($modules['standby']['view']);
                    } else {
                        $template = $this->twig->render('SEAdminBundle:layout:standby.html.twig');
                    }

                    // On configure la réponse avec notre template
                    $response = new Response($template);
                    $event->setResponse($response);
                }
            }
        }
    }
}