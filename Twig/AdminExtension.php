<?php
namespace SE\AdminBundle\Twig;

/**
 * Les extensions custom twig
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class AdminExtension extends \Twig_Extension
{
    /**
     * Enregistrement des filtres
     * @return array
     */
    public function getFilters()
    {
        return array(
            'is_date'    => new \Twig_Filter_Method($this, 'isDateFilter'),
            'over_today' => new \Twig_Filter_Method($this, 'overTodayFilter'),
            'image_size' => new \Twig_Filter_Method($this, 'getImageSize'),
        );
    }

    /**
     * Get Image size
     * @param string $url
     * @return string
     */
    public function getImageSize($url)
    {
        if (file_exists($url)) {
            $sizes = getimagesize($url);

            return $sizes[0]." x ".$sizes[1];
        }

        return "-";
    }

    /**
     * Check si la variable est un objet DateTime
     * @param DateTime $date
     * @return boolean
     */
    public function isDateFilter($date)
    {
        if ($date instanceof \DateTime) {
            return true;
        }

        return false;       
    }

    /**
     * Retour true si la date est supérieur à celle du jour
     * @param DateTime $date
     * @return boolean
     */
    public function overTodayFilter($date)
    {
        $now = new \DateTime();
        if ($date->format('Ymd') >= $now->format('Ymd')) {
            return true;
        }

        return false;
    }

    /**
     * Nom du filtre twig
     * @return string
     */
    public function getName()
    {
        return 'admin_extension';
    }
}