<?php
namespace SE\AdminBundle\Tests\Services;
use SE\AdminBundle\Tests\TestCase;

class ApiTest extends TestCase
{
    public function testGetPublicite() {
    	$api = $this->container->get('se.api');
   		$this->grantUser();
   		$this->assertFalse($api->getPublicite());
   		$this->grantAdmin();
   		$this->assertTrue(is_array($api->getPublicite()), '/!\ id_api must be configured in config.yml ...');
    }
}