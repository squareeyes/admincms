<?php
namespace SE\AdminBundle\Tests\Services;
use SE\AdminBundle\Tests\TestCase;

/**
 * Test unitaire sur le service se.slugify
 */
class SlugifyTest extends TestCase
{
    /**
     * Test sur la méthode de slug
     */
    public function testSlug() {
        $slugify = $this->container->get('se.slugify');
        $this->assertEquals('eeeeacoou', $slugify->slug('éèêëàçöôù'));
        $this->assertEquals('space-try-fr', $slugify->slug('space TRY.fr'));
        $this->assertEquals('strange-char', $slugify->slug('strange-char(@/#\"\'%*$€!\)(&<>=+:;`,?'));
    }    
}