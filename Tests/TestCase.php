<?php
namespace SE\AdminBundle\Tests;
require_once dirname(__DIR__).'/../../../app/AppKernel.php';
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

abstract class TestCase extends \PHPUnit_Framework_TestCase
{
    protected $kernel;
    protected $em;
    protected $security_context;
    protected $container;

    public function setUp() {
        $this->kernel = new \AppKernel('test', true);
        $this->kernel->boot();
        $this->container = $this->kernel->getContainer();
        $this->em = $this->container->get('doctrine')->getManager();
        $this->security_context = $this->container->get('security.context');

        parent::setUp();
    }

    public function tearDown() {
        $this->kernel->shutdown();
        parent::tearDown();
    }

    protected function grantAdmin() {
        $providerKey = $this->container->getParameter('fos_user.firewall_name');
        $token = new UsernamePasswordToken('admin', null, $providerKey, array('ROLE_USER','ROLE_ADMIN'));
        $this->security_context->setToken($token);
    }

    protected function grantUser() {
        $providerKey = $this->container->getParameter('fos_user.firewall_name');
        $token = new UsernamePasswordToken('user', null, $providerKey, array('ROLE_USER'));
        $this->security_context->setToken($token);
    }
}