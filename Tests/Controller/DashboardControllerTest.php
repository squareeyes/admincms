<?php
namespace SE\AdminBundle\Tests\Controller;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Test les naviguations au sein du dashboard
 */
class DashboardControllerTest extends WebTestCase
{
	/**
	 * On test que le composant
	 * Seul un utilisateur avec ROLE_ADMIN est authorisé
	 */
    public function testShouldBeSecurisedArea() {
        $client = static::createClient(array(), array(
        	'PHP_AUTH_USER' => 'admin',
    		'PHP_AUTH_PW'   => 'azerty'
        ));
    	$crawler = $client->request('GET', '/admin');
    	$this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}