<?php
namespace SE\AdminBundle\Services;
use Symfony\Component\HttpFoundation\Response;

class File
{
	private $slugify;	
	private $originalFilename;
	private $filename;
	private $extension;
	private $sizeof;
	private $sizeMax;
	private $validExt;
	private $destination;
	
	public function __construct($slugify) {
		$this->slugify = $slugify;
	}
	
	private function setConfig($file) {
		//On récupère le nom du fichier original
		$this->originalFilename = $file->getClientOriginalName();
		//On récupère l'extension du fichier
		//$this->extension = $file->guessExtension();
		$explodeFilename = explode('.', $this->originalFilename);
		$this->extension = array_pop($explodeFilename);

		//Si l'extension n'est pas authorisée setConfig renvoie false;
		if(is_array($this->validExt) && count($this->validExt) > 0) { 
			if(!in_array($this->extension, $this->validExt)) 
				return false;
		}
		//On récupère la taille du fichier et on la convertie en MO
		$this->sizeof = $file->getSize()/1024/1024;
		//Si le fichier est plus lourd que la taille maximum indiquée on renvoie false
		if(!empty($this->sizeMax) && $this->sizeof > $this->sizeMax)
			return false;
		//Si filename est différent de null ici, c'est que l'utilisateur à mis un nom qui lui tien a coeur pour son fichier
		if(!empty($this->filename)) {
			//On slugify le nom du fichier fournie par l'utilisateur (par principe) et on ajoute l'extension
			$this->filename = $this->slugify->slug($this->filename).'.'.$this->extension;
		} else {
			//Sinon on prend le nom d'origine du fichier et on supprime/remplace tout les caractères étranges via le service "melody.slugify"
			$this->filename = $this->slugify->slug(\preg_replace('/.'.$this->extension.'/', '', $this->originalFilename)).'.'.$this->extension;
		}
		//On s'assure d'avoir un nom de fichier unique
		$this->createUniqueFile();
		//Si on arrive ici c'est que le fichier peux être upload on renvoie true
		return true;
	}
	
	/*
	 * $file = Objet issu du formulaire exemple = $form['filephoto']
	 * $chemin = chemin ou le fichier doit être envoyé exemple "melodyfilebundle/images/photo/"
	 * $nomDuFichier = Nom sous lequelle le fichier doit être enregistré (sans l'extension) exemple = "photo" si non définit = on prend le nom du fichier
	 * $validExtArr = array contenant les extensions accéptées exemple array('jpg','jpeg','gif','png','pdf') si non définit = tout accepté 
	 * $sizeMax = poid maximum du fichier (en MO) exemple "5" si non définit = fichier de toutes tailles accéptés
	 */
	public function upload($file, $chemin='upload/', $nomDuFichier='', $validExtArr='', $sizeMax='') {
		$this->reinitObj();
		//On configure l'attribut contenant le chemin de destination du fichier upload
		if(\substr($chemin, -1, 1) == '/') $this->destination = $chemin; else $this->destination = $chemin.'/';
		//On configure le nom du fichier s'il est null on en générera un unique a partir du nom d'origine du fichier
		if(!empty($nomDuFichier)) $this->filename = $nomDuFichier;
		//Si $validExtArr est un tableau on lance la method addValidExt avec le tableau en paramétre
		if(!empty($validExtArr) && \is_array($validExtArr)) $this->addValidExt($validExtArr);
		//Si $sizeMax et un int ou un float et que c'est différent de null on attribut un valeur a sizeMax
		if(!empty($sizeMax) && \is_float($sizeMax) || \is_integer($sizeMax)) $this->sizeMax = $sizeMax;
		//Si la configuration nous renvoie true c'est que le fichier peux être upload sinon on return false au controller
		if(!$this->setConfig($file))
			return false;
		//On appelle donc la method d'upload
		return $this->goUpload($file);
	}
	
	private function goUpload($file) {
		//On upload le fichier et on renvoie le chemin complet pour accéder au fichier
		$file->move($this->destination, $this->filename);
		return $this->destination.$this->filename;
	}
	
	private function createUniqueFile() {
		$i = 0;
		//Tant que le fichier existe on lui rajoute un marqueur et on re-test
		while(\file_exists($this->destination.$this->filename)) {
			$nomDuFichierBrut = \preg_replace('/.'.$this->extension.'/', '', $this->filename);
			if($i>0) {
				$arr = explode('_',$nomDuFichierBrut);
				$supCompteur = $arr[count($arr)-1];
				$nomDuFichierBrut = \preg_replace('/_'.$supCompteur.'/', '', $nomDuFichierBrut);
			}
			$this->filename = $nomDuFichierBrut.'_'.$i.'.'.$this->extension;
			$i++;
		}
	}
	
	private function addValidExt($arr) {
		$arrExt = array();
		foreach($arr as $ext) {
			$extLower = \strtolower($ext);
			$extUpper = \strtoupper($ext);
			$arrExt[] = $extLower;
			$arrExt[] = $extUpper;
		}
		$this->validExt = $arrExt;
	}
	
	//Vu que le service est instancié qu'une seul fois, on creer une fonction pour réinitialiser les attribut de l'objet
	private function reinitObj() {
		$this->originalFilename = '';
		$this->filename = '';
		$this->extension = '';
		$this->sizeof = '';
		$this->sizeMax = '';
		$this->validExt = '';
		$this->destination = '';
	}
}