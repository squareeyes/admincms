<?php
namespace SE\AdminBundle\Services;

use SE\AdminBundle\Entity\CmsComment;
use SE\AdminBundle\Form\Type\CommentType;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Build comment form
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class CommentBuilder
{
    private $request;
    private $handler;
    private $formFactory;
    private $twig;
    private $mailer;
    private $seadmin;

    /**
     * Constructor
     * @param Request        $request
     * @param CommentHandler $handler
     * @param FormFactory    $formFactory
     * @param Twig           $twig
     * @param Mailer         $mailer
     * @param array          $seadmin
     */
    public function __construct($request, $handler, $formFactory, $twig, $mailer, $seadmin)
    {
        $this->request     = $request;
        $this->handler     = $handler;
        $this->formFactory = $formFactory;
        $this->twig        = $twig;
        $this->mailer      = $mailer;
        $this->seadmin     = $seadmin;
    }

    /**
     * Build form
     * @param CmsGeneric $gen
     * @return string HTML
     */
    public function build($gen)
    {
        $comment = new CmsComment();
        $comment->setGeneric($gen);
        $form = $this->formFactory->create(new CommentType(), $comment);
        if ($comment = $this->handler->process($form)) {
            if (array_key_exists('email', $this->seadmin['site'])) {
                $contentMail = $this->twig->render('SEAdminBundle:comment:email.html.twig', array(
                    'gen'     => $gen,
                    'comment' => $comment
                ));
                $message = \Swift_Message::newInstance();
                $message->setSubject("[COMMENTAIRE] : Un nouveau commentaire")
                         ->setFrom($this->seadmin['site']['email'])
                         ->setTo($this->seadmin['site']['email'])
                         ->setBody($contentMail, 'text/html');
                $this->mailer->send($message);
            }

            return null;
        }

        return $this->twig->render('SEAdminBundle:comment:addfront.html.twig', array(
            'form' => $form->createView()
        ));
    }
}