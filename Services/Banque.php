<?php
namespace SE\AdminBundle\Services;

use SE\AdminBundle\Entity\CmsBanqueImage;

/**
 * Service pour tester si l'image est utilisée
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class Banque
{
    private $entityManager;

    /**
     * Constructeur
     * @param EntityManager $entityManager
     */
    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Test si l'image passée en paramètre est utilisée
     * @param CmsBanqueImage $img
     * @return boolean
     */
    public function isUsed(CmsBanqueImage $img)
    {
        $used = false;
        $id = $img->getId();
        
        if ($this->entityManager->getRepository('SEAdminBundle:CmsNews')->findOneBy(array('image' => $id))) $used = true;
        if ($this->entityManager->getRepository('SEAdminBundle:CmsNewsImage')->findOneBy(array('image' => $id))) $used = true;
        if ($this->entityManager->getRepository('SEAdminBundle:CmsPrestation')->findOneBy(array('image' => $id))) $used = true;
        if ($this->entityManager->getRepository('SEAdminBundle:CmsSeo')->findOneBy(array('image' => $id))) $used = true;
        if ($this->entityManager->getRepository('SEAdminBundle:CmsSlideshowImage')->findOneBy(array('image' => $id))) $used = true;
        if ($this->entityManager->getRepository('SEAdminBundle:CmsCategory')->findOneBy(array('image' => $id))) $used = true;

        return $used;
    }
}