<?php
namespace SE\AdminBundle\Services;

use SE\AdminBundle\Entity\CmsNewsletterMail;

/**
 * Service d'envoi d'emails
 *
 * @author Vincent KELLEHER <vincent@squareeyes.com>
 */
class Mailer {

    private $mailer;
    private $templating;
    private $entityManager;
    private $params;

    /**
     * @param Mailer $mailer 
     * @param EngineInterface $templating
     * @param EntityManager $entityManager 
     * @param array $mailer 
     */
    public function __construct($mailer, $templating, $entityManager, $params)
    {
        $this->mailer        = $mailer;
        $this->templating    = $templating;
        $this->entityManager = $entityManager;
        $this->params        = $params;
    }

    /**
     * Envoi une newsletter
     * @param CmsNewsletter $newsletter
     * @return boolean
     */
    public function sendNewsletter($newsletter)
    {
        $mailinglist = $newsletter->getSendtoall() ? $this->entityManager->getRepository('SEAdminBundle:CmsNewsletterMail')->findAll() : $newsletter->getMailinglist();

        $nls = count($this->entityManager->getRepository('SEAdminBundle:CmsNewsletter')->findBy(array('sent' => '1'))) + 1;
        foreach ($mailinglist as $customer) {
            $data = array(
                'title'      => $newsletter->getTitle(),
                'content'    => $newsletter->getContent(),
                'image'      => $newsletter->getUrlimg(),
                'newsletter' => $newsletter,
                'number'     => $nls
            );

            if (!$this->sendMail($newsletter->getTitle(), null, $customer->getEmail(), $newsletter->getTemplate()->getName(), $data)) {
                return false;
            }
        }

        $newsletter->setDatesent(new \DateTime());
        $newsletter->setSent(true);
        $this->entityManager->persist($newsletter);
        $this->entityManager->flush();

        return true;
    }

    /**
     * Envoi une newsletter de test
     * @param CmsNewsletter $newsletter
     * @return boolean
     */
    public function sendTestNewsletter($newsletter, $testEmail)
    {
        $nls = count($this->entityManager->getRepository('SEAdminBundle:CmsNewsletter')->findBy(array('sent' => '1'))) + 1;       
        $data = array(
            'title'      => $newsletter->getTitle(),
            'content'    => $newsletter->getContent(),
            'image'      => $newsletter->getUrlimg(),
            'newsletter' => $newsletter,
            'number'     => $nls
        );

        if (!$this->sendMail($newsletter->getTitle(), null, $testEmail, $newsletter->getTemplate()->getName(), $data)) {
            return false;
        }

        return true;
    }

    /**
     * Ajoute un email pour les newsletters
     * @param string $email
     * @return boolean
     */
    public function addNewsletterEmail($email)
    {
        $newsletterEmail = new CmsNewsletterMail();
        $newsletterEmail->setEmail($email);
        $newsletterEmail->setDateadd(new \DateTime());

        $this->entityManager->persist($newsletterEmail);
        $this->entityManager->flush();

        return true;
    }

    /**
     * Envoyer un email
     * @param string $subject
     * @param string $from
     * @param string $to
     * @param string $template
     * @param array  $data
     * @return boolean
     */
    public function sendMail($subject, $from = null, $to, $template, $data)
    {
        //->setContentType("text/html")
        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($from ? $form : $this->params['site']['email'])
            ->setTo($to)
            ->setContentType("text/html")
            ->setBody(
                $this->templating->render($template, $data)
            );

        return $this->mailer->send($message);
    }
}