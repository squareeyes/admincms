<?php
namespace SE\AdminBundle\Services;

/**
 * Service pour communiquer avec l'API de publicité PubOuest
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class Api
{
	private $params;
	private $securityContext;
	private $idApi = Null;

	/**
	 * Injection de la configuration et du securityContext
	 * @param array           $params
	 * @param SecurityContext $securityContext
	 */
	public function __construct($params, $securityContext)
	{
		$this->params = $params;
		$this->securityContext = $securityContext;
		if(array_key_exists('site', $this->params) && array_key_exists('id_api', $this->params['site']))
			$this->idApi = $this->params['site']['id_api'];
	}

	/**
	 * Envoie une requête sur l'api pub-ouest pour historiser la connexion de l'admin
	 * @return void
	 */
	public function setLastConnect()
	{
		if($this->idApi != Null && $this->securityContext->isGranted('ROLE_ADMIN')) {
			$this->curl("http://api.pub-ouest.fr/clients/".$this->idApi."/connect");
		}
	}

	/**
	 * Récupère la prochaine facturation
	 * @return array
	 */
	public function getNextInvoice()
	{
		if($this->idApi != Null && $this->securityContext->isGranted('ROLE_ADMIN')) {
			$invoice = $this->curl("http://api.pub-ouest.fr/clients/".$this->idApi."/invoice");

			return json_decode($invoice);
		}

		return false;
	}

	/**
	 * Recherche les publicités diffusée par Pub Ouest
	 * @return array
	 */
	public function getPublicite()
	{
		if($this->idApi != Null && $this->securityContext->isGranted('ROLE_ADMIN')) {
			$pub = $this->curl("http://api.pub-ouest.fr/clients/".$this->idApi."/publicite");

			return json_decode($pub);
		}

		return false;
	}

	/**
	 * Emet une requête CURL sur l'url passé en param
	 * @param string $url
	 * @return json
	 */
	private function curl($url)
	{
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$json = curl_exec($curl);
		curl_close($curl);

		return $json;
	}
}