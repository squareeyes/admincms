## 4/ Create your own user entity

#### 4.1/ Own user class `exemple : src/Acme/UserBundle/Entity/User.php`
	
	<?php
	namespace Acme\UserBundle\Entity;
	use SE\AdminBundle\Model\CmsUser as BaseUser;
	use Doctrine\ORM\Mapping as ORM;

	/**
	 * @ORM\Entity
	 * @ORM\Table(name="CMS_USER")
	 */
	class User extends BaseUser
	{
	    /**
	     * @ORM\Id
	     * @ORM\Column(type="integer")
	     * @ORM\GeneratedValue(strategy="AUTO")
	     */
	    protected $id;

	    public function __construct()
	    {
	        parent::__construct();
	        // your own logic
	    }
	}
