## 3/ Define your's firewall

#### 3.1/ Firewall : security.yml

	security:
	    encoders:
	        FOS\UserBundle\Model\UserInterface: sha512

	    providers:
	        fos_userbundle:
	            id: fos_user.user_provider.username

	    firewalls:
	        main:
	            pattern: ^/
	            form_login:
	                provider: fos_userbundle
	                csrf_provider: form.csrf_provider
	            logout: true
	            anonymous: true

	    access_control:
	        - { path: ^/admin, roles: ROLE_ADMIN }