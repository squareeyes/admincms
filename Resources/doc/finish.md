## 5/ Finish install

#### 5.1/ Build BDD schema

	php app/console doctrine:schema:update --force



#### 5.2/ Create needed folders

	web/upload
	web/media/admin


#### 5.3/ Build assets

	php app/console assets:install web



#### 5.4/ Create Admin user

	php app/console fos:user:create
	php app/console fos:user:promote

Promote user with **ROLE_ADMIN**.