## 2/ How to configure SEAdmin

#### 2.1/ Config doctrine

Define mappings for SEAdminBundle and the resolve target entities with your User entity.  
`app/config/config.yml`

    doctrine:
        orm:
            [...]
            mappings:
                SEAdminBundle: ~
            resolve_target_entities:
                SE\AdminBundle\Model\CmsUser: Demo\TestBundle\Entity\User



#### 2.2/ Config SEAdmin  

`app/config/config.yml`

    se_admin:
        analytics:
            oocharts: 
                apikey: 367877990e4eae03d9be3a2ad17bbfae2b214a69
                profile: 68864131

        site: 
            id_api: 1 # ID in BDD_APIPUB
            name: "Square Eyes Picture" # Client's company
            email: "mikael@se-picture.com" # Client's email

        fos_user:
            user_class: Demo\TestBundle\Entity\User # Your user entity which extends with SE\AdminBundle\Model\CmsUser

        pages:
            accueil: { label: "Accueil", icon: "icon-home", content: false, image: false, slideshow: false, images: true, video: false }
            service: { label: "Services", icon: "icon-book", content: false, image: false, slideshow: false, images: true, video: false }
            contact: { label: "Contact", icon: "icon-envelope", content: false, image: false, slideshow: false, images: true, video: false }
        modules:
            dashboard: true
            standby: { view: 'DemoTestBundle::offline.html.twig' } # variable dispo separameters.standby_title separameters.standby_msg
            publicite: true # Display advertising
            news: { title: "News" } # Newsletter
            prestation: { title: "Projets", categories: true, slideshow: false, images: true, video: false } # Prestation / Projects / ...
            keyword: { title: "Expertise", categories: true, image: true } # Expertise
            storage: true # Cloud ...
            products: true # Product management
            shop: true # Order, Delivery, clients, ...
            help: true

            parameters: # Awesome config globals parameters
                phone: { label: "Phone number : ", type: string }
                makey: { label: "Yes or No : ", type: boolean }
                size: { label: "My size : ", type: choices, options: ['Small', 'Medium', 'Large'] }
                text: { label: "Mon super block : ", type: text }
                file: { label: "L'url de mon fichier : ", type: text }
                image: { label: "Mon logo", type: image }
                wysiwyg: { label: "Texte wysiwyg : ", type: wysiwyg }

            generics:
                producer: # My config key for this generic entity
                    title: "Producteur" # Admin menu name
                    icon: "icon-user" # Admin menu icon
                    order: { field: "pos", way: "ASC" } # Orderby in admin listing
                    format: list # Display in admin (list or thumbnail)

                    # Hide or Show defaults fields for generic entity
                    displayed:
                        content: false      # wysiwyg
                        image: true         # image bank
                        images: false       # add more images
                        youtube: true       # add youtubes videos
                        files: true         # add annexe files
                        date: false         # add date
                        position: true      # can sortable 

                    # Add other fields for your entity
                    fields:
                        url: { label: "Lien : ", type: "string" }             # Simple text field
                        text: { label: "Text : ", type: "text" }              # Simple textarea field
                        content: { label: "Content : ", type: "wysiwyg" }     # Wysiwyg field
                        logo: { label: "My logo : ", type: "image" }          # Image bank field
                        makey: { label: "Yes or No : ", type: boolean }       # Radio Yes / No fields
                        mysize: { label: "My size : ", type: choices, options: ['Small', 'Medium', 'Large'] }

                    mappedmany: product # Add multiple choice with generic entity (configurable with configkey)
                # Add other generics entities ...


        wysiwyg: # Option in wysiwyg
            image: true
            video: true
            format: true
            alignment: true
            link: true
            table: true
            debug: true 

        help: # Display help for client
            contact: { company: "Square Eyes Picture", phone: "06 98 17 24 81", address: "1 quai deschamps - 33800 Bordeaux", email: "debug@squareeyes.fr" }
            hosting: false
            seo: false
            email: { smtp: "mail.agram.fr", email: "contact@agram.fr", passwd: "*******" }
            video: true
            image: false

        css: ['css/custom-admin.css'] # Load your's own CSS for admin interface
        js: ['js/custom-admin.js'] # Load your's own JS for admin interface