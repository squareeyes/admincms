## 1/ How to install SEAdmin
	
#### 1.1/ Add into composer.json

	"repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:squareeyes/admincms.git"
        }
    ],
    "require": {
    	[...],
		"friendsofsymfony/user-bundle": "2.0.*@dev",
    	"liip/imagine-bundle": "dev-master",
    	"squareeyes/admin-bundle": "dev-master"
    }

Then run `composer update` into the application folder  
**/!\ Take care : admincms.git is a private repository so you must add your id_rsa.pub into bitbucket repository**



#### 1.2/ Update AppKernel.php

	[...],
	new SE\AdminBundle\SEAdminBundle(),
    new Liip\ImagineBundle\LiipImagineBundle(),
    new FOS\UserBundle\FOSUserBundle(),



#### 1.3/ Add route into app/config/routing.yml

	se_admin:
	    resource: "@SEAdminBundle/Resources/config/routing.yml"
	    prefix:   /