$(document).ready(function(){
	$('#sort_news_image').sortable({ 
		handle: ".move", 
		cancel: '',
		helper: 'clone',
		stop: function(event, ui) {
			var prepared = [];
			$('#sort_news_image').children('li').each(function(key, item) {
				prepared.push($(this).attr('data-ref'));
			});
			saveSort(prepared);
		}
	}).disableSelection();

	function saveSort(prepared) {
		var url = $('#sort_news_image').attr('data-url');
		$.post(url, { refnewsimages: prepared }, function(data){});
	}
});