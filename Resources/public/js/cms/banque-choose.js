$(document).ready(function(){
	$('#add_img_ajax').unbind('click');
	$('#add_img_ajax').bind('click', function() {
		if($(this).attr('data-open') == 'choose') {
			$('#modal_choose_banque').hide();
			$('#modal_add_banque').show();
			$(this).attr('data-open', 'add');
			$(this).html('Choisir dans la banque');
		} else {
			$('#modal_add_banque').hide();
			$('#modal_choose_banque').show();
			$(this).attr('data-open', 'choose');
			$(this).html('Ajouter une image');
		}
	});

	// Lorsque l'on clique pour choisir un image
	$('.banque_img').click(function() {
		var refimg = $(this).attr('data-refimg');
		if (banqueEL == "" && banqueRedactorEl != "") {
			var urlimg = $(this).attr('data-urlimage');
			banqueRedactorEl.insertHtml('<img src="' + urlimg + '" title="" alt="">');
		} else {
			banqueEL.children("input[type='hidden']").val(refimg);
			banqueEL.children('.report').html($(this).html()+'<div class="ecart10"></div>');
			banqueEL.children('.report').children('img').width(200);
		}
		$('#banque_modal').modal('hide');
	});
	
	// Lorsque l'on ajoute une image via le formulaire de la popin
	$('#add_img_ajax_form').submit(function(){
		showLoad();
		$(this).ajaxSubmit(function(res){
			hideLoad();
			$('#content_banque_modal').html(res);
		});
		return false;
	});
});