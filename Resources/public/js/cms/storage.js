$(document).ready(function() {
	$('#sort_files_list').sortable({ 
		handle: ".move_files", 
		cancel: '',
		helper: 'clone',
		stop: function(event, ui) {
			var prepared = [];
			$('#sort_files_list').children('tr').each(function(key, item) {
				prepared.push($(this).attr('data-ref'));
			});
			var url = $('#sort_files_list').attr('data-url');
			saveSort(prepared, url);
		}
	}).disableSelection();

	function saveSort(prepared, url) {
		$.post(url, { reffiles: prepared }, function(data){});
	}
});