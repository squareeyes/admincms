$(document).ready(function() {
	$('#sort_presta').sortable({ 
		handle: ".move", 
		cancel: '',
		helper: 'clone',
		stop: function(event, ui) {
			var prepared = [];
			$('#sort_presta').children('li').each(function(key, item) {
				prepared.push($(this).attr('data-ref'));
			});
			saveSort(prepared);
		}
	}).disableSelection();

	function saveSort(prepared) {
		var url = $('#sort_presta').attr('data-url');
		$.post(url, { refprestas: prepared }, function(data){});
	}
});