$(document).ready(function(){
	oo.load(function() {
		var profile = $('#container_analytics').attr('data-oochart-profile');
		
		// Tableau pour la localisation par ville
		var table = new oo.Table(profile, "1m");
        table.addMetric("ga:visits", "Visiteurs");
        table.addMetric("ga:newVisits", "Nouveaux visiteurs");
        table.addDimension("ga:city", "Ville");
        table.addDimension("ga:country", "Pays");
        table.query.setSort("-ga:visits");
        table.query.setMaxResults("10");
        table.draw('table_localisation');

        var table_search = new oo.Table(profile, "1m");
        table_search.addMetric("ga:organicSearches", "Nombre");
        table_search.addDimension("ga:keyword", "Mots clés");
        table_search.query.setSort("-ga:organicSearches");
        table_search.query.setMaxResults("10");
        table_search.draw('table_search');

	});


	
});