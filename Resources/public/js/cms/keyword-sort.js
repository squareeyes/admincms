$(document).ready(function() {
	$('#sort_kw').sortable({ 
		handle: ".move_kw", 
		cancel: '',
		helper: 'clone',
		stop: function(event, ui) {
			var prepared = [];
			$('#sort_kw').children('tr').each(function(key, item) {
				prepared.push($(this).attr('data-ref'));
			});
			saveSort(prepared);
		}
	}).disableSelection();

	function saveSort(prepared) {
		var url = $('#sort_kw').attr('data-url');
		$.post(url, { refkws: prepared }, function(data){});
	}
});