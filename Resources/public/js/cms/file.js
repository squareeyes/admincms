$(document).ready(function(){
	$('#sort_file').sortable({ 
		handle: ".move_generic", 
		cancel: '',
		helper: 'clone',
		stop: function(event, ui) {
			var prepared = [];
			$('#sort_file').children('tr').each(function(key, item) {
				prepared.push($(this).attr('data-ref'));
			});
			saveSort(prepared);
		}
	}).disableSelection();

	function saveSort(prepared) {
		var url = $('#sort_file').attr('data-url');
		$.post(url, { reffiles: prepared }, function(data){});
	}
});