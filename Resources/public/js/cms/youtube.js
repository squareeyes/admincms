$(document).ready(function(){
	$('#sort_youtube').sortable({ 
		handle: ".move", 
		cancel: '',
		helper: 'clone',
		stop: function(event, ui) {
			var prepared = [];
			$('#sort_youtube').children('li').each(function(key, item) {
				prepared.push($(this).attr('data-ref'));
			});
			saveSort(prepared);
		}
	}).disableSelection();

	function saveSort(prepared) {
		var url = $('#sort_youtube').attr('data-url');
		$.post(url, { refyoutubes: prepared }, function(data){});
	}
});