$(document).ready(function() {
	$('#sort_slideshow').sortable({ 
		handle: ".move", 
		cancel: '',
		helper: 'clone',
		stop: function(event, ui) {
			var prepared = [];
			$('#sort_slideshow').children('li').each(function(key, item) {
				prepared.push($(this).attr('data-ref'));
			});
			saveSort(prepared);
		}
	}).disableSelection();

	function saveSort(prepared) {
		var url = $('#sort_slideshow').attr('data-url');
		$.post(url, { refimages: prepared }, function(data){});
	}

	$('.remove_slideshow_image').click(function(){
		var href = $(this).attr('href');
		var el = $(this);
		$.post(href, function(data) {
			el.parent('li').remove();
		});

		return false;
	});
	
});