$(document).ready(function() {
	$('#sort_generic').sortable({ 
		handle: ".move_generic", 
		cancel: '',
		helper: 'clone',
		stop: function(event, ui) {
			var prepared = [];
			$('#sort_generic').children('li').each(function(key, item) {
				prepared.push($(this).attr('data-ref'));
			});
			var url = $('#sort_generic').attr('data-url');
			saveSort(prepared, url);
		}
	}).disableSelection();

	$('#sort_generic_list').sortable({ 
		handle: ".move_generic", 
		cancel: '',
		helper: 'clone',
		stop: function(event, ui) {
			var prepared = [];
			$('#sort_generic_list').children('tr').each(function(key, item) {
				prepared.push($(this).attr('data-ref'));
			});
			var url = $('#sort_generic_list').attr('data-url');
			saveSort(prepared, url);
		}
	}).disableSelection();

	function saveSort(prepared, url) {
		$.post(url, { refgeneric: prepared }, function(data){});
	}
});