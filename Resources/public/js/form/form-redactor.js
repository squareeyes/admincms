$(document).ready(function(){
	var formattxt = $('body').attr('data-wysiwyg-format') == 1 ? true : false;
	var table = $('body').attr('data-wysiwyg-table') == 1 ? true : false;
	var image = $('body').attr('data-wysiwyg-image') == 1 ? true : false;
	var video = $('body').attr('data-wysiwyg-video') == 1 ? true : false;
	var link = $('body').attr('data-wysiwyg-link') == 1 ? true : false;
	var alignment = $('body').attr('data-wysiwyg-alignment') == 1 ? true : false;
	var debug = $('body').attr('data-wysiwyg-debug') == 1 ? true : false;

	var buttons1 = formattxt ? ['formattxt', '|'] : [];
	var buttons2 = ['bold', 'italic'];
	var buttons3 = alignment ? ['|', 'alignleft', 'aligncenter', 'alignright', 'justify'] : [];
	var buttons4 = table ? ['|', 'table'] : [];
	var buttons5 = link ? ['|', 'link'] : [];
	var buttons6 = image ? ['|', 'imagebanque'] : [];
	var buttons7 = video ? ['|', 'video'] : [];
	var buttons8 = debug ? ['|', 'html'] : [];
	var buttons = [];
	buttons = buttons.concat(buttons1).concat(buttons2).concat(buttons3).concat(buttons4).concat(buttons5).concat(buttons6).concat(buttons7).concat(buttons8);
	$('.cms_redactor').redactor({
		buttons: buttons,
		//colors: ['#000000', '#eeece1', '#1f497d', '#4f81bd', '#c0504d', '#9bbb59', '#8064a2', '#4bacc6', '#f79646', '#ffff00'],
		toolbarFixed: true,
		allowedTags: ['h1', 'br', 'img', 'p', 'a', 'strong', 'em', 'b', 'i', 'h3', 'hr', 'div', 'h2', 'iframe', 'object', 'table', 'tr', 'td', 'embed'],
		cleanup: true,
		shortcuts: false,
		lang: 'fr',
		convertLinks: true,
		minHeight: 100,
		observeImages: true,
		linebreaks: true,
		buttonsAdd: ['list'],
	    buttonsCustom: {
	    	imagebanque: {
	    		title: "Ouvrir la banque d'image",
	    		callback: function(obj, event, key) {
					showLoad();
					banqueEL = "";
					banqueRedactorEl = this;
					var url = $('.cms_redactor').attr('data-url');
					$.post(url, function(res) {
						hideLoad();
						$('#banque_modal').modal();
						$('#content_banque_modal').html(res);
					});
			    }
	    	},
	    	formattxt: {
                title: "Format du text",
                dropdown: {
                	titre: {
	                	title: "Titre",
						callback: function(obj, event, key) {
							//this.insertHtml('<img src="http://images4.wikia.nocookie.net/__cb20111012233031/desencyclopedie/images/f/f0/Trollface-fa4f7f3e69.png">');
					    	this.execCommand('formatblock', '<h1>');
					    }
                	},
                	sstitre: {
	                	title: "Sous titre",
						callback: function(obj, event, key) {
					    	this.execCommand('formatblock', '<h2>');
					    }
                	},
                	smalltitre: {
	                	title: "Petit titre",
						callback: function(obj, event, key) {
					    	this.execCommand('formatblock', '<h3>');
					    }
                	},
                	text: {
	                	title: "Paragraphe",
						callback: function(obj, event, key) {
					    	this.execCommand('formatblock', '<p>');
					    }
                	}
                }
			}
		}
	});
});