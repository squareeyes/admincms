$(document).ready(function(){
	$('.open_choosable_img').unbind('click');
	$('.open_choosable_img').bind('click', function(){
		showLoad();
		banqueEL = $(this).parent('label').parent('.choose_please');
		var url = $(this).attr('data-url');
		$.post(url, function(res) {
			hideLoad();
			$('#banque_modal').modal();
			$('#content_banque_modal').html(res);
		});
	});
});