$(document).ready(function(){
	$('.datepicker-form').datepicker({
		format: 'dd/mm/yyyy',
		language: 'fr',
		todayHighlight: true,
		autoclose: true
	});
});