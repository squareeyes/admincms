$(document).ready(function() {
    toggleEmails();
    $('#se_adminbundle_newstype_sendtoall').click(function() {
        toggleEmails();
    });
    
});

function toggleEmails() {
    if ($('#se_adminbundle_newstype_sendtoall').is(':checked')) {
        $('#sendtoall-container').hide();
    } else {
        $('#sendtoall-container').show();
    }
}