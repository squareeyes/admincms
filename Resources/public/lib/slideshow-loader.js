/**
 *  data-slideshow
 *  data-slideshow-pauseover = 0 | 1
 *  data-slideshow-autoplay = 0 | 1
 *  data-slideshow-nextprev = 0 | 1
 *  data-slideshow-bullet = 0 | 1
 *  data-slideshow-pourcent = pourcentage
 *  data-duration-view = seconde
 *  data-duration-effect = seconde
 *  data-prev-button = "#id"
 *  data-next-button = "#id"
 *  data-pause-button = "#id"
 */
$(document).ready(function() {
    $("*[data-slideshow]").each(function() {
        var options = {};
        options.effect = "fade"
        options.animSpeed = $(this).attr('data-duration-effect') ? parseInt($(this).attr('data-duration-effect')) : 400;
        options.pauseTime = $(this).attr('data-duration-view') ? parseInt($(this).attr('data-duration-view')) : 4000;
        options.pauseOnHover = $(this).attr('data-slideshow-pauseover') == 1 ? true : false;
        options.manualAdvance = $(this).attr('data-slideshow-autoplay') ? $(this).attr('data-slideshow-autoplay') == 1 ? false : true : false;
        options.directionNav = $(this).attr('data-slideshow-nextprev') == 1 ? true : false;
        options.prevButton = $(this).attr('data-prev-button') ? $(this).attr('data-prev-button') : '';
        options.nextButton = $(this).attr('data-next-button') ? $(this).attr('data-next-button') : '';
        options.pauseButton = $(this).attr('data-pause-button') ? $(this).attr('data-pause-button') : '';
        options.controlNav = $(this).attr('data-slideshow-bullet') == 1 ? true : false;
        options.slices = 1;

        if ($(this).attr('data-slideshow') == 'fade') {
            options.effect = 'fade';
        }

        if ($(this).attr('data-slideshow') == 'kenburns') {
            options.effect = 'kenburns';
            options.kenburnsPourcent = $(this).attr('data-slideshow-pourcent') ? $(this).attr('data-slideshow-pourcent') : 20;
        }

        if ($(this).attr('data-slideshow') == 'reversekenburns') {
            options.effect = 'reverseKenburns';
            options.kenburnsPourcent = $(this).attr('data-slideshow-pourcent') ? $(this).attr('data-slideshow-pourcent') : 20;   
        }

        if ($(this).attr('data-slideshow').match(/panelx/g)) {
            options.effect = 'fold';
            options.slices = parseInt($(this).attr('data-slideshow').replace('panelx', ''));
        }

        if ($(this).attr('data-slideshow') == 'slideleft') {
            options.effect = 'slideInRight';
        }

        if ($(this).attr('data-slideshow') == 'slideright') {
            options.effect = 'slideInLeft';
        }

        if ($(this).attr('data-slideshow') == 'slideparallax') {
            options.effect = 'slideParallax';
        }

        if ($(this).attr('data-slideshow') == 'slideupparallax') {
            options.effect = 'slideUpParallax';
        }

        if ($(this).attr('data-slideshow') == 'cartoon') {
            options.effect = 'cartoon';
        }

        if ($(this).attr('data-slideshow') == 'puffup') {
            options.effect = 'puffUp';
        }
        
        options.pauseTime = options.pauseTime + options.animSpeed;
        $(this).nivoSlider(options);
    });
});
