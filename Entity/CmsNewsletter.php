<?php
namespace SE\AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CmsNewsletter
 *
 * @ORM\Table(name="CMS_NEWSLETTER")
 * @ORM\Entity
 */
class CmsNewsletter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
    * @ORM\ManyToMany(targetEntity="CmsNewsletterMail", inversedBy="newsletters", cascade={"persist"})
    * @ORM\JoinTable(name="CMS_NEWSLETTER_REL_MAIL",
    *     joinColumns={@ORM\JoinColumn(name="refnewsletter", referencedColumnName="id")},
    *     inverseJoinColumns={@ORM\JoinColumn(name="refnewslettermail", referencedColumnName="id")}
    * )
    */
    private $mailinglist;

    /**
     * @var boolean
     *
     * @ORM\Column(name="sendtoall", type="boolean", nullable=true)
     */
    private $sendtoall;

    /**
    * @var CmsNewsletterTemplate
    *
    * @ORM\ManyToOne(targetEntity="CmsNewsletterTemplate")
    * @ORM\JoinColumns({
    *   @ORM\JoinColumn(name="refnewslettertemplate", referencedColumnName="id")
    * })
    */
    private $template;

    /**
     * @var boolean
     *
     * @ORM\Column(name="sent", type="boolean", nullable=true)
     */
    private $sent;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datesent", type="datetime", nullable=true)
     */
    private $datesent;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=60, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="urlimg", type="text", nullable=true)
     */
    private $urlimg;

    /**
     * Don't persist
     * @Assert\Image(mimeTypesMessage="Vous devez chargement seulement des images aux format JPG ou PNG")
     */
    private $submittedImage;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=false)
     */
    private $content;

    /**
    * @ORM\OneToMany(targetEntity="CmsNewsletterImage", mappedBy="newsletter", cascade={"persist", "remove"})
    * @ORM\OrderBy({"position"="ASC"})
    */
    private $images;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->mailinglist = new \Doctrine\Common\Collections\ArrayCollection();
        $this->images      = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get submitted Image
     * @return Image
     */
    public function getSubmittedImage()
    {
        return $this->submittedImage;
    }

    /**
     * Set submitted image
     * @param Image $image
     * @return CmsNewsletter
     */
    public function setSubmittedImage($image)
    {
        $this->submittedImage = $image;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datesent
     *
     * @param \DateTime $datesent
     * @return CmsNewsletter
     */
    public function setDatesent($datesent)
    {
        $this->datesent = $datesent;

        return $this;
    }

    /**
     * Get datesent
     *
     * @return \DateTime 
     */
    public function getDatesent()
    {
        return $this->datesent;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return CmsNewsletter
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return CmsNewsletter
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Add mailinglist
     *
     * @param \SE\AdminBundle\Entity\CmsNewsletterMail $mailinglist
     * @return CmsNewsletter
     */
    public function addMailinglist(\SE\AdminBundle\Entity\CmsNewsletterMail $mailinglist)
    {
        $this->mailinglist[] = $mailinglist;

        return $this;
    }

    /**
     * Remove mailinglist
     *
     * @param \SE\AdminBundle\Entity\CmsNewsletterMail $mailinglist
     */
    public function removeMailinglist(\SE\AdminBundle\Entity\CmsNewsletterMail $mailinglist)
    {
        $this->mailinglist->removeElement($mailinglist);
    }

    /**
     * Get mailinglist
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMailinglist()
    {
        return $this->mailinglist;
    }

    /**
     * Set template
     *
     * @param \SE\AdminBundle\Entity\CmsNewsletterTemplate $template
     * @return CmsNewsletter
     */
    public function setTemplate(\SE\AdminBundle\Entity\CmsNewsletterTemplate $template = null)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return \SE\AdminBundle\Entity\CmsNewsletterTemplate 
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set sent
     *
     * @param boolean $sent
     * @return CmsNewsletter
     */
    public function setSent($sent)
    {
        $this->sent = $sent;

        return $this;
    }

    /**
     * Get sent
     *
     * @return boolean 
     */
    public function getSent()
    {
        return $this->sent;
    }

    /**
     * Set sendtoall
     *
     * @param boolean $sendtoall
     * @return CmsNewsletter
     */
    public function setSendtoall($sendtoall)
    {
        $this->sendtoall = $sendtoall;

        return $this;
    }

    /**
     * Get sendtoall
     *
     * @return boolean 
     */
    public function getSendtoall()
    {
        return $this->sendtoall;
    }

    /**
     * Set urlimg
     *
     * @param string $urlimg
     *
     * @return CmsNewsletter
     */
    public function setUrlimg($urlimg)
    {
        $this->urlimg = $urlimg;

        return $this;
    }

    /**
     * Get urlimg
     *
     * @return string 
     */
    public function getUrlimg()
    {
        return $this->urlimg;
    }

    /**
     * Add images
     *
     * @param \SE\AdminBundle\Entity\CmsNewsletterImage $images
     *
     * @return CmsNewsletter
     */
    public function addImage(\SE\AdminBundle\Entity\CmsNewsletterImage $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \SE\AdminBundle\Entity\CmsNewsletterImage $images
     */
    public function removeImage(\SE\AdminBundle\Entity\CmsNewsletterImage $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }
}
