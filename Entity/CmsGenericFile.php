<?php
namespace SE\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CmsGenericFile
 *
 * @ORM\Table(name="CMS_GENERIC_FILE")
 * @ORM\Entity
 */
class CmsGenericFile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
    * @var CmsGenericCategoryFile
    *
    * @ORM\ManyToOne(targetEntity="CmsGenericCategoryFile", inversedBy="files")
    * @ORM\JoinColumns({
    *   @ORM\JoinColumn(name="refcategory", referencedColumnName="id")
    * })
    */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=150, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="urlweb", type="string", length=255, nullable=true)
     */
    private $urlweb;

    /**
     * Don't persist
     */
    private $submittedFile;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=false)
     */
    private $position;

    /**
     * @var CmsGeneric
     *
     * @ORM\ManyToOne(targetEntity="CmsGeneric", inversedBy="youtubes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="refgeneric", referencedColumnName="id")
     * })
     */
    private $generic;

    public function getSubmittedFile() 
    {
        return $this->submittedFile;
    }

    public function setSubmittedFile($file) 
    {
        $this->submittedFile = $file;
        return $this;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->position = 0;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return CmsGenericFile
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return CmsGenericFile
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return CmsGenericFile
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set generic
     *
     * @param \SE\AdminBundle\Entity\CmsGeneric $generic
     *
     * @return CmsGenericFile
     */
    public function setGeneric(\SE\AdminBundle\Entity\CmsGeneric $generic = null)
    {
        $this->generic = $generic;

        return $this;
    }

    /**
     * Get generic
     *
     * @return \SE\AdminBundle\Entity\CmsGeneric 
     */
    public function getGeneric()
    {
        return $this->generic;
    }

    /**
     * Set urlweb
     *
     * @param string $urlweb
     *
     * @return CmsGenericFile
     */
    public function setUrlweb($urlweb)
    {
        $this->urlweb = $urlweb;

        return $this;
    }

    /**
     * Get urlweb
     *
     * @return string 
     */
    public function getUrlweb()
    {
        return $this->urlweb;
    }

    /**
     * Set category
     *
     * @param \SE\AdminBundle\Entity\CmsGenericCategoryFile $category
     *
     * @return CmsGenericFile
     */
    public function setCategory(\SE\AdminBundle\Entity\CmsGenericCategoryFile $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \SE\AdminBundle\Entity\CmsGenericCategoryFile 
     */
    public function getCategory()
    {
        return $this->category;
    }
}
