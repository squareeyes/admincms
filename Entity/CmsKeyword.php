<?php
namespace SE\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CmsKeyword
 *
 * @ORM\Table(name="CMS_KEYWORD")
 * @ORM\Entity(repositoryClass="SE\AdminBundle\Repositories\CmsKeywordRepository")
 */
class CmsKeyword
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="urlimg", type="text", nullable=true)
     */
    private $urlimg;

    /**
     * @var string
     * Don't persisted
     * @Assert\Image(mimeTypesMessage = "Le fichier ne semble pas être un fichier image valide.")
     */
    protected $submitedImg;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=150, nullable=false)
     * @Assert\Length(min=3, max=150, minMessage="Le libellé doit être supèrieur à 3 caractères.", maxMessage="Le libellé ne doit pas dépasser 150 caractères.")
     * @Assert\NotBlank(message="Le libellé est obligatoire.")
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="pos", type="integer", nullable=true)
     */
    private $pos;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="text", nullable=true)
     */
    private $url;

    /**
    * @ORM\ManyToMany(targetEntity="CmsCategory", inversedBy="prestations")
    * @ORM\JoinTable(name="CMS_KEYWORD_REL_CAT",
    *     joinColumns={@ORM\JoinColumn(name="refkw", referencedColumnName="id")},
    *     inverseJoinColumns={@ORM\JoinColumn(name="refcat", referencedColumnName="id")}
    * )
    * @ORM\OrderBy({"libelle" = "ASC"})
    */
    private $categories;

    /**
     * Constructeur
     */
    public function __construct()
    {
        $this->pos = 0;
        $this->categories = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return CmsKeyword
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set pos
     *
     * @param integer $pos
     *
     * @return CmsKeyword
     */
    public function setPos($pos)
    {
        $this->pos = $pos;

        return $this;
    }

    /**
     * Get pos
     *
     * @return integer 
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return CmsKeyword
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Add categories
     *
     * @param \SE\AdminBundle\Entity\CmsCategory $categories
     *
     * @return CmsKeyword
     */
    public function addCategory(\SE\AdminBundle\Entity\CmsCategory $categories)
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \SE\AdminBundle\Entity\CmsCategory $categories
     */
    public function removeCategory(\SE\AdminBundle\Entity\CmsCategory $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Get submitedImg
     * @return string
     */
    public function getSubmitedImg() 
    {
        return $this->submitedImg;
    }

    public function setSubmitedImg($file) 
    {
        $this->submitedImg = $file;

        return $this;
    }

    public function eraseSubmitedImg() 
    {
        $this->submitedImg = null;

        return $this;
    }

    /**
     * Set urlimg
     *
     * @param string $urlimg
     *
     * @return CmsKeyword
     */
    public function setUrlimg($urlimg)
    {
        $this->urlimg = $urlimg;

        return $this;
    }

    /**
     * Get urlimg
     *
     * @return string 
     */
    public function getUrlimg()
    {
        return $this->urlimg;
    }
}
