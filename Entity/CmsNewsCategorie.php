<?php
namespace SE\AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * CmsNewsCategorie
 *
 * @ORM\Table(name="CMS_NEWS_CATEGORIE")
 * @ORM\Entity(repositoryClass="SE\AdminBundle\Repositories\CmsNewsCategorieRepository")
 * @UniqueEntity("libelle", message="La catégorie que vous voulez ajouter existe déjà.")
 */
class CmsNewsCategorie
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=60, nullable=false)
     * @Assert\Length(
     *      min = "3",
     *      max = "60",
     *      minMessage = "La catégorie doit faire au moins {{ limit }} caractères.",
     *      maxMessage = "La catégorie ne peut pas être plus long que {{ limit }} caractères."
     * )
     * @Assert\NotBlank(message="Le libellé de la catégorie est obligatoire.")
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="text", nullable=false)
     */
    private $slug;

    /**
    * @ORM\ManyToMany(targetEntity="CmsNews", mappedBy="categories")
    * @ORM\OrderBy({"datepost" = "DESC"})
    */
    private $news;
    

    public function __construct()
    {
        $this->news = new ArrayCollection();
    }
        

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return CmsNewsCategorie
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return CmsNewsCategorie
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add news
     *
     * @param \SE\AdminBundle\Entity\CmsNews $news
     *
     * @return CmsNewsCategorie
     */
    public function addNews(\SE\AdminBundle\Entity\CmsNews $news)
    {
        $this->news[] = $news;

        return $this;
    }

    /**
     * Remove news
     *
     * @param \SE\AdminBundle\Entity\CmsNews $news
     */
    public function removeNews(\SE\AdminBundle\Entity\CmsNews $news)
    {
        $this->news->removeElement($news);
    }

    /**
     * Get news
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNews()
    {
        return $this->news;
    }
}
