<?php
namespace SE\AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CmsBanqueImage
 *
 * @ORM\Table(name="CMS_BANQUE_IMAGE")
 * @ORM\Entity(repositoryClass="SE\AdminBundle\Repositories\CmsBanqueImageRepository")
 */
class CmsBanqueImage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="urlimg", type="text", nullable=true)
     */
    private $urlimg;

    /**
     * @var string
     * Don't persisted
     * @Assert\Image(mimeTypesMessage = "Le fichier ne semble pas être un fichier image valide.")
     */
    protected $submitedImg;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=60, nullable=true)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="text", nullable=true)
     */
    private $keywords;

    public function __toString()
    {
        return (string) $this->id;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set urlimg
     *
     * @param string $urlimg
     *
     * @return CmsBanqueImage
     */
    public function setUrlimg($urlimg)
    {
        $this->urlimg = $urlimg;

        return $this;
    }

    /**
     * Get urlimg
     *
     * @return string 
     */
    public function getUrlimg()
    {
        return $this->urlimg;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return CmsBanqueImage
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     *
     * @return CmsBanqueImage
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string 
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    public function getSubmitedImg() 
    {
        return $this->submitedImg;
    }

    public function setSubmitedImg($file) 
    {
        $this->submitedImg = $file;
        return $this;
    }

    public function eraseSubmitedImg() 
    {
        $this->submitedImg = null;
        return $this;
    }
}
