<?php
namespace SE\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * CmsGenericField
 *
 * @ORM\Table(name="CMS_GENERIC_FIELD")
 * @ORM\Entity(repositoryClass="SE\AdminBundle\Repositories\CmsGenericFieldRepository")
 */
class CmsGenericField
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
    * @var CmsGeneric
    *
    * @ORM\ManyToOne(targetEntity="CmsGeneric", inversedBy="fields")
    * @ORM\JoinColumns({
    *   @ORM\JoinColumn(name="refgeneric", referencedColumnName="id")
    * })
    */
    private $generic;

    /**
     * @var string
     *
     * @ORM\Column(name="configkey", type="string", length=20, nullable=false)
     */
    private $fieldkey;

    /**
     * @var string
     *
     * @ORM\Column(name="sevalue", type="text", nullable=true)
     */
    private $fieldvalue;

    /**
     * @var CmsSlideshow
     *
     * @ORM\ManyToOne(targetEntity="CmsSlideshow")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="refslideshow", referencedColumnName="id")
     * })
     */
    private $slideshow;

    /**
     * @var CmsBanqueImage
     *
     * @ORM\ManyToOne(targetEntity="CmsBanqueImage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="refbanque", referencedColumnName="id")
     * })
     */
    private $image;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->childrens = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fieldkey
     *
     * @param string $fieldkey
     *
     * @return CmsGenericField
     */
    public function setFieldkey($fieldkey)
    {
        $this->fieldkey = $fieldkey;

        return $this;
    }

    /**
     * Get fieldkey
     *
     * @return string 
     */
    public function getFieldkey()
    {
        return $this->fieldkey;
    }

    /**
     * Set fieldvalue
     *
     * @param string $fieldvalue
     *
     * @return CmsGenericField
     */
    public function setFieldvalue($fieldvalue)
    {
        $this->fieldvalue = $fieldvalue;

        return $this;
    }

    /**
     * Get fieldvalue
     *
     * @return string 
     */
    public function getFieldvalue()
    {
        return $this->fieldvalue;
    }

    /**
     * Set generic
     *
     * @param \SE\AdminBundle\Entity\CmsGeneric $generic
     *
     * @return CmsGenericField
     */
    public function setGeneric(\SE\AdminBundle\Entity\CmsGeneric $generic = null)
    {
        $this->generic = $generic;

        return $this;
    }

    /**
     * Get generic
     *
     * @return \SE\AdminBundle\Entity\CmsGeneric 
     */
    public function getGeneric()
    {
        return $this->generic;
    }

    /**
     * Set slideshow
     *
     * @param \SE\AdminBundle\Entity\CmsSlideshow $slideshow
     *
     * @return CmsGenericField
     */
    public function setSlideshow(\SE\AdminBundle\Entity\CmsSlideshow $slideshow = null)
    {
        $this->slideshow = $slideshow;

        return $this;
    }

    /**
     * Get slideshow
     *
     * @return \SE\AdminBundle\Entity\CmsSlideshow 
     */
    public function getSlideshow()
    {
        return $this->slideshow;
    }

    /**
     * Set image
     *
     * @param \SE\AdminBundle\Entity\CmsBanqueImage $image
     *
     * @return CmsGenericField
     */
    public function setImage(\SE\AdminBundle\Entity\CmsBanqueImage $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \SE\AdminBundle\Entity\CmsBanqueImage 
     */
    public function getImage()
    {
        return $this->image;
    }
}
