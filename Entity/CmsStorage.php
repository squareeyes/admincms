<?php
namespace SE\AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CmsStorage
 *
 * @ORM\Table(name="CMS_STORAGE")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class CmsStorage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="urlfile", type="text", nullable=false)
     */
    private $urlfile;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=150, nullable=false)
     * @Assert\NotBlank(message="Le titre est obligatoire.")
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateadd", type="datetime", nullable=false)
     */
    private $dateadd;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=false)
     */
    private $position;

    /**
     * @var file
     */
    protected $submitedFile;

    public function __construct()
    {
        $this->dateadd = new \DateTime();
        $this->position = 0;
    }

    /**
     * @ORM\PreRemove
     */
    public function removeFile()
    {
        if (file_exists($this->urlfile)) {
            unlink($this->urlfile);
        }
    }

    /**
     * Supprime le fichier upload
     */
    public function eraseSubmitedFile()
    {
        $this->submitedFile = null;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set urlfile
     *
     * @param string $urlfile
     *
     * @return CmsStorage
     */
    public function setUrlfile($urlfile)
    {
        $this->urlfile = $urlfile;

        return $this;
    }

    /**
     * Get urlfile
     *
     * @return string 
     */
    public function getUrlfile()
    {
        return $this->urlfile;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return CmsStorage
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    public function getSubmitedFile() 
    {
        return $this->submitedFile;
    }

    public function setSubmitedFile($file) 
    {
        $this->submitedFile = $file;
        return $this;
    }

    /**
     * Set dateadd
     *
     * @param \DateTime $dateadd
     *
     * @return CmsStorage
     */
    public function setDateadd($dateadd)
    {
        $this->dateadd = $dateadd;

        return $this;
    }

    /**
     * Get dateadd
     *
     * @return \DateTime 
     */
    public function getDateadd()
    {
        return $this->dateadd;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return CmsStorage
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }
}
