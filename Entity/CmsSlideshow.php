<?php
namespace SE\AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CmsSlideshow
 *
 * @ORM\Table(name="CMS_SLIDESHOW")
 * @ORM\Entity
 */
class CmsSlideshow
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=150, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="effect", type="string", length=50, nullable=true)
     */
    private $effect;

    /**
     * @var float
     *
     * @ORM\Column(name="delayanim", type="float", nullable=true)
     */
    private $delayanim;

    /**
     * @var float
     *
     * @ORM\Column(name="delayshow", type="float", nullable=true)
     */
    private $delayshow;

    /**
    * @ORM\OneToMany(targetEntity="CmsSlideshowImage", mappedBy="slideshow", cascade={"persist", "remove"})
    * @ORM\OrderBy({"position" = "ASC", "id" = "DESC"})
    */
    private $images;
    
    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->effect = 'fifo';
        $this->delayanim = 1;
        $this->delayshow = 3;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return CmsSlideshow
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set effect
     *
     * @param string $effect
     *
     * @return CmsSlideshow
     */
    public function setEffect($effect)
    {
        $this->effect = $effect;

        return $this;
    }

    /**
     * Get effect
     *
     * @return string 
     */
    public function getEffect()
    {
        return $this->effect;
    }

    /**
     * Add images
     *
     * @param \SE\AdminBundle\Entity\CmsSlideshowImage $images
     *
     * @return CmsSlideshow
     */
    public function addImage(\SE\AdminBundle\Entity\CmsSlideshowImage $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \SE\AdminBundle\Entity\CmsSlideshowImage $images
     */
    public function removeImage(\SE\AdminBundle\Entity\CmsSlideshowImage $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set delayanim
     *
     * @param float $delayanim
     *
     * @return CmsSlideshow
     */
    public function setDelayanim($delayanim)
    {
        $this->delayanim = $delayanim;

        return $this;
    }

    /**
     * Get delayanim
     *
     * @return float 
     */
    public function getDelayanim()
    {
        return $this->delayanim;
    }

    /**
     * Set delayshow
     *
     * @param float $delayshow
     *
     * @return CmsSlideshow
     */
    public function setDelayshow($delayshow)
    {
        $this->delayshow = $delayshow;

        return $this;
    }

    /**
     * Get delayshow
     *
     * @return float 
     */
    public function getDelayshow()
    {
        return $this->delayshow;
    }
}
