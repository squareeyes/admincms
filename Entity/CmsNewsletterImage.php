<?php
namespace SE\AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CmsNewsletter
 *
 * @ORM\Table(name="CMS_NEWSLETTER_IMAGE")
 * @ORM\Entity
 */
class CmsNewsletterImage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
    * @var CmsNewsletter
    *
    * @ORM\ManyToOne(targetEntity="CmsNewsletter", inversedBy="images")
    * @ORM\JoinColumns({
    *   @ORM\JoinColumn(name="refnewsletter", referencedColumnName="id")
    * })
    */
    private $newsletter;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=60, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="urlimg", type="text", nullable=true)
     */
    private $urlimg;

    /**
     * Don't persist
     * @Assert\Image(mimeTypesMessage="Vous devez chargement seulement des images aux format JPG ou PNG")
     */
    private $submittedImage;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=false)
     */
    private $content;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=false)
     */
    private $position;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->position = 0;
    }

    /**
     * Get submitted Image
     * @return Image
     */
    public function getSubmittedImage()
    {
        return $this->submittedImage;
    }

    /**
     * Set submitted image
     * @param Image $image
     * @return CmsNewsletter
     */
    public function setSubmittedImage($image)
    {
        $this->submittedImage = $image;

        return $this;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return CmsNewsletterImage
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set urlimg
     *
     * @param string $urlimg
     *
     * @return CmsNewsletterImage
     */
    public function setUrlimg($urlimg)
    {
        $this->urlimg = $urlimg;

        return $this;
    }

    /**
     * Get urlimg
     *
     * @return string 
     */
    public function getUrlimg()
    {
        return $this->urlimg;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return CmsNewsletterImage
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return CmsNewsletterImage
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set newsletter
     *
     * @param \SE\AdminBundle\Entity\CmsNewsletter $newsletter
     *
     * @return CmsNewsletterImage
     */
    public function setNewsletter(\SE\AdminBundle\Entity\CmsNewsletter $newsletter = null)
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    /**
     * Get newsletter
     *
     * @return \SE\AdminBundle\Entity\CmsNewsletter 
     */
    public function getNewsletter()
    {
        return $this->newsletter;
    }
}
