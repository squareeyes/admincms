<?php
namespace SE\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CmsComment
 *
 * @ORM\Table(name="CMS_COMMENT")
 * @ORM\Entity(repositoryClass="SE\AdminBundle\Repositories\CmsCommentRepository")
 */
class CmsComment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", nullable=false)
     * @Assert\NotBlank(message="Le commentaire est obligatoire.")
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=150, nullable=true)
     * @Assert\Length(max=150, maxMessage="Votre pseudo ne doit pas dépasser 150 caractères.")
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Assert\Length(max=255, maxMessage="Votre email ne doit pas dépasser 255 caractères.")
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datepost", type="datetime", nullable=false)
     */
    private $datepost;

    /**
    * @var CmsGeneric
    *
    * @ORM\ManyToOne(targetEntity="CmsGeneric", inversedBy="comments")
    * @ORM\JoinColumns({
    *   @ORM\JoinColumn(name="refgeneric", referencedColumnName="id")
    * })
    */
    private $generic;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->datepost = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return CmsComment
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return CmsComment
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return CmsComment
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set datepost
     *
     * @param \DateTime $datepost
     *
     * @return CmsComment
     */
    public function setDatepost($datepost)
    {
        $this->datepost = $datepost;

        return $this;
    }

    /**
     * Get datepost
     *
     * @return \DateTime 
     */
    public function getDatepost()
    {
        return $this->datepost;
    }

    /**
     * Set generic
     *
     * @param \SE\AdminBundle\Entity\CmsGeneric $generic
     *
     * @return CmsComment
     */
    public function setGeneric(\SE\AdminBundle\Entity\CmsGeneric $generic = null)
    {
        $this->generic = $generic;

        return $this;
    }

    /**
     * Get generic
     *
     * @return \SE\AdminBundle\Entity\CmsGeneric 
     */
    public function getGeneric()
    {
        return $this->generic;
    }
}
