<?php
namespace SE\AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CmsPrestation
 *
 * @ORM\Table(name="CMS_PRESTATION")
 * @ORM\Entity(repositoryClass="SE\AdminBundle\Repositories\CmsPrestationRepository")
 */
class CmsPrestation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
    * @var CmsBanqueImage
    *
    * @ORM\ManyToOne(targetEntity="CmsBanqueImage")
    * @ORM\JoinColumns({
    *   @ORM\JoinColumn(name="refbanque", referencedColumnName="id")
    * })
    * @Assert\NotBlank(message="Vous devez charger une image.")
    */
    private $image;

    /**
    * @var CmsSlideshow
    *
    * @ORM\ManyToOne(targetEntity="CmsSlideshow", cascade={"persist", "remove"})
    * @ORM\JoinColumns({
    *   @ORM\JoinColumn(name="refslideshow", referencedColumnName="id")
    * })
    */
    private $slideshow;

    /**
    * @var CmsVideo
    *
    * @ORM\ManyToOne(targetEntity="CmsVideo", cascade={"persist", "remove"})
    * @ORM\JoinColumns({
    *   @ORM\JoinColumn(name="refvideo", referencedColumnName="id")
    * })
    */
    private $video;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=150, nullable=false)
     * @Assert\Length(min=5, max=150, minMessage="Le titre doit être supèrieur à 5 caractères.", maxMessage="Le titre ne doit pas dépasser 150 caractères.")
     * @Assert\NotBlank(message="Le titre est obligatoire.")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="text", nullable=false)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var integer
     *
     * @ORM\Column(name="pos", type="integer", nullable=true)
     */
    private $pos;

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=false)
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="text", nullable=true)
     */
    private $url;

    /**
    * @ORM\ManyToMany(targetEntity="CmsCategory", inversedBy="prestations")
    * @ORM\JoinTable(name="CMS_PRESTATION_REL_CAT",
    *     joinColumns={@ORM\JoinColumn(name="refpresta", referencedColumnName="id")},
    *     inverseJoinColumns={@ORM\JoinColumn(name="refcat", referencedColumnName="id")}
    * )
    * @ORM\OrderBy({"libelle" = "ASC"})
    */
    private $categories;

    public function __construct()
    {
        $this->pos = 0;
        $this->active = 0;
        $this->categories = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return CmsPrestation
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return CmsPrestation
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return CmsPrestation
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set pos
     *
     * @param integer $pos
     *
     * @return CmsPrestation
     */
    public function setPos($pos)
    {
        $this->pos = $pos;

        return $this;
    }

    /**
     * Get pos
     *
     * @return integer 
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return CmsPrestation
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set image
     *
     * @param \SE\AdminBundle\Entity\CmsBanqueImage $image
     *
     * @return CmsPrestation
     */
    public function setImage(\SE\AdminBundle\Entity\CmsBanqueImage $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \SE\AdminBundle\Entity\CmsBanqueImage 
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * Set slideshow
     *
     * @param \SE\AdminBundle\Entity\CmsSlideshow $slideshow
     *
     * @return CmsPrestation
     */
    public function setSlideshow(\SE\AdminBundle\Entity\CmsSlideshow $slideshow = null)
    {
        $this->slideshow = $slideshow;

        return $this;
    }

    /**
     * Get slideshow
     *
     * @return \SE\AdminBundle\Entity\CmsSlideshow 
     */
    public function getSlideshow()
    {
        return $this->slideshow;
    }

    /**
     * Set active
     *
     * @param integer $active
     *
     * @return CmsPrestation
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return (boolean) $this->active;
    }

    /**
     * Set video
     *
     * @param \SE\AdminBundle\Entity\CmsVideo $video
     *
     * @return CmsPrestation
     */
    public function setVideo(\SE\AdminBundle\Entity\CmsVideo $video = null)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return \SE\AdminBundle\Entity\CmsVideo 
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Add categories
     *
     * @param \SE\AdminBundle\Entity\CmsCategory $categories
     *
     * @return CmsPrestation
     */
    public function addCategory(\SE\AdminBundle\Entity\CmsCategory $categories)
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \SE\AdminBundle\Entity\CmsCategory $categories
     */
    public function removeCategory(\SE\AdminBundle\Entity\CmsCategory $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }
}
