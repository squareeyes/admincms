<?php
namespace SE\AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * CmsNewsletterMail
 *
 * @ORM\Table(name="CMS_NEWSLETTER_MAIL")
 * @ORM\Entity
 * @UniqueEntity("email", message="Vous êtes déjà inscrit à notre newsletter.")
 */
class CmsNewsletterMail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=60, nullable=false)
     * @Assert\Email(message="Votre email n'est pas valide.")
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateadd", type="datetime", length=255, nullable=false)
     */
    private $dateadd;

    /**
     * @ORM\ManyToMany(targetEntity="CmsNewsletter", mappedBy="mailinglist")
     */
    private $newsletters;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->newsletters = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dateadd = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return CmsNewsletterMail
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set dateadd
     *
     * @param \DateTime $dateadd
     * @return CmsNewsletterMail
     */
    public function setDateadd($dateadd)
    {
        $this->dateadd = $dateadd;

        return $this;
    }

    /**
     * Get dateadd
     *
     * @return \DateTime 
     */
    public function getDateadd()
    {
        return $this->dateadd;
    }

    /**
     * Add newsletters
     *
     * @param \SE\AdminBundle\Entity\CmsNewsletter $newsletters
     * @return CmsNewsletterMail
     */
    public function addNewsletter(\SE\AdminBundle\Entity\CmsNewsletter $newsletters)
    {
        $this->newsletters[] = $newsletters;

        return $this;
    }

    /**
     * Remove newsletters
     *
     * @param \SE\AdminBundle\Entity\CmsNewsletter $newsletters
     */
    public function removeNewsletter(\SE\AdminBundle\Entity\CmsNewsletter $newsletters)
    {
        $this->newsletters->removeElement($newsletters);
    }

    /**
     * Get newsletters
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNewsletters()
    {
        return $this->newsletters;
    }
}
