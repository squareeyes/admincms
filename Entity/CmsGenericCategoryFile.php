<?php
namespace SE\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * CmsGenericCategoryFile
 *
 * @ORM\Table(name="CMS_GENERIC_CATEGORY_FILE")
 * @UniqueEntity("title", message="La catégorie existe déjà.")
 * @ORM\Entity(repositoryClass="SE\AdminBundle\Repositories\CmsGenericCategoryFileRepository")
 */
class CmsGenericCategoryFile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=150, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", nullable=true)
     */
    private $content;

    /**
    * @ORM\OneToMany(targetEntity="CmsGenericFile", mappedBy="category", cascade={"persist", "remove"})
    */
    private $files;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->files = new ArrayCollection();
    }

    /**
     * Check if free
     * @return boolean
     */
    public function isFree()
    {
        if (count($this->files) > 0) {
            return false;
        }

        return true;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return CmsGenericCategoryFile
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Add files
     *
     * @param \SE\AdminBundle\Entity\CmsGenericFile $files
     *
     * @return CmsGenericCategoryFile
     */
    public function addFile(\SE\AdminBundle\Entity\CmsGenericFile $files)
    {
        $this->files[] = $files;

        return $this;
    }

    /**
     * Remove files
     *
     * @param \SE\AdminBundle\Entity\CmsGenericFile $files
     */
    public function removeFile(\SE\AdminBundle\Entity\CmsGenericFile $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return CmsGenericCategoryFile
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }
}
