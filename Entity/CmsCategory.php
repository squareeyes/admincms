<?php
namespace SE\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * CmsCategory
 *
 * @ORM\Table(name="CMS_CATEGORY")
 * @ORM\Entity(repositoryClass="SE\AdminBundle\Repositories\CmsCategoryRepository")
 * @UniqueEntity("libelle", message="La catégorie que vous voulez ajouter existe déjà.")
 */
class CmsCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
    * @var CmsBanqueImage
    *
    * @ORM\ManyToOne(targetEntity="CmsBanqueImage")
    * @ORM\JoinColumns({
    *   @ORM\JoinColumn(name="refbanque", referencedColumnName="id")
    * })
    */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=60, nullable=false)
     * @Assert\Length(
     *      min = "3",
     *      max = "60",
     *      minMessage = "La catégorie doit faire au moins {{ limit }} caractères.",
     *      maxMessage = "La catégorie ne peut pas être plus long que {{ limit }} caractères."
     * )
     * @Assert\NotBlank(message="Le libellé de la catégorie est obligatoire.")
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="text", nullable=false)
     */
    private $slug;

    /**
    * @ORM\ManyToMany(targetEntity="CmsPrestation", mappedBy="categories")
    * @ORM\OrderBy({"pos" = "ASC"})
    */
    private $prestations;

    /**
    * @ORM\ManyToMany(targetEntity="CmsKeyword", mappedBy="categories")
    * @ORM\OrderBy({"pos" = "ASC"})
    */
    private $keywords;
    
    /**
     * Constructeur
     */
    public function __construct()
    {
        $this->news = new ArrayCollection();
        $this->prestations = new ArrayCollection();
        $this->keywords = new ArrayCollection();
    }

    /**
     * Test si la catégorie est rattaché à qq chose
     * @return boolean
     */
    public function isFree()
    {
        if (count($this->prestations) == 0 && count($this->keywords) == 0) {
            return true;
        }

        return false;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return CmsCategory
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return CmsCategory
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set image
     *
     * @param \SE\AdminBundle\Entity\CmsBanqueImage $image
     *
     * @return CmsCategory
     */
    public function setImage(\SE\AdminBundle\Entity\CmsBanqueImage $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \SE\AdminBundle\Entity\CmsBanqueImage 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add prestations
     *
     * @param \SE\AdminBundle\Entity\CmsPrestation $prestations
     *
     * @return CmsCategory
     */
    public function addPrestation(\SE\AdminBundle\Entity\CmsPrestation $prestations)
    {
        $this->prestations[] = $prestations;

        return $this;
    }

    /**
     * Remove prestations
     *
     * @param \SE\AdminBundle\Entity\CmsPrestation $prestations
     */
    public function removePrestation(\SE\AdminBundle\Entity\CmsPrestation $prestations)
    {
        $this->prestations->removeElement($prestations);
    }

    /**
     * Get prestations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPrestations()
    {
        return $this->prestations;
    }

    /**
     * Add keywords
     *
     * @param \SE\AdminBundle\Entity\CmsKeyword $keywords
     *
     * @return CmsCategory
     */
    public function addKeyword(\SE\AdminBundle\Entity\CmsKeyword $keywords)
    {
        $this->keywords[] = $keywords;

        return $this;
    }

    /**
     * Remove keywords
     *
     * @param \SE\AdminBundle\Entity\CmsKeyword $keywords
     */
    public function removeKeyword(\SE\AdminBundle\Entity\CmsKeyword $keywords)
    {
        $this->keywords->removeElement($keywords);
    }

    /**
     * Get keywords
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getKeywords()
    {
        return $this->keywords;
    }
}
