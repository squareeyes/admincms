<?php
namespace SE\AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * CmsParameter
 *
 * @ORM\Table(name="CMS_PARAMETER")
 * @ORM\Entity(repositoryClass="SE\AdminBundle\Repositories\CmsParameterRepository")
 */
class CmsParameter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sekey", type="string", length=20, nullable=false)
     */
    private $sekey;

    /**
     * @var string
     *
     * @ORM\Column(name="sevalue", type="text", nullable=true)
     */
    private $sevalue;

    /**
    * @var CmsBanqueImage
    *
    * @ORM\ManyToOne(targetEntity="CmsBanqueImage")
    * @ORM\JoinColumns({
    *   @ORM\JoinColumn(name="refimage", referencedColumnName="id")
    * })
    */
    private $image;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sekey
     *
     * @param string $sekey
     *
     * @return CmsParameter
     */
    public function setSekey($sekey)
    {
        $this->sekey = $sekey;

        return $this;
    }

    /**
     * Get sekey
     *
     * @return string 
     */
    public function getSekey()
    {
        return $this->sekey;
    }

    /**
     * Set sevalue
     *
     * @param string $sevalue
     *
     * @return CmsParameter
     */
    public function setSevalue($sevalue)
    {
        $this->sevalue = $sevalue;

        return $this;
    }

    /**
     * Get sevalue
     *
     * @return string 
     */
    public function getSevalue()
    {
        return $this->sevalue;
    }

    /**
     * Set image
     *
     * @param \SE\AdminBundle\Entity\CmsBanqueImage $image
     *
     * @return CmsParameter
     */
    public function setImage(\SE\AdminBundle\Entity\CmsBanqueImage $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \SE\AdminBundle\Entity\CmsBanqueImage 
     */
    public function getImage()
    {
        return $this->image;
    }
}
