<?php
namespace SE\AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * CmsNews
 *
 * @ORM\Table(name="CMS_NEWS")
 * @ORM\Entity(repositoryClass="SE\AdminBundle\Repositories\CmsNewsRepository")
 * @UniqueEntity("title", message="Le titre semble déjà avoir été utilisé.")
 */
class CmsNews
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
    * @var CmsBanqueImage
    *
    * @ORM\ManyToOne(targetEntity="CmsBanqueImage")
    * @ORM\JoinColumns({
    *   @ORM\JoinColumn(name="refbanque", referencedColumnName="id")
    * })
    * @Assert\NotBlank(message="Vous devez charger une image.")
    */
    private $image;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datepost", type="date", nullable=false)
     * @Assert\NotBlank(message="Vous devez déterminer une date de publication.")
     */
    private $datepost;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=150, nullable=false)
     * @Assert\NotBlank(message="Le titre est obligatoire.")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="text", nullable=false)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
    * @ORM\OneToMany(targetEntity="CmsNewsImage", mappedBy="news", cascade={"persist", "remove"})
    * @ORM\OrderBy({"position" = "ASC"})
    */
    private $images;

    /**
    * @ORM\ManyToMany(targetEntity="CmsNewsCategorie", inversedBy="news")
    * @ORM\JoinTable(name="CMS_NEWS_REL_CAT",
    *     joinColumns={@ORM\JoinColumn(name="refnews", referencedColumnName="id")},
    *     inverseJoinColumns={@ORM\JoinColumn(name="refcat", referencedColumnName="id")}
    * )
    * @ORM\OrderBy({"libelle" = "ASC"})
    */
    private $categories;
    
    public function __construct() {
        $this->images = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->datepost = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datepost
     *
     * @param \DateTime $datepost
     *
     * @return CmsNews
     */
    public function setDatepost($datepost)
    {
        $this->datepost = $datepost;

        return $this;
    }

    /**
     * Get datepost
     *
     * @return \DateTime 
     */
    public function getDatepost()
    {
        return $this->datepost;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return CmsNews
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return CmsNews
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return CmsNews
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set image
     *
     * @param \SE\AdminBundle\Entity\CmsBanqueImage $image
     *
     * @return CmsNews
     */
    public function setImage(\SE\AdminBundle\Entity\CmsBanqueImage $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \SE\AdminBundle\Entity\CmsBanqueImage 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add categories
     *
     * @param \SE\AdminBundle\Entity\CmsNewsCategorie $categories
     *
     * @return CmsNews
     */
    public function addCategory(\SE\AdminBundle\Entity\CmsNewsCategorie $categories)
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \SE\AdminBundle\Entity\CmsNewsCategorie $categories
     */
    public function removeCategory(\SE\AdminBundle\Entity\CmsNewsCategorie $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Add images
     *
     * @param \SE\AdminBundle\Entity\CmsNewsImage $images
     *
     * @return CmsNews
     */
    public function addImage(\SE\AdminBundle\Entity\CmsNewsImage $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \SE\AdminBundle\Entity\CmsNewsImage $images
     */
    public function removeImage(\SE\AdminBundle\Entity\CmsNewsImage $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }
}
