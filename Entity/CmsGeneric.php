<?php
namespace SE\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CmsGeneric
 *
 * @ORM\Table(name="CMS_GENERIC")
 * @ORM\Entity(repositoryClass="SE\AdminBundle\Repositories\CmsGenericRepository")
 */
class CmsGeneric
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var CmsBanqueImage
     *
     * @ORM\ManyToOne(targetEntity="CmsBanqueImage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="refbanque", referencedColumnName="id")
     * })
     */
    private $image;

    /**
     * @var CmsSlideshow
     *
     * @ORM\ManyToOne(targetEntity="CmsSlideshow", cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="refslideshow", referencedColumnName="id")
     * })
     */
    private $slideshow;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=150, nullable=false)
     * @Assert\Length(min=5, max=150, minMessage="Le titre doit être supèrieur à 5 caractères.", maxMessage="Le titre ne doit pas dépasser 150 caractères.")
     * @Assert\NotBlank(message="Le titre est obligatoire.")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="configkey", type="string", length=150, nullable=false)
     */
    private $configkey;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="text", nullable=false)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datepost", type="date", nullable=false)
     */
    private $datepost;

    /**
     * @var integer
     *
     * @ORM\Column(name="pos", type="integer", nullable=true)
     */
    private $pos;

    /**
     * @ORM\OneToMany(targetEntity="CmsGenericField", mappedBy="generic", cascade={"persist", "remove"})
     */
    private $fields;

    /**
     * @var array
     * Array preparé
     */
    private $preparedFields;

    /**
     * @var array
     * Form field import
     */
    private $submitedFields;

    /**
    * @ORM\ManyToMany(targetEntity="CmsGeneric", inversedBy="genericschildrens")
    * @ORM\JoinTable(name="CMS_GENERIC_REL",
    *     joinColumns={@ORM\JoinColumn(name="refgenparent", referencedColumnName="id")},
    *     inverseJoinColumns={@ORM\JoinColumn(name="refgenchildren", referencedColumnName="id")}
    * )
    */
    private $genericsparents;

    /**
     * @ORM\ManyToMany(targetEntity="CmsGeneric", mappedBy="genericsparents")
     */
    private $genericschildrens;

    /**
     * @ORM\OneToMany(targetEntity="CmsComment", mappedBy="generic", cascade={"persist", "remove"})
     * @ORM\OrderBy({ "datepost" = "DESC" })
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity="CmsYoutube", mappedBy="generic", cascade={"persist", "remove"})
     * @ORM\OrderBy({ "position" = "ASC", "id" = "DESC" })
     */
    private $youtubes;

    /**
     * @ORM\OneToMany(targetEntity="CmsGenericFile", mappedBy="generic", cascade={"persist", "remove"})
     * @ORM\OrderBy({ "position" = "ASC", "id" = "DESC" })
     */
    private $files;
    

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->datepost = new \DateTime();
        $this->submitedFields = array();
        $this->fields = new ArrayCollection();
        $this->genericsparents = new ArrayCollection();
        $this->genericschildrens = new ArrayCollection();
        $this->preparedFields = array();
        $this->comments = new ArrayCollection();
        $this->youtubes = new ArrayCollection();
        $this->files = new ArrayCollection();
    }

    /**
     * Recherche le champ complémentaire
     * @param string $fieldkey
     * @return CmsGenericField
     */
    public function getField($fieldkey)
    {
        if (count($this->fields) > 0 && count($this->preparedFields) != count($this->fields)) {
            $this->bindPreparedFields();
        }
        if (array_key_exists($fieldkey, $this->preparedFields)) {
            return $this->preparedFields[$fieldkey];
        }

        return false;
    }

    /**
     * Bind prepared field
     */
    public function bindPreparedFields()
    {
        foreach ($this->fields as $field) {
            if (is_object($field->getImage())) {
                $this->preparedFields[$field->getFieldkey()] = $field->getImage();
            } else {
                $this->preparedFields[$field->getFieldkey()] = $field->getFieldvalue();
            }
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return CmsGeneric
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return CmsGeneric
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return CmsGeneric
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set datepost
     *
     * @param \DateTime $datepost
     *
     * @return CmsGeneric
     */
    public function setDatepost($datepost)
    {
        $this->datepost = $datepost;

        return $this;
    }

    /**
     * Get datepost
     *
     * @return \DateTime 
     */
    public function getDatepost()
    {
        return $this->datepost;
    }

    /**
     * Set pos
     *
     * @param integer $pos
     *
     * @return CmsGeneric
     */
    public function setPos($pos)
    {
        $this->pos = $pos;

        return $this;
    }

    /**
     * Get pos
     *
     * @return integer 
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * Set image
     *
     * @param \SE\AdminBundle\Entity\CmsBanqueImage $image
     *
     * @return CmsGeneric
     */
    public function setImage(\SE\AdminBundle\Entity\CmsBanqueImage $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \SE\AdminBundle\Entity\CmsBanqueImage 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set slideshow
     *
     * @param \SE\AdminBundle\Entity\CmsSlideshow $slideshow
     *
     * @return CmsGeneric
     */
    public function setSlideshow(\SE\AdminBundle\Entity\CmsSlideshow $slideshow = null)
    {
        $this->slideshow = $slideshow;

        return $this;
    }

    /**
     * Get slideshow
     *
     * @return \SE\AdminBundle\Entity\CmsSlideshow 
     */
    public function getSlideshow()
    {
        return $this->slideshow;
    }

    /**
     * Add fields
     *
     * @param \SE\AdminBundle\Entity\CmsGenericField $fields
     *
     * @return CmsGeneric
     */
    public function addField(\SE\AdminBundle\Entity\CmsGenericField $fields)
    {
        $this->fields[] = $fields;

        return $this;
    }

    /**
     * Remove fields
     *
     * @param \SE\AdminBundle\Entity\CmsGenericField $fields
     */
    public function removeField(\SE\AdminBundle\Entity\CmsGenericField $fields)
    {
        $this->fields->removeElement($fields);
    }

    /**
     * Get fields
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Add genericsparents
     *
     * @param \SE\AdminBundle\Entity\CmsGeneric $genericsparents
     *
     * @return CmsGeneric
     */
    public function addGenericsparent(\SE\AdminBundle\Entity\CmsGeneric $genericsparents)
    {
        $this->genericsparents[] = $genericsparents;

        return $this;
    }

    /**
     * Remove genericsparents
     *
     * @param \SE\AdminBundle\Entity\CmsGeneric $genericsparents
     */
    public function removeGenericsparent(\SE\AdminBundle\Entity\CmsGeneric $genericsparents)
    {
        $this->genericsparents->removeElement($genericsparents);
    }

    /**
     * Get genericsparents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGenericsparents()
    {
        return $this->genericsparents;
    }

    /**
     * Add genericschildrens
     *
     * @param \SE\AdminBundle\Entity\CmsGeneric $genericschildrens
     *
     * @return CmsGeneric
     */
    public function addGenericschildren(\SE\AdminBundle\Entity\CmsGeneric $genericschildrens)
    {
        $this->genericschildrens[] = $genericschildrens;

        return $this;
    }

    /**
     * Remove genericschildrens
     *
     * @param \SE\AdminBundle\Entity\CmsGeneric $genericschildrens
     */
    public function removeGenericschildren(\SE\AdminBundle\Entity\CmsGeneric $genericschildrens)
    {
        $this->genericschildrens->removeElement($genericschildrens);
    }

    /**
     * Get genericschildrens
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGenericschildrens()
    {
        return $this->genericschildrens;
    }

    /**
     * Set configkey
     *
     * @param string $configkey
     *
     * @return CmsGeneric
     */
    public function setConfigkey($configkey)
    {
        $this->configkey = $configkey;

        return $this;
    }

    /**
     * Get configkey
     *
     * @return string 
     */
    public function getConfigkey()
    {
        return $this->configkey;
    }

    /**
     * Set submitedFields
     *
     * @param string $submitedFields
     *
     * @return CmsGeneric
     */
    public function setSubmitedFields($submitedFields)
    {
        $this->submitedFields = $submitedFields;

        return $this;
    }

    /**
     * Get submitedFields
     *
     * @return string 
     */
    public function getSubmitedFields()
    {
        return $this->submitedFields;
    }

    /**
     * Add comments
     *
     * @param \SE\AdminBundle\Entity\CmsComment $comments
     *
     * @return CmsGeneric
     */
    public function addComment(\SE\AdminBundle\Entity\CmsComment $comments)
    {
        $this->comments[] = $comments;

        return $this;
    }

    /**
     * Remove comments
     *
     * @param \SE\AdminBundle\Entity\CmsComment $comments
     */
    public function removeComment(\SE\AdminBundle\Entity\CmsComment $comments)
    {
        $this->comments->removeElement($comments);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Add youtubes
     *
     * @param \SE\AdminBundle\Entity\CmsYoutube $youtubes
     *
     * @return CmsGeneric
     */
    public function addYoutube(\SE\AdminBundle\Entity\CmsYoutube $youtubes)
    {
        $this->youtubes[] = $youtubes;

        return $this;
    }

    /**
     * Remove youtubes
     *
     * @param \SE\AdminBundle\Entity\CmsYoutube $youtubes
     */
    public function removeYoutube(\SE\AdminBundle\Entity\CmsYoutube $youtubes)
    {
        $this->youtubes->removeElement($youtubes);
    }

    /**
     * Get youtubes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getYoutubes()
    {
        return $this->youtubes;
    }

    /**
     * Add files
     *
     * @param \SE\AdminBundle\Entity\CmsGenericFile $files
     *
     * @return CmsGeneric
     */
    public function addFile(\SE\AdminBundle\Entity\CmsGenericFile $files)
    {
        $this->files[] = $files;

        return $this;
    }

    /**
     * Remove files
     *
     * @param \SE\AdminBundle\Entity\CmsGenericFile $files
     */
    public function removeFile(\SE\AdminBundle\Entity\CmsGenericFile $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFiles()
    {
        return $this->files;
    }
}
