<?php
namespace SE\AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CmsSeo
 *
 * @ORM\Table(name="CMS_SEO")
 * @ORM\Entity(repositoryClass="SE\AdminBundle\Repositories\CmsSeoRepository")
 */
class CmsSeo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="pagekey", type="string", length=50, nullable=false)
     */
    private $pagekey;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=150, nullable=true)
     * @Assert\Length(min=10, max=150, minMessage="Pour améliorer le référencement le titre de la page doit être supèrieur à 10 caractères.", maxMessage="L'optimisation du titre ne doit pas dépasser 150 caractères.")
     * @Assert\NotBlank(message="Le titre de la page est obligatoire.")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=250, nullable=true)
     * @Assert\Length(min=20, max=150, minMessage="Pour améliorer le référencement la description de la page doit être supèrieur à 20 caractères.", maxMessage="L'optimisation de la description ne doit pas dépasser 255 caractères.")
     * @Assert\NotBlank(message="La description de la page est obligatoire.")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
    * @var CmsBanqueImage
    *
    * @ORM\ManyToOne(targetEntity="CmsBanqueImage")
    * @ORM\JoinColumns({
    *   @ORM\JoinColumn(name="refbanque", referencedColumnName="id")
    * })
    */
    private $image;

    /**
    * @var CmsSlideshow
    *
    * @ORM\ManyToOne(targetEntity="CmsSlideshow", cascade={"persist", "remove"})
    * @ORM\JoinColumns({
    *   @ORM\JoinColumn(name="refslideshow", referencedColumnName="id")
    * })
    */
    private $slideshow;

    /**
    * @var CmsVideo
    *
    * @ORM\ManyToOne(targetEntity="CmsVideo", cascade={"persist", "remove"})
    * @ORM\JoinColumns({
    *   @ORM\JoinColumn(name="refvideo", referencedColumnName="id")
    * })
    */
    private $video;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pagekey
     *
     * @param string $pagekey
     *
     * @return CmsSeo
     */
    public function setPagekey($pagekey)
    {
        $this->pagekey = $pagekey;

        return $this;
    }

    /**
     * Get pagekey
     *
     * @return string 
     */
    public function getPagekey()
    {
        return $this->pagekey;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return CmsSeo
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return CmsSeo
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return CmsSeo
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set image
     *
     * @param \SE\AdminBundle\Entity\CmsBanqueImage $image
     *
     * @return CmsSeo
     */
    public function setImage(\SE\AdminBundle\Entity\CmsBanqueImage $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \SE\AdminBundle\Entity\CmsBanqueImage 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set slideshow
     *
     * @param \SE\AdminBundle\Entity\CmsSlideshow $slideshow
     *
     * @return CmsSeo
     */
    public function setSlideshow(\SE\AdminBundle\Entity\CmsSlideshow $slideshow = null)
    {
        $this->slideshow = $slideshow;

        return $this;
    }

    /**
     * Get slideshow
     *
     * @return \SE\AdminBundle\Entity\CmsSlideshow 
     */
    public function getSlideshow()
    {
        return $this->slideshow;
    }

    /**
     * Set video
     *
     * @param \SE\AdminBundle\Entity\CmsVideo $video
     *
     * @return CmsSeo
     */
    public function setVideo(\SE\AdminBundle\Entity\CmsVideo $video = null)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return \SE\AdminBundle\Entity\CmsVideo 
     */
    public function getVideo()
    {
        return $this->video;
    }
}
