<?php
namespace SE\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CmsGeneric
 *
 * @ORM\Table(name="CMS_YOUTUBE")
 * @ORM\Entity
 */
class CmsYoutube
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var CmsBanqueImage
     *
     * @ORM\ManyToOne(targetEntity="CmsBanqueImage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="refbanque", referencedColumnName="id")
     * })
     * @Assert\NotNull(message="Une illustration pour la vidéo est obligatoire")
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=150, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=false)
     */
    private $url;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=false)
     */
    private $position;

    /**
     * @var CmsGeneric
     *
     * @ORM\ManyToOne(targetEntity="CmsGeneric", inversedBy="youtubes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="refgeneric", referencedColumnName="id")
     * })
     */
    private $generic;

    /**
     * Get key from youtube link
     * @return string
     */
    public function getKey()
    {
        $arr = explode('/', $this->url);
        if (is_array($arr)) {
            return array_pop($arr);
        }

        return $this->url;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->position = 0;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return CmsYoutube
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return CmsYoutube
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set image
     *
     * @param \SE\AdminBundle\Entity\CmsBanqueImage $image
     *
     * @return CmsYoutube
     */
    public function setImage(\SE\AdminBundle\Entity\CmsBanqueImage $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \SE\AdminBundle\Entity\CmsBanqueImage 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set generic
     *
     * @param \SE\AdminBundle\Entity\CmsGeneric $generic
     *
     * @return CmsYoutube
     */
    public function setGeneric(\SE\AdminBundle\Entity\CmsGeneric $generic = null)
    {
        $this->generic = $generic;

        return $this;
    }

    /**
     * Get generic
     *
     * @return \SE\AdminBundle\Entity\CmsGeneric 
     */
    public function getGeneric()
    {
        return $this->generic;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return CmsYoutube
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }
}
