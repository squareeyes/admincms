<?php
namespace SE\AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CmsSlideshowImage
 *
 * @ORM\Table(name="CMS_SLIDESHOW_IMAGE")
 * @ORM\Entity
 */
class CmsSlideshowImage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
    * @var CmsBanqueImage
    *
    * @ORM\ManyToOne(targetEntity="CmsBanqueImage")
    * @ORM\JoinColumns({
    *   @ORM\JoinColumn(name="refbanque", referencedColumnName="id")
    * })
    * @Assert\NotBlank(message="Vous devez charger une image.")
    */
    private $image;

    /**
    * @var CmsSlideshow
    *
    * @ORM\ManyToOne(targetEntity="CmsSlideshow", inversedBy="images")
    * @ORM\JoinColumns({
    *   @ORM\JoinColumn(name="refslideshow", referencedColumnName="id")
    * })
    */
    private $slideshow;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=150, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", nullable=true)
     */
    private $url;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=false)
     */
    private $position;

    public function __construct()
    {
        $this->position = 0;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return CmsSlideshowImage
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set image
     *
     * @param \SE\AdminBundle\Entity\CmsBanqueImage $image
     *
     * @return CmsSlideshowImage
     */
    public function setImage(\SE\AdminBundle\Entity\CmsBanqueImage $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \SE\AdminBundle\Entity\CmsBanqueImage 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set slideshow
     *
     * @param \SE\AdminBundle\Entity\CmsSlideshow $slideshow
     *
     * @return CmsSlideshowImage
     */
    public function setSlideshow(\SE\AdminBundle\Entity\CmsSlideshow $slideshow = null)
    {
        $this->slideshow = $slideshow;

        return $this;
    }

    /**
     * Get slideshow
     *
     * @return \SE\AdminBundle\Entity\CmsSlideshow 
     */
    public function getSlideshow()
    {
        return $this->slideshow;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return CmsSlideshowImage
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return CmsSlideshowImage
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return CmsSlideshowImage
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }
}
