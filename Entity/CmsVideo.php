<?php
namespace SE\AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CmsVideo
 *
 * @ORM\Table(name="CMS_VIDEO")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class CmsVideo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="urlmp4", type="text", nullable=true)
     */
    private $urlmp4;

    /**
     * @var string
     * Don't persisted
     * @Assert\File(
     *     maxSize = "100M",
     *     mimeTypes = {"video/mp4"},
     *     mimeTypesMessage = "Votre vidéo doit être au format mp4"
     * )
     */
    protected $submitedMp4;

    /**
     * @var string
     *
     * @ORM\Column(name="urlogg", type="text", nullable=true)
     */
    private $urlogg;

    /**
     * @var string
     * Don't persisted
     * @Assert\File(
     *     maxSize = "100M"
     * )
     */
    protected $submitedOgg;

    /**
     * @var string
     *
     * @ORM\Column(name="urlwebm", type="text", nullable=true)
     */
    private $urlwebm;

    /**
     * @var string
     * Don't persisted
     * @Assert\File(
     *     maxSize = "100M",
     *     mimeTypes = {"video/webm"},
     *     mimeTypesMessage = "Votre vidéo doit être au format webm"
     * )
     */
    protected $submitedWebm;

    /**
     * @ORM\PreRemove
     */
    public function removeVideos()
    {
        if ($this->isReadable()) {
            unlink($this->urlmp4);
            unlink($this->urlogg);
            unlink($this->urlwebm);
        }
    }

    /**
     * Check if video is readable
     * @return boolean
     */
    public function isReadable()
    {
        if ($this->urlmp4 != null && $this->urlogg != null && $this->urlwebm != null) {
            return true;
        }

        return false;
    }

    /**
     * Supprime les fichiers videos
     * @return CmsVideo
     */
    public function eraseSubmitedVideos()
    {
        $this->submitedWebm = null;
        $this->submitedOgg = null;
        $this->submitedMp4 = null;

        return $this;
    }    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set urlmp4
     *
     * @param string $urlmp4
     *
     * @return CmsVideo
     */
    public function setUrlmp4($urlmp4)
    {
        $this->urlmp4 = $urlmp4;

        return $this;
    }

    /**
     * Get urlmp4
     *
     * @return string 
     */
    public function getUrlmp4()
    {
        return $this->urlmp4;
    }

    /**
     * Set urlogg
     *
     * @param string $urlogg
     *
     * @return CmsVideo
     */
    public function setUrlogg($urlogg)
    {
        $this->urlogg = $urlogg;

        return $this;
    }

    /**
     * Get urlogg
     *
     * @return string 
     */
    public function getUrlogg()
    {
        return $this->urlogg;
    }

    /**
     * Set urlwebm
     *
     * @param string $urlwebm
     *
     * @return CmsVideo
     */
    public function setUrlwebm($urlwebm)
    {
        $this->urlwebm = $urlwebm;

        return $this;
    }

    /**
     * Get urlwebm
     *
     * @return string 
     */
    public function getUrlwebm()
    {
        return $this->urlwebm;
    }

    public function getSubmitedMp4() 
    {
        return $this->submitedMp4;
    }

    public function setSubmitedMp4($file) 
    {
        $this->submitedMp4 = $file;
        return $this;
    }

    public function getSubmitedOgg() 
    {
        return $this->submitedOgg;
    }

    public function setSubmitedOgg($file) 
    {
        $this->submitedOgg = $file;
        return $this;
    }

    public function getSubmitedWebm() 
    {
        return $this->submitedWebm;
    }

    public function setSubmitedWebm($file) 
    {
        $this->submitedWebm = $file;
        return $this;
    }
}
