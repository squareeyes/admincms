<?php
namespace SE\AdminBundle\Model;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

abstract class CmsUser extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
    * @var SE\AdminBundle\Entity\CmsUserAdresse
    * @ORM\OneToOne(targetEntity="SE\AdminBundle\Entity\CmsUserAdresse", cascade={"persist", "remove"})
    * @ORM\JoinColumn(name="refadresse", referencedColumnName="id")
    */
    protected $adresse;

    /**
    * @var SE\AdminBundle\Entity\CmsUserAdresse
    * @ORM\OneToOne(targetEntity="SE\AdminBundle\Entity\CmsUserAdresse", cascade={"persist", "remove"})
    * @ORM\JoinColumn(name="refadresselivraison", referencedColumnName="id")
    */
    protected $adresselivraison;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdat", type="datetime", nullable=true)
     */
    protected $createdat;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=50, nullable=true)
     */
    protected $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=50, nullable=true)
     */
    protected $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=50, nullable=true)
     */
    protected $company;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=30, nullable=true)
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="mobil", type="string", length=30, nullable=true)
     */
    protected $mobil;

    /**
     * @var string
     *
     * @ORM\Column(name="admin", type="string", length=1, nullable=false)
     */
    protected $admin;

    public function __construct() {
        parent::__construct();
        $this->createdat = new \DateTime();
        $this->admin = 0;
    }

    public function getId() { return $this->id; }
    public function getAdresse() { return $this->adresse; }
    public function getAdresselivraison() { return $this->adresselivraison; }
    public function getCreatedat() { return $this->createdat; }
    public function getLastname() { return $this->lastname; }
    public function getFirstname() { return $this->firstname; }
    public function getCompany() { return $this->company; }
    public function getPhone() { return $this->phone; }
    public function getMobil() { return $this->mobil; }
    public function getAdmin() { return $this->admin; }

    public function setAdresse($i) { $this->adresse = $i; }
    public function setAdresselivraison($i) { $this->adresselivraison = $i; }
    public function setCreatedat($i) { $this->createdat = $i; }
    public function setLastname($i) { $this->lastname = $i; }
    public function setFirstname($i) { $this->firstname = $i; }
    public function setCompany($i) { $this->company = $i; }
    public function setPhone($i) { $this->phone = $i; }
    public function setMobil($i) { $this->mobil = $i; }
    public function setAdmin($i) { $this->admin = $i; }
}