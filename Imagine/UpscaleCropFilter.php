<?php
namespace SE\AdminBundle\Imagine;

use Imagine\Filter\Basic\Resize;
use Imagine\Image\ImageInterface;
use Imagine\Image\Box;
use Imagine\Filter\Basic\Thumbnail;
use Liip\ImagineBundle\Imagine\Filter\Loader\LoaderInterface;

/**
 * Upscale and crop after filter
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class UpscaleCropFilter implements LoaderInterface
{
    /**
     * Apply filter
     * @param ImageInterface $image
     * @param array          $options
     * @return Response
     */
    public function load(ImageInterface $image, array $options = array())
    {
        if (!isset($options['size'])) {
            throw new InvalidArgumentException('Missing size option.');
        }
        list($width, $height) = $options['size'];

        // Upscale
        $size = $image->getSize();
        $origWidth = $size->getWidth();
        $origHeight = $size->getHeight();
        if ($origWidth < $width || $origHeight < $height) {
            $widthRatio = $width / $origWidth ;
            $heightRatio = $height / $origHeight;
            $ratio = $widthRatio > $heightRatio ? $widthRatio : $heightRatio;
            $newOrigWidth = $origWidth * $ratio;
            $newOrigHeight = $origHeight * $ratio;
            $filter = new Resize(new Box($origWidth * $ratio, $origHeight * $ratio));
            $filter->apply($image);
        } else {
            $newOrigWidth = $origWidth;
            $newOrigHeight = $origHeight;
        }

        // Outbound
        $mode = ImageInterface::THUMBNAIL_OUTBOUND;
        if (null === $width || null === $height) {
            if (null === $height) {
                $height = (int) (($width / $newOrigWidth) * $origHeight);
            } else if (null === $width) {
                $width = (int) (($height / $newOrigHeight) * $newOrigWidth);
            }
        }
        $filter = new Thumbnail(new Box($width, $height), $mode);
        $image = $filter->apply($image);

        return $image;
    }
}