# SEAdminBundle
> Square Eyes CMS  
[1 - How to install SEAdmin](https://bitbucket.org/squareeyes/admincms/src/master/Resources/doc/install.md)  
[2 - How to configure SEAdmin](https://bitbucket.org/squareeyes/admincms/src/master/Resources/doc/config.md)  
[3 - Define your's firewall](https://bitbucket.org/squareeyes/admincms/src/master/Resources/doc/firewall.md)  
[4 - Create your own user entity](https://bitbucket.org/squareeyes/admincms/src/master/Resources/doc/user.md)  
[5 - Finish install](https://bitbucket.org/squareeyes/admincms/src/master/Resources/doc/finish.md)  