<?php
namespace SE\AdminBundle;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class appelée lors du chargement du bundle
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class SEAdminBundle extends Bundle
{
    /**
     * Override FosUserBundle
     * @return string
     */
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
