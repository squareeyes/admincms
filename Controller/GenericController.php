<?php
namespace SE\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use SE\AdminBundle\Form\Type\GenericType;
use SE\AdminBundle\Entity\CmsGeneric;

/**
 * Generic management
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class GenericController extends Controller
{
    /**
     * Afficher le listing des generiques
     * @param string $configkey
     * @return Response
     */
    public function listingAction($configkey)
    {
        $config = $this->container->getParameter('se_admin')['modules']['generics'];
        if (array_key_exists($configkey, $config)) {
            $entityManager = $this->getDoctrine()->getManager();
            $field = "title";
            $way = "ASC";
            if (array_key_exists('order', $config[$configkey]) && array_key_exists('field', $config[$configkey]['order']) && array_key_exists('way', $config[$configkey]['order'])) {
                $field = $config[$configkey]['order']['field'];
                $way = $config[$configkey]['order']['way'];
            }
            $generics = $entityManager->getRepository('SEAdminBundle:CmsGeneric')->findAllByConfigKey($configkey, $field, $way);

            return $this->render('SEAdminBundle:generic:listing.html.twig', array(
                'configkey' => $configkey,
                'generics'  => $generics,
                'message'   => $this->get('session')->getFlashBag()->get('message')
            ));
        }

        throw $this->createNotFoundException();
    }

    /**
     * Ajoute une entrée générique
     * @param string $configkey
     * @param int    $refgen
     * @return Response
     */
    public function formAction($configkey, $refgen)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $generic = new CmsGeneric();
        if ($refgen) {
            $generic = $entityManager->getRepository('SEAdminBundle:CmsGeneric')->findOneByConfigKeyAndId($configkey, $refgen);    
            if (!$generic) {
                throw $this->createNotFoundException();
            }
        }

        $config = $this->container->getParameter('se_admin')['modules']['generics'];
        if (array_key_exists($configkey, $config)) {
            $form = $this->get('form.factory')->create(new GenericType($config, $configkey), $generic);

            if ($newGeneric = $this->get('se.generic.form.handler')->process($form, $configkey)) {
                $this->get('session')->getFlashBag()->set('message', 'Les informations ont bien été enregistrées.');

                return $this->redirect($this->generateUrl('SEAdminBundle_generic_add', array(
                    'configkey' => $configkey,
                    'refgen'    => $newGeneric->getId()
                )));
            }

            return $this->render('SEAdminBundle:generic:form.html.twig', array(
                'configkey' => $configkey,
                'form'      => $form->createView(),
                'generic'   => $generic->getId() ? $generic : false,
                'message'   => $this->get('session')->getFlashBag()->get('message'),
                'erreur'    => $this->get('session')->getFlashBag()->get('error')
            ));
        }

        throw $this->createNotFoundException();
    }

    /**
     * Sort generics
     * @param string $configkey
     * @return Response
     */
    public function sortAction($configkey)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $post = $this->getRequest()->request;
        foreach ($post->get('refgeneric') as $k => $refgen) {
            $gen = $entityManager->getRepository('SEAdminBundle:CmsGeneric')->findOneBy(array('id' => $refgen));
            if ($gen) {
                $gen->setPos($k);
                $entityManager->persist($gen);
            }
        }
        $entityManager->flush();

        return new Response(1);
    }

    /**
     * Supprime une entrée
     * @param string $configkey
     * @param int    $refgen
     * @return Response
     */
    public function removeAction($configkey, $refgen)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $generic = $entityManager->getRepository('SEAdminBundle:CmsGeneric')->findOneByConfigKeyAndId($configkey, $refgen);    
        if (!$generic) {
            throw $this->createNotFoundException();
        }

        $entityManager->remove($generic);
        $entityManager->flush();

        $this->get('session')->getFlashBag()->set('message', 'Votre suppression à bien été effectuée.');

        return $this->redirect($this->generateUrl('SEAdminBundle_generic_listing', array(
            'configkey' => $configkey
        )));
    }
}