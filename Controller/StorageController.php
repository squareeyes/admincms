<?php
namespace SE\AdminBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use SE\AdminBundle\Entity\CmsStorage;
use SE\AdminBundle\Form\Type\StorageType;

/**
 * Controller lié aux fichiers
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class StorageController extends Controller
{
    /**
     * Listing des fichiers uploads
     * @return Response
     */
    public function listingAction()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $files = $entityManager->getRepository('SEAdminBundle:CmsStorage')->findBy(array(), array('position' => 'ASC'));

        return $this->render('SEAdminBundle:storage:listing.html.twig', array(
            'files' => $files
        ));
    }

    /**
     * Ajoute un fichier
     * @return Response
     */
    public function addAction()
    {
        $form = $this->get('form.factory')->create(new StorageType());
        if ($this->get('se.storage.form.handler')->process($form)) {
            return $this->redirect($this->generateUrl('SEAdminBundle_storage_listing'));
        }

        return $this->render('SEAdminBundle:storage:add.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * Supprime un fichier
     * @param CmsStorage $file
     * @return Redirect
     */
    public function removeAction(CmsStorage $file)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($file);
        $entityManager->flush();

        return $this->redirect($this->generateUrl('SEAdminBundle_storage_listing'));
    }

    /**
     * Sort generics
     * @return Response
     */
    public function sortAction()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $post = $this->getRequest()->request;
        foreach ($post->get('reffiles') as $k => $reffile) {
            $file = $entityManager->getRepository('SEAdminBundle:CmsStorage')->findOneBy(array('id' => $reffile));
            if ($file) {
                $file->setPosition($k);
                $entityManager->persist($file);
            }
        }
        $entityManager->flush();

        return new Response(1);
    }
}