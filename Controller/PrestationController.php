<?php
namespace SE\AdminBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use SE\AdminBundle\Form\Type\PrestationType;
use SE\AdminBundle\Entity\CmsPrestation;

/**
 * Controller pour les actions liées aux prestations
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class PrestationController extends Controller
{
    /**
     * Page pour ajouter un prestation
     * @return Response
     */
    public function addAction()
    {
        $form = $this->get('form.factory')->create(new PrestationType());
        if ($this->get('se.prestation.form.handler')->process($form)) {
            return $this->redirect($this->generateUrl('SEAdminBundle_prestation_listing'));
        }
        
        return $this->render('SEAdminBundle:prestation:add.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * Listing des prestations
     * @return Response
     */
    public function listingAction()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $prestas = $entityManager->getRepository('SEAdminBundle:CmsPrestation')->findAll();

        return $this->render('SEAdminBundle:prestation:listing.html.twig', array(
            'prestas' => $prestas
        ));
    }

    /**
     * Page pour modifier une prestation
     * @param CmsPrestation $presta
     * @return Response
     */
    public function showAction(CmsPrestation $presta)
    {
        $post = $this->getRequest()->request;
        $form = $this->get('form.factory')->create(new PrestationType(), $presta);
        if ($post->get('se_adminbundle_prestationtype') && $this->get('se.prestation.form.handler')->process($form)) {
            $this->get('session')->getFlashBag()->set('updated', true);

            return $this->redirect($this->generateUrl('SEAdminBundle_prestation_show', array('id' => $presta->getId())));
        }

        return $this->render('SEAdminBundle:prestation:show.html.twig', array(
            'p'         => $presta,
            'form'      => $form->createView(),
            'updated'   => $this->get('session')->getFlashBag()->get('updated'),
            'message'   => $this->get('session')->getFlashBag()->get('message'),
            'erreur'    => $this->get('session')->getFlashBag()->get('error')
        ));
    }

    /**
     * Action pour enregistrer le tri des prestations en AJAX
     * @return int
     */
    public function sortAction()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $post = $this->getRequest()->request;
        foreach ($post->get('refprestas') as $k => $refpresta) {
            $presta = $entityManager->getRepository('SEAdminBundle:CmsPrestation')->findOneBy(array('id' => $refpresta));
            if ($presta) {
                $presta->setPos($k);
                $entityManager->persist($presta);
            }
        }
        $entityManager->flush();

        return new Response(1);
    }

    /**
     * Suppression d'une prestation
     * @param CmsPrestation $presta
     * @return Redirect
     */
    public function removeAction(CmsPrestation $presta)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($presta);
        $entityManager->flush();

        return $this->redirect($this->generateUrl('SEAdminBundle_prestation_listing'));
    }
}