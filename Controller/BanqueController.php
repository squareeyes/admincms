<?php
namespace SE\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller,
    Symfony\Component\HttpFoundation\Response,
    SE\AdminBundle\Form\Type\BanqueType,
    SE\AdminBundle\Entity\CmsBanqueImage;

/**
 * Controller pour les pages liée à la gestion de la banque d'image
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class BanqueController extends Controller
{
    /**
     * Formulaire pour l'ajout d'image
     * @return Response
     */
    public function addAction()
    { 
        $form = $this->get('form.factory')->create(new BanqueType());
        if ($this->get('se.banque.form.handler')->process($form)) {
            $this->get('session')->getFlashBag()->set('success_banque', 'Votre image à bien été sauvegardée dans votre banque d\'image.');

            return $this->redirect($this->generateUrl('SEAdminBundle_banque_listing'));
        }

        return $this->render('SEAdminBundle:banque:add.html.twig', array(
            'form'    => $form->createView(),
            'message' => $this->get('session')->getFlashBag()->get('success_banque')
        ));
    }


    /**
     * Listing des images en banque
     * @return Response
     */
    public function listingAction()
    {
        $em = $this->getDoctrine()->getManager();
        $banques = $em->getRepository('SEAdminBundle:CmsBanqueImage')->findBy(array(), array('id' => 'DESC'));

        return $this->render('SEAdminBundle:banque:listing.html.twig', array(
            'banques' => $banques,
            'message' => $this->get('session')->getFlashBag()->get('message'),
            'erreur'  => $this->get('session')->getFlashBag()->get('erreur')
        ));
    }


    /**
     * Affichage du formulaire de modification de l'image
     * @param CmsBanqueImage $img
     * @return Response
     */
    public function showAction(CmsBanqueImage $img)
    {
        $form = $this->get('form.factory')->create(new BanqueType(), $img);
        if ($this->get('se.banque.form.handler')->process($form)) {
            $this->get('session')->getFlashBag()->set('success_banque', 'Votre image à bien été modifiée dans votre banque d\'image.');

            return $this->redirect($this->generateUrl('SEAdminBundle_banque_show', array('id' => $img->getId())));
        }

        return $this->render('SEAdminBundle:banque:show.html.twig', array(
            'img'     => $img,
            'form'    => $form->createView(),
            'message' => $this->get('session')->getFlashBag()->get('success_banque')
        ));
    }


    /**
     * Supprime une image de la banque d'image
     * @param CmsBanqueImage $img
     * @return Redirect
     */
    public function removeAction(CmsBanqueImage $img)
    {
        $em = $this->getDoctrine()->getManager();
        if (!$this->get('se.banque')->isUsed($img)) {
            unlink($img->getUrlimg());
            unlink('media/admin/seadmin_200x120/'.$img->getUrlimg());
            $em->remove($img);
            $em->flush();
            $this->get('session')->getFlashBag()->set('message', "L'image a bien été supprimée.");
        } else {
            $this->get('session')->getFlashBag()->set('erreur', "L'image que vous voulez supprimer est utilisée dans votre site internet. Pour la supprimer veuillez d'abord la supprimer de votre site internet.");
        }

        return $this->redirect($this->generateUrl('SEAdminBundle_banque_listing'));
    }


    /**
     * Génére la popin pour le choix d'une image en banque
     * @return Response
     */
    public function openAction()
    {
        $msg = "";
        $post = $this->getRequest()->request;
        $em = $this->getDoctrine()->getManager();
        $form = $this->get('form.factory')->create(new BanqueType());
        if ($post->get('se_adminbundle_banquetype')) {
            if ($this->get('se.banque.form.handler')->process($form)) {
                $msg = '<div class="alert alert-success"><strong>Ok !</strong> l\'image a bien été ajoutée dans votre banque.</div>';
            } else {
                $msg = '<div class="alert alert-danger"><strong>Erreur !</strong> l\'image n\'a pas été ajoutée, une erreur est survenue.</div>';
            }
        }
        $banques = $em->getRepository('SEAdminBundle:CmsBanqueImage')->findBy(array(), array('id' => 'DESC'));

        return $this->render('SEAdminBundle:banque:add-ajax.html.twig', array(
            'banque' => $banques,
            'form'   => $form->createView(),
            'msg'    => $msg
        ));
    }


    /**
     * Ajoute une image en banque via requête AJAX
     * @return Int
     */
    public function addAjaxAction()
    {
        $form = $this->get('form.factory')->create(new BanqueType());
        if ($img = $this->get('se.banque.form.handler')->process($form)) {
            return new Response($img->getId());
        }

        return new Response(0);
    }

    /**
     * Retourne une image via l'id de la banque
     * @param CmsBanqueImage $img
     * @return Response
     */
    public function generateImageAction(CmsBanqueImage $img)
    {
        return $this->render('SEAdminBundle:banque:image.html.twig', array(
            'img' => $img
        ));
    }
}
