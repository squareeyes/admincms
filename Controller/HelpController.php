<?php
namespace SE\AdminBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
/**
 * Controller pour les pages liées à l'assitance
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class HelpController extends Controller
{
    /**
     * Affiche les catégories d'aide
     * @return Response
     */
    public function menuAction()
    {
    	$invoice = $this->get('se.api')->getNextInvoice();
        return $this->render('SEAdminBundle:help:menu.html.twig', array(
        	'invoice' => $invoice
        ));
    }
}