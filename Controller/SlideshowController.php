<?php
namespace SE\AdminBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SE\AdminBundle\Entity\CmsSlideshow;
use SE\AdminBundle\Entity\CmsSlideshowImage;
use SE\AdminBundle\Form\Type\SlideshowType;
use SE\AdminBundle\Form\Type\SlideshowImageType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller lié aux diaporamas
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class SlideshowController extends Controller
{
    /**
     * Modification et creation du slideshow
     * @param CmsSlideshow $slideshow
     * @return Response
     */
    public function updateAction(CmsSlideshow $slideshow)
    {
        $post = $this->getRequest()->request;

        // Formulaire de configuration du diaporama
        $slideForm = $this->get('form.factory')->create(new SlideshowType(), $slideshow);
        if ($post->get('se_adminbundle_slideshowtype')) {
            if ($this->get('se.slideshow.form.handler')->process($slideForm)) {
                $this->get('session')->getFlashBag()->set('message', 'Le diaporama à bien été modifié.');
            } else {
                $this->get('session')->getFlashBag()->set('error', 'Le diaporama n\'a pas été modifié suite à une erreur.');
            }
        }

        if ($post->get('back')) {
            return $this->redirect($post->get('back'));
        }

        return $this->render('SEAdminBundle:slideshow:update.html.twig', array(
            'slideForm' => $slideForm->createView(),
            'slideshow' => $slideshow
        ));
    }

    /**
     * Formulaire pour ajouter des images au diaporama
     * @param CmsSlideshow $slideshow
     * @return Response
     */
    public function addImageAction(CmsSlideshow $slideshow, $back = null)
    {
        $post = $this->getRequest()->request;

        // Formulaire d'ajoute d'image
        $image = new CmsSlideshowImage();
        $image->setSlideshow($slideshow);
        $imageForm = $this->get('form.factory')->create(new SlideshowImageType(), $image);
        if ($post->get('se_adminbundle_slideshowimagetype')) {
            if ($this->get('se.slideshow.form.handler')->process($imageForm)) {
                $this->get('session')->getFlashBag()->set('message', 'L\'image à bien été ajoutée.');
            } else {
                $this->get('session')->getFlashBag()->set('error', 'Le l\'image n\'a pas été ajoutée suite à une erreur.');
            }
        }

        if ($post->get('back')) {
            return $this->redirect($post->get('back'));
        }

        return $this->render('SEAdminBundle:slideshow:add-image.html.twig', array(
            'slideshow' => $slideshow,
            'imageForm' => $imageForm->createView(),
            'back'      => $back
        ));
    }

    /**
     * Formulaire pour modifier l'image d'un diaporama
     * @param CmsSlideshowImage $image
     * @return Response
     */
    public function updateImageAction(CmsSlideshowImage $image)
    {
        $post = $this->getRequest()->request;

        // Formulaire d'ajoute d'image
        $imageForm = $this->get('form.factory')->create(new SlideshowImageType(), $image);
        if ($post->get('se_adminbundle_slideshowimagetype')) {
            if ($this->get('se.slideshow.form.handler')->process($imageForm)) {
                $this->get('session')->getFlashBag()->set('message', 'L\'image à bien été modifiée.');
            } else {
                $this->get('session')->getFlashBag()->set('error', 'Le l\'image n\'a pas été modifiée suite à une erreur.');
            }
        }

        if ($post->get('back')) {
            return $this->redirect($post->get('back'));
        }

        return $this->render('SEAdminBundle:slideshow:update-image.html.twig', array(
            'imageForm' => $imageForm->createView(),
            'back'      => $this->getRequest()->query->get('back')
        ));
    }

    /**
     * Trie les images du diaporama
     * @param CmsSlideshow $slideshow
     * @return Response
     */
    public function sortImageAction(CmsSlideshow $slideshow)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $post = $this->getRequest()->request;
        foreach ($post->get('refimages') as $k => $refimage) {
            $image = $entityManager->getRepository('SEAdminBundle:CmsSlideshowImage')->findOneBy(array('id' => $refimage));
            if ($image) {
                $image->setPosition($k);
                $entityManager->persist($image);
            }
        }
        $entityManager->flush();

        return new Response(1);
    }

    /**
     * Supprime une image d'un diaporama
     * @param CmsSlideshowImage $image
     * @return Response
     */
    public function removeImageAction(CmsSlideshowImage $image)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($image);
        $entityManager->flush();

        return new Response(1);
    }
}