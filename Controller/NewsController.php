<?php
namespace SE\AdminBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use SE\AdminBundle\Form\Type\NewsType,
	SE\AdminBundle\Entity\CmsNews,
    SE\AdminBundle\Form\Type\NewsCategoryType,
    SE\AdminBundle\Form\Type\NewsImageType,
    SE\AdminBundle\Entity\CmsNewsImage,
    SE\AdminBundle\Entity\CmsNewsCategorie;

class NewsController extends Controller
{
    public function listingAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	$news = $em->getRepository('SEAdminBundle:CmsNews')->findAllAdmin();

    	return $this->render('SEAdminBundle:news:listing.html.twig', array(
    	    'news' => $news
    	));
    }

    public function addAction()
    {
    	$form = $this->get('form.factory')->create(new NewsType());
    	if ($news = $this->get('se.news.form.handler')->process($form)) {
    		return $this->redirect($this->generateUrl('SEAdminBundle_news_show', array(
                'slug' => $news->getSlug()
            )));
        }

    	return $this->render('SEAdminBundle:news:add.html.twig', array(
    	    'form' => $form->createView()
    	));
    }

    /**
     * Supprimer une news
     * @return Redirect
     */
    public function removeAction(CmsNews $news)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($news);
        $em->flush();

        return $this->redirect($this->generateUrl('SEAdminBundle_news_listing'));
    }

    /**
     * Affiche la news pour la modifier ou ajouter des médias
     * @return Response
     */
    public function showAction(CmsNews $news)
    {
        $post = $this->getRequest()->request;

        $formImage = $this->get('form.factory')->create(new NewsImageType());
        if ($post->get('se_adminbundle_newsimagetype') && $this->get('se.news.image.form.handler')->process($formImage, $news)) {
            $this->get('session')->getFlashBag()->set('message', 'L\'image a bien été ajoutée.');

            return $this->redirect($this->generateUrl('SEAdminBundle_news_show', array(
                'slug' => $news->getSlug()
            )));
        }

        $form = $this->get('form.factory')->create(new NewsType(), $news);
        if ($post->get('se_adminbundle_newstype') && $updNews = $this->get('se.news.form.handler')->process($form)) {
            $this->get('session')->getFlashBag()->set('message', 'Vos modifications sont validées.');

            return $this->redirect($this->generateUrl('SEAdminBundle_news_show', array(
                'slug' => $updNews->getSlug()
            )));
        }

        return $this->render('SEAdminBundle:news:show.html.twig', array(
            'news'      => $news,
            'form'      => $form->createView(),
            'formImage' => $formImage->createView(),
            'message'   => $this->get('session')->getFlashBag()->get('message')
        ));
    }

    /**
     * Formulaire pour ajouter une catégorie d'actualité
     * @return Response
     */
    public function addCategoryAction()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $categories = $entityManager->getRepository('SEAdminBundle:CmsNewsCategorie')->findAll();

        $form = $this->get('form.factory')->create(new NewsCategoryType());
        if ($this->get('se.news.category.form.handler')->process($form)) {
            $this->get('session')->getFlashBag()->set('message', 'La catégorie a bien été ajoutée.');

            return $this->redirect($this->generateUrl('SEAdminBundle_news_add_category'));
        }
        return $this->render('SEAdminBundle:news:add-category.html.twig', array(
            'form'       => $form->createView(),
            'categories' => $categories,
            'message'    => $this->get('session')->getFlashBag()->get('message')
        ));
    }

    /**
     * Supprime une catégorie
     * @return Redirect
     */
    public function removeCategoryAction(CmsNewsCategorie $cat)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($cat);
        $em->flush();
        $this->get('session')->getFlashBag()->set('message', 'La catégorie a bien été supprimée.');

        return $this->redirect($this->generateUrl('SEAdminBundle_news_add_category'));
    }

    /**
     * Tri des images rattachée à la news
     * @return Response
     */
    public function sortImageAction()
    {
        $em = $this->getDoctrine()->getManager();
        $post = $this->getRequest()->request;
        foreach($post->get('refnewsimages') as $k => $refnewsimage) {
            $newsimage = $em->getRepository('SEAdminBundle:CmsNewsImage')->findOneBy(array('id' => $refnewsimage));
            if($newsimage) {
                $newsimage->setPosition($k);
                $em->persist($newsimage);
            }
        }
        $em->flush();

        return new Response(1);
    }

    /**
     * Supprime une image rattachée à une news
     * @return Redirect
     */
    public function removeImageAction(CmsNewsImage $image)
    {
        $slug = $image->getNews()->getSlug();
        $em = $this->getDoctrine()->getManager();
        $em->remove($image);
        $em->flush();

        return $this->redirect($this->generateUrl('SEAdminBundle_news_show', array(
            'slug' => $slug
        )));
    }
}