<?php
namespace SE\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SE\AdminBundle\Form\Type\KeywordType;
use SE\AdminBundle\Entity\CmsKeyword;

/**
 * Gestion des keywords
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class KeywordController extends Controller
{
    /**
     * Ajoute et liste les keywords
     * @return Response
     */
    public function addAction()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $kws = $entityManager->getRepository('SEAdminBundle:CmsKeyword')->findBy(array(), array('pos' => 'ASC', 'id' => 'DESC'));

        $form = $this->get('form.factory')->create(new KeywordType());
        if ($this->get('se.keyword.form.handler')->process($form)) {
            return $this->redirect($this->generateUrl('SEAdminBundle_keyword_add'));
        }

        return $this->render('SEAdminBundle:keyword:index.html.twig', array(
            'form' => $form->createView(),
            'kws'  => $kws
        ));
    }

    /**
     * Sauvegarde l'ordre des mots-clés
     * @return int
     */
    public function sortAction()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $post = $this->getRequest()->request;
        foreach ($post->get('refkws') as $k => $refkw) {
            $keyword = $entityManager->getRepository('SEAdminBundle:CmsKeyword')->findOneBy(array('id' => $refkw));
            if ($keyword) {
                $keyword->setPos($k);
                $entityManager->persist($keyword);
            }
        }
        $entityManager->flush();

        return new Response(1);
    }

    /**
     * Supprime un mot-clé
     * @param CmsKeyword $keyword
     * @return Redirect
     */
    public function removeAction(CmsKeyword $keyword)
    {
        if ($keyword->getUrlimg() != "" && file_exists($keyword->getUrlimg())) {
            unlink($keyword->getUrlimg());
        } 
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($keyword);
        $entityManager->flush();

        return $this->redirect($this->generateUrl('SEAdminBundle_keyword_add'));       
    }
}