<?php
namespace SE\AdminBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use SE\AdminBundle\Entity\CmsCategory;
use SE\AdminBundle\Form\Type\CategoryType;

/**
 * Gestion des catégories
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class CategoryController extends Controller
{
    /**
     * Affichage des catégories
     * @return Response
     */
    public function listingAction()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $categories = $entityManager->getRepository('SEAdminBundle:CmsCategory')->findBy(array(), array('libelle' => 'ASC'));

        $form = $this->get('form.factory')->create(new CategoryType());
        if ($this->get('se.category.form.handler')->process($form)) {
            if ($this->getRequest()->query->get('back')) {
                return $this->redirect($this->getRequest()->query->get('back'));
            }

            return $this->redirect($this->generateUrl('SEAdminBundle_category'));
        }

        return $this->render('SEAdminBundle:category:listing.html.twig', array(
            'form'       => $form->createView(),
            'categories' => $categories
        ));
    }

    /**
     * Modifie la categorie
     * @param CmsCategory $category
     * @return Response
     */
    public function updateAction(CmsCategory $category)
    {
        $form = $this->get('form.factory')->create(new CategoryType(), $category);
        if ($this->get('se.category.form.handler')->process($form)) {
            return $this->redirect($this->generateUrl('SEAdminBundle_category'));
        }

        return $this->render('SEAdminBundle:category:update.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * Supprime une catégorie
     * @param CmsCategory $category
     * @return Redirection
     */
    public function removeAction(CmsCategory $category)
    {
        $entityManager = $this->getDoctrine()->getManager();
        if ($category->isFree()) {
            $entityManager->remove($category);
            $entityManager->flush();
        }

        return $this->redirect($this->generateUrl('SEAdminBundle_category'));
    }

}