<?php
namespace SE\AdminBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller pour afficher les publicités dans le bandeau de droite
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class PubliciteController extends Controller
{
    /**
     * Afficher le bandeau de droite avec les publicités
     * @return Response
     */
    public function showAction()
    {
        $pubs = $this->get('se.api')->getPublicite();

        return $this->render('SEAdminBundle:publicite:show.html.twig', array(
            'pubs' => $pubs
        ));
    }
}