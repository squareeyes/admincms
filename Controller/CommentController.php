<?php
namespace SE\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SE\AdminBundle\Form\Type\CommentType;
use SE\AdminBundle\Entity\CmsGeneric;
use SE\AdminBundle\Entity\CmsComment;

/**
 * Comment controller
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class CommentController extends Controller
{
    /**
     * Liste of comments
     * @param string $configkey
     * @return Response
     */
    public function listingAction($configkey)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $comments = $entityManager->getRepository('SEAdminBundle:CmsComment')->findByConfigKey($configkey);

        return $this->render('SEAdminBundle:comment:listing.html.twig', array(
            'comments'  => $comments,
            'configkey' => $configkey
        ));
    }

    /**
     * Update comments
     * @param string $configkey
     * @param int    $refcomment
     * @return Response
     */
    public function updateAction($configkey, $refcomment)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $comment = $entityManager->getRepository('SEAdminBundle:CmsComment')->findOneById($refcomment);
        if ($comment) {
            $form = $this->get('form.factory')->create(new CommentType(), $comment);
            if ($this->get('se.comment.form.handler')->process($form)) {
                return $this->redirect($this->generateUrl('SEAdminBundle_comments_listing', array(
                    'configkey' => $configkey
                )));
            }

            return $this->render('SEAdminBundle:comment:update.html.twig', array(
                'comment'   => $comment,
                'configkey' => $configkey,
                'form'      => $form->createView()
            ));
        }

        throw $this->createNotFoundException();
    }

    /**
     * Remove comment
     * @param string $configkey
     * @param int    $refcomment
     * @return Redirect
     */
    public function removeAction($configkey, $refcomment)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $comment = $entityManager->getRepository('SEAdminBundle:CmsComment')->findOneById($refcomment);
        if ($comment) {
            $entityManager->remove($comment);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('SEAdminBundle_comments_listing', array(
                'configkey' => $configkey
            )));
        }

        throw $this->createNotFoundException();       
    }
}