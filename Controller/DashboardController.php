<?php
namespace SE\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SE\AdminBundle\Form\Type\ParametersType;

/**
 * Controller pour les pages liées au dashboard
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class DashboardController extends Controller
{
    /**
     * Affiche la page principale du dashboard
     * @return Response
     */
    public function showAction()
    {
        // Redirection sur la banque d'image si le module dashboard est désactivé
        $params = $this->container->getParameter('se_admin');
        if (array_key_exists('dashboard', $params['modules']) && !$params['modules']['dashboard']) {
            return $this->redirect($this->generateUrl('SEAdminBundle_banque_add'));
        }

        // Formulaire pour la page de standby
        $params = array(
            'standby' => array('label' => 'Site web hors ligne : ', 'type' => 'boolean'),
            'standby_allowedip' => array('label' => 'IP autorisée : ', 'type' => 'string'),
            'standby_title' => array('label' => 'Titre : ', 'type' => 'string'),
            'standby_msg' => array('label' => 'Message : ', 'type' => 'text')
        );
        $form = $this->get('form.factory')->create(new ParametersType($params));

        // Formulaire pour la page de mise à jour du naviguateur
        $paramsOldBrowser = array(
            'oldbrowser_ie' => array('label' => 'Internet Explorer : ', 'type' => 'choices', 'options' => array('', 11, 10, 9, 8, 7, 6)),
            'oldbrowser_title' => array('label' => 'Titre : ', 'type' => 'string'),
            'oldbrowser_msg' => array('label' => 'Message : ', 'type' => 'text')
        );
        $formOldBrowser = $this->get('form.factory')->create(new ParametersType($paramsOldBrowser));

        // Traitement des formulaires
        $post = $this->getRequest()->request;
        if ($post->get('standby')) {
            if ($this->get('se.parameter.form.handler')->process($form)) {
                return $this->redirect($this->generateUrl('SEAdminBundle_dashboard'));
            }
        } elseif ($post->get('oldbrowser')) {
            if ($this->get('se.parameter.form.handler')->process($formOldBrowser)) {
                return $this->redirect($this->generateUrl('SEAdminBundle_dashboard'));
            }
        }

        return $this->render('SEAdminBundle:dashboard:show.html.twig', array(
            'form'           => $form->createView(),
            'formOldBrowser' => $formOldBrowser->createView()
        ));
    }
}