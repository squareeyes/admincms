<?php
namespace SE\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SE\AdminBundle\Entity\CmsGeneric;
use SE\AdminBundle\Form\Type\YoutubeType;
use SE\AdminBundle\Entity\CmsYoutube;
use Symfony\Component\HttpFoundation\Response;

/**
 * Youtube controller
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class YoutubeController extends Controller
{
    /**
     * Add youtube
     * @param CmsGeneric $gen
     * @return Response
     */
    public function formAction(CmsGeneric $gen)
    {
        $form = $this->get('form.factory')->create(new YoutubeType());

        return $this->render('SEAdminBundle:youtube:form.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * Add youtube
     * @param CmsGeneric $gen
     * @return Response
     */
    public function addAction(CmsGeneric $gen)
    {
        $youtube = new CmsYoutube();
        $youtube->setGeneric($gen);
        $form = $this->get('form.factory')->create(new YoutubeType(), $youtube);

        if ($this->get('se.youtube.form.handler')->process($form)) {
            $this->get('session')->getFlashBag()->set('message', 'Votre vidéo a bien été ajoutée.');
        } else {
            $this->get('session')->getFlashBag()->set('error', 'Vous devez charger une image pour illustrer votre vidéo.');
        }

        return $this->redirect($this->generateUrl('SEAdminBundle_generic_add', array(
            'configkey' => $gen->getConfigkey(),
            'refgen'    => $gen->getId()
        )));
    }

    /**
     * Sort youtube
     * @param CmsGeneric $gen
     * @return Response
     */
    public function sortAction(CmsGeneric $gen)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $post = $this->getRequest()->request;
        foreach ($post->get('refyoutubes') as $k => $refyoutube) {
            $youtube = $entityManager->getRepository('SEAdminBundle:CmsYoutube')->findOneBy(array('id' => $refyoutube));
            if ($youtube) {
                $youtube->setPosition($k);
                $entityManager->persist($youtube);
            }
        }
        $entityManager->flush();

        return new Response(1);
    }

    /**
     * Remove
     * @param CmsYoutube
     * @return Redirect
     */
    public function removeAction(CmsYoutube $youtube)
    {
        $refgen = $youtube->getGeneric()->getId();
        $configkey = $youtube->getGeneric()->getConfigkey();

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($youtube);
        $entityManager->flush();

        return $this->redirect($this->generateUrl('SEAdminBundle_generic_add', array(
            'configkey' => $configkey,
            'refgen'    => $refgen
        )));
    }
}