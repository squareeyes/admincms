<?php
namespace SE\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SE\AdminBundle\Entity\CmsGeneric;
use SE\AdminBundle\Form\Type\GenericFileType;
use SE\AdminBundle\Form\Type\GenericCategoryFileType;
use SE\AdminBundle\Entity\CmsGenericFile;
use SE\AdminBundle\Entity\CmsGenericCategoryFile;
use Symfony\Component\HttpFoundation\Response;

/**
 * Generic files controller
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class GenericFileController extends Controller
{
    /**
     * File form
     * @param CmsGeneric $gen
     * @return Response
     */
    public function formAction(CmsGeneric $gen)
    {
        $form = $this->get('form.factory')->create(new GenericFileType());

        return $this->render('SEAdminBundle:genericfile:form.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * File Category form
     * @param CmsGeneric $gen
     * @return Response
     */
    public function formCategoryAction(CmsGeneric $gen)
    {
        $form = $this->get('form.factory')->create(new GenericCategoryFileType());
        if ($this->get('se.category_file.form.handler')->process($form)) {
            $this->get('session')->getFlashBag()->set('message', 'La catégorie a bien été ajoutée.');

            return $this->redirect($this->generateUrl('SEAdminBundle_generic_add', array(
                'configkey' => $gen->getConfigkey(),
                'refgen'    => $gen->getId()
            )));
        }
        $entityManager = $this->getDoctrine()->getManager();
        $categories = $entityManager->getRepository('SEAdminBundle:CmsGenericCategoryFile')->findBy(array(), array('title' => 'ASC'));

        return $this->render('SEAdminBundle:genericfile:form_category.html.twig', array(
            'form'       => $form->createView(),
            'regen'      => $gen->getId(),
            'gen'        => $gen,
            'categories' => $categories
        ));
    }

    /**
     * File category remove
     * @param CmsGeneric $gen
     * @param int $refcat
     * @return Redirect
     */
    public function removeCategoryFileAction(CmsGeneric $gen, $refcat)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $cat = $entityManager->getRepository('SEAdminBundle:CmsGenericCategoryFile')->findOneById($refcat);

        if ($cat && $cat->isFree()) {
            $entityManager->remove($cat);
            $entityManager->flush();
        }

        return $this->redirect($this->generateUrl('SEAdminBundle_genericcategory_add', array(
            'id' => $gen->getId()
        )));
    }

    /**
     * Add file
     * @param CmsGeneric $gen
     * @return Response
     */
    public function addAction(CmsGeneric $gen)
    {
        $file = new CmsGenericFile();
        $file->setGeneric($gen);
        $form = $this->get('form.factory')->create(new GenericFileType(), $file);

        if ($this->get('se.file.form.handler')->process($form)) {
            $this->get('session')->getFlashBag()->set('message', 'Votre fichier a bien été ajoutée.');
        } else {
            $this->get('session')->getFlashBag()->set('error', 'Vous devez charger un fichier et un titre pour votre fichier.');
        }

        return $this->redirect($this->generateUrl('SEAdminBundle_generic_add', array(
            'configkey' => $gen->getConfigkey(),
            'refgen'    => $gen->getId()
        )));
    }

    /**
     * Sort files
     * @param CmsGeneric $gen
     * @return Response
     */
    public function sortAction(CmsGeneric $gen)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $post = $this->getRequest()->request;
        foreach ($post->get('reffiles') as $k => $reffile) {
            $file = $entityManager->getRepository('SEAdminBundle:CmsGenericFile')->findOneBy(array('id' => $reffile));
            if ($file) {
                $file->setPosition($k);
                $entityManager->persist($file);
            }
        }
        $entityManager->flush();

        return new Response(1);
    }

    /**
     * Remove
     * @param CmsGenericFile $file
     * @return Redirect
     */
    public function removeAction(CmsGenericFile $file)
    {
        $refgen = $file->getGeneric()->getId();
        $configkey = $file->getGeneric()->getConfigkey();

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($file);
        $entityManager->flush();

        return $this->redirect($this->generateUrl('SEAdminBundle_generic_add', array(
            'configkey' => $configkey,
            'refgen'    => $refgen
        )));
    }
}