<?php
namespace SE\AdminBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use SE\AdminBundle\Entity\CmsVideo;
use SE\AdminBundle\Form\Type\VideoType;

/**
 * Controller lié aux vidéos
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class VideoController extends Controller
{
    /**
     * Modification de la video
     * @param CmsVideo $video
     * @return Response
     */
    public function updateAction(CmsVideo $video)
    {
        $post = $this->getRequest()->request;
        $videoForm = $this->get('form.factory')->create(new VideoType(), $video);
        if ($post->get('se_adminbundle_videotype')) {
            if ($this->get('se.video.form.handler')->process($videoForm)) {
                $this->get('session')->getFlashBag()->set('message', 'La vidéo a bien été modifiée.');
            } else {
                $this->get('session')->getFlashBag()->set('error', 'La vidéo n\'a pas été modifié suite à une erreur. Les 3 formats sont obligatoires, attention à bien les respecter !');
            }
        }

        if ($post->get('back')) {
            return $this->redirect($post->get('back'));
        }

        return $this->render('SEAdminBundle:video:update.html.twig', array(
            'videoForm' => $videoForm->createView(),
            'video'     => $video
        ));
    }
}