<?php
namespace SE\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller,
    Symfony\Component\HttpFoundation\Response,
    SE\AdminBundle\Form\Type\NewsletterType,
    SE\AdminBundle\Entity\CmsNewsletter,
    SE\AdminBundle\Entity\CmsNewsletterMail,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter,
    SE\AdminBundle\Entity\CmsNewsletterImage,
    SE\AdminBundle\Form\Type\NewsletterImageType;

/**
 * Controller pour les pages liée à la gestion des newsletters
 *
 * @author Vincent KELLEHER <vincent@squaereeyes.com>
 */
class NewsletterController extends Controller
{
    /**
     * Listing des newsletters
     * @return Response
     */
    public function listingAction()
    {
        $em = $this->getDoctrine()->getManager();
        $newsletters = $em->getRepository('SEAdminBundle:CmsNewsletter')->findBy(array(), array('sent' => 'ASC', 'datesent' => 'DESC'));

        return $this->render('SEAdminBundle:newsletter:listing.html.twig', array(
            'newsletters' => $newsletters,
            'message' => $this->get('session')->getFlashBag()->get('message'),
            'erreur'  => $this->get('session')->getFlashBag()->get('erreur')
        ));
    }

    /**
     * Créer une Newsletter
     * @return Response
     */
    public function createAction()
    {
        $form = $this->get('form.factory')->create(new NewsletterType());
        if ($newsletter = $this->get('se.newsletter.form.handler')->process($form)) {
            return $this->redirect($this->generateUrl('SEAdminBundle_newsletter_show', array(
                'newsletter' => $newsletter->getId()
            )));
        }

        return $this->render('SEAdminBundle:newsletter:create.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * Modifier une Newsletter
     * @param CmsNewsletter $newsletter
     * @return Response
     */
    public function showAction(CmsNewsletter $newsletter)
    {
        $form = $this->get('form.factory')->create(new NewsletterType(), $newsletter);
        $newsImage = new CmsNewsletterImage();
        $newsImage->setNewsletter($newsletter);
        $formImage = $this->get('form.factory')->create(new NewsletterImageType(), $newsImage);

        if ($testEmail = $this->get('request')->request->get('test_email')) {
            if ($this->get('se.mailer')->sendTestNewsletter($newsletter, $testEmail)) {
                $this->get('session')->getFlashBag()->set('notice', 'Email de test envoyé avec succès');
            } else {
                $this->get('session')->getFlashBag()->set('error', 'Echec de l\'envoi');
            }
        }

        if ($this->get('se.newsletter_image.form.handler')->process($formImage)) {
            return $this->redirect($this->generateUrl('SEAdminBundle_newsletter_show', array(
                'newsletter' => $newsletter->getId()
            )));
        }

        if (!$testEmail && $this->get('se.newsletter.form.handler')->process($form)) {
            return $this->redirect($this->generateUrl('SEAdminBundle_newsletter_listing'));
        }

        return $this->render('SEAdminBundle:newsletter:show.html.twig', array(
            'form'       => $form->createView(),
            'formImage'  => $formImage->createView(),
            'newsletter' => $newsletter
        ));
    }

    /**
     * Remove image
     * @param CmsNewsletterImage $image
     * @return Redirect
     */
    public function removeImageAction(CmsNewsletterImage $image)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $refnl = $image->getNewsletter()->getId();
        $entityManager->remove($image);
        $entityManager->flush();

        return $this->redirect($this->generateUrl('SEAdminBundle_newsletter_show', array(
            'newsletter' => $refnl
        )));
    }

    /**
     * Upodate newsletter image
     * @param CmsNewsletterImage $image
     * @return Response
     */
    public function updateImageAction(CmsNewsletterImage $image)
    {
        $newsletter = $image->getNewsletter();
        $entityManager = $this->getDoctrine()->getManager();
        $formImage = $this->get('form.factory')->create(new NewsletterImageType(), $image);

        if ($this->get('se.newsletter_image.form.handler')->process($formImage)) {
            return $this->redirect($this->generateUrl('SEAdminBundle_newsletter_show', array(
                'newsletter' => $newsletter->getId()
            )));
        }

        return $this->render('SEAdminBundle:newsletter:update_image.html.twig', array(
            'formImage'  => $formImage->createView(),
            'newsletter' => $newsletter,
            'image'      => $image
        ));
    }

    /**
     * Modifier l'ordre des images
     * @return Response
     */
    public function sortImageAction()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $post = $this->getRequest()->request;
        foreach ($post->get('refimages') as $k => $refimages) {
            $image = $entityManager->getRepository('SEAdminBundle:CmsNewsletterImage')->findOneBy(array('id' => $refimages));
            if ($image) {
                $image->setPosition($k);
                $entityManager->persist($image);
            }
        }
        $entityManager->flush();

        return new Response(1);
    }

    /**
     * Envoi une Newsletter
     * @param CmsNewsletter $newsletter
     * @return Response
     */
    public function sendAction(CmsNewsletter $newsletter)
    {
        $this->get('se.mailer')->sendNewsletter($newsletter);

        return $this->redirect($this->generateUrl('SEAdminBundle_newsletter_listing'));
    }

    /**
     * Supprime une Newsletter
     * @param CmsNewsletter $newsletter
     * @return Response
     */
    public function deleteAction(CmsNewsletter $newsletter)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($newsletter);
        $entityManager->flush();

        return $this->redirect($this->generateUrl('SEAdminBundle_newsletter_listing'));
    }

    /**
     * Listing des emails pour les newsletters
     * @return Response
     */
    public function emailListingAction()
    {
        $em = $this->getDoctrine()->getManager();
        $emails = $em->getRepository('SEAdminBundle:CmsNewsletterMail')->findBy(array(), array('dateadd' => 'DESC'));

        return $this->render('SEAdminBundle:newsletter:email_listing.html.twig', array(
            'emails'  => $emails,
            'message' => $this->get('session')->getFlashBag()->get('message'),
            'erreur'  => $this->get('session')->getFlashBag()->get('erreur')
        ));
    }

    /**
     * Supprime un email pour les newsletters
     * @return Response
     */
    public function emailDeleteAction(CmsNewsletterMail $email)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($email);
        $em->flush();

        return $this->redirect($this->generateUrl('SEAdminBundle_newsletter_email_listing'));
    }
}
