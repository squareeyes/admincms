<?php
namespace SE\AdminBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use SE\AdminBundle\Entity\CmsSeo;
use SE\AdminBundle\Entity\CmsSlideshow;
use SE\AdminBundle\Entity\CmsVideo;
use SE\AdminBundle\Form\Type\SeoType;

/**
 * Controller pour la gestion des pages static du site internet
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class PageController extends Controller
{
    /**
     * Listing des pages static
     * @return Response
     */
    public function listingAction()
    {
    	return $this->render('SEAdminBundle:page:listing.html.twig');
    }

    /**
     * Gestion du contenu de la page
     * @param string $pagekey
     * @return Response
     */
    public function showAction($pagekey)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $page = $entityManager->getRepository('SEAdminBundle:CmsSeo')->findOneByPagekey($pagekey);
        if (!$page) {
            $page = new CmsSeo();
            $video = new CmsVideo();
            $slideshow = new CmsSlideshow();
            $page->setVideo($video);
            $page->setSlideshow($slideshow);
            $page->setPagekey($pagekey);
            $entityManager->persist($page);
            $entityManager->flush();
        }
        $seadminpage = $this->container->getParameter('se_admin')['pages'][$pagekey];
        $form = $this->get('form.factory')->create(new SeoType($seadminpage['image'], $seadminpage['content']), $page);
        if ($this->get('se.seo.form.handler')->process($form)) {
            $this->get('session')->getFlashBag()->set('message', 'Vos modifications ont bien été prisent en compte.');
            return $this->redirect($this->generateUrl('SEAdminBundle_page_show', array('pagekey' => $pagekey)));
        }

        return $this->render('SEAdminBundle:page:show.html.twig', array(
            'page'    => $page,
            'pagekey' => $pagekey,
            'form'    => $form->createView(),
            'message'   => $this->get('session')->getFlashBag()->get('message'),
            'erreur'    => $this->get('session')->getFlashBag()->get('error')
        ));
    }

}