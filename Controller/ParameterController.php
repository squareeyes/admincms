<?php
namespace SE\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SE\AdminBundle\Form\Type\ParametersType;

/**
 * Gestion des contantes
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class ParameterController extends Controller
{
    /**
     * Construction et sauvegarde du formulaire avec les constantes
     * @return Response
     */
    public function formAction()
    {
        $parameters = $this->container->getParameter('se_admin')['modules']['parameters'];
        $form = $this->get('form.factory')->create(new ParametersType($parameters));

        if ($this->get('se.parameter.form.handler')->process($form)) {
            return $this->redirect($this->generateUrl('SEAdminBundle_parameter'));
        }

        return $this->render('SEAdminBundle:parameters:form.html.twig', array(
            'form' => $form->createView()
        ));
    }
}